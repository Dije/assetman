<?php

class locationModel { 
	private $ID;
	private $LocationName; 
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;    
    
	public function __construct() {} 

	public function countDataLocation($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataLocations($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/all";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataLocationOffset($token, $limit, $offset, $order, $dir){
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/list/".$limit."/".$offset."/".$order."/".$dir;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function searchDataLocations($token, $limit, $offset, $order, $dir, $search) {
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/search/".$limit."/".$offset."/".$order."/".$dir."/".$search;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function CountSearchLocation($token, $search){
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/countSearch/".$search;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataLocation($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getExcDataLocation($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function CreateLocation($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'location_name' => $data['LocationName'],
			'remark' => $data['Remark'],
			'created_user_id'=> $ID,
			'updated_user_id' => $ID, 
			'created_at' => $Date, 
			'updated_at'=> $Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataLocation($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/".$data['location_id'];

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'location_name' => $data['LocationName'],
			'remark' => $data['Remark'],
			'updated_user_id' => $ID,  
			'updated_at'=> $Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function DeleteLocation($token, $id, $LocationNewName, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/location/".$id; 
		
		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'location_name' => $LocationNewName, 
			'deleted_user_id' => $ID,  
			'deleted_at'=> $Date 
		);

		$data_json = json_encode($data_array);   

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
}