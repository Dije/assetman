<?php

class companylicenseModel { 
	private $ID;
	private $ParentLicenseID;
	private $LicenseNo;
	private $LicenseTypeID; 
	private $CompanyID; 
	private $Renewable; 
	private $ReminderCounter; 
	private $IssuedBy; 
	private $IssuedDate; 
	private $ExpiredDate; 
	private $EarliestRenewalDate; 
	private $Status; 
	private $RenewalStatus; 
	private $ApprovedUserID; 
	private $RenewalApprovedUserID; 
	private $ApprovedDate;  
	private $RenewalApprovedDate; 
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;    

	public function __construct() {} 

	public function getDataCompanyLicenses($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataAppCompanyLicenses($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/app/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataExpCompanyLicenses($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/exp/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

    public function getDataCompanyLicenseByCompanyId($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/byCompany/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataCompanyLicense($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getExcDataCompanyLicense($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
	
	public function CreateCompanyLicense($token, $data, $Renewable, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'parent_license_id' => (int)$data['ParentLN'],
			'license_no' => $data['LicenseNo'],
			'license_type_id' => (int)$data['LicenseTypeID'],
			'company_id' => (int)$data['company_id'],
			'renewable' => (bool)$Renewable,
			'reminder_counter' => (int)$data['ReminderCounter'],
			'issued_by' => $data['IssuedBy'],
			'issued_date' => (int)strtotime($data['IssuedDate']),
			'expired_date' => (int)strtotime($data['ExpiredDate']),
			'earliest_renewal_date' => (int)strtotime($data['EarliestRenewalDate']),
			'last_renewal_date' => (int)strtotime($data['LastRenewalDate']),
			'status' => (int)$data['Status'],
			'renewal_status' => (int)$data['RenStatus'],
			'approved_user_id' => (int)$data['ApprovedBy'],
			'renewal_approved_user_id' => (int)$data['RenewalApprovedBy'],
			'approved_date' => (int)strtotime($data['ApprovedDate']),
			'renewal_approved_date' => (int)strtotime($data['RenewalApprovedDate']),
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array); 

		curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataCompanyLicense($token, $data, $Renewable, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/".$data['company_license_id'];

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'parent_license_id' => (int)$data['ParentLN'],
			'license_no' => $data['LicenseNo'],
			'license_type_id' => (int)$data['LicenseTypeID'],
			'company_id' => (int)$data['company_id'],
			'renewable' => (bool)$Renewable,
			'reminder_counter' => (int)$data['ReminderCounter'],
			'issued_by' => $data['IssuedBy'],
			'issued_date' => (int)strtotime($data['IssuedDate']),
			'expired_date' => (int)strtotime($data['ExpiredDate']),
			'earliest_renewal_date' => (int)strtotime($data['EarliestRenewalDate']),
			'last_renewal_date' => (int)strtotime($data['LastRenewalDate']),
			'status' => (int)$data['Status'],
			'renewal_status' => (int)$data['RenStatus'], 
			'remark' => $data['Remark'],
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date  
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	
	public function UpdateStatusCompanyLicenseApproved($token, $id, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/status/".$id;

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array( 
			'status' => 3,
			'approved_user_id' => (int)$ID, 
			'approved_date' => (int)$Date
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function UpdateRenewalStatus3CompanyLicenseApproved($token, $id, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/renewal/".$id;

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array( 
			'renewal_status' => 5,
			'renewal_approved_user_id' => (int)$ID, 
			'renewal_approved_date' => (int)$Date
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function UpdateRenewalStatus4CompanyLicenseApproved($token, $id, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/renewal/".$id;

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array( 
			'renewal_status' => 6,
			'renewal_approved_user_id' => (int)$ID, 
			'renewal_approved_date' => (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
		
	public function UpdateDataCompanyRemark($token, $id, $ResultRemark, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/remark/".$id;

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array( 
			'remark' => $ResultRemark 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function UpdateDataCompanyDeactive($token, $id, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/statusDeactive/".$id;

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array( 
			'status' => 4 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function DeleteCompanyLicense($token, $id, $CompanyLicenseNewName, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyLicense/".$id;  

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		); 

		$data_array = array( 
			'license_no' => $CompanyLicenseNewName,
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
		); 
		
		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
} 