<?php

class emailqueueModel { 
	private $ID;
	private $EmailQueueTypeID;
	private $EmailRecipient;
	private $EmailCC;
	private $EmailSubject;
	private $EmailBody;
	private $Status;
	private $ErrorMessage; 
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;   

	public function __construct() {} 

	public function getDataEmailQueues($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/EmailQueue/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataEmailQueue($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/EmailQueue/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataEmailQueueByStatus($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/EmailQueue/byStatus/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
 
	public function getExcDataEmailQueue($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/EmailQueue/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
	
	// public function CreateEmailQueue($token, $data, $ID, $Date){
	// 	$ch = curl_init();

	// 	$url = "http://localhost:8080/api/EmailQueue/";

	// 	$header = array(
	// 		'Authorization: '.$token,
	// 		'Accept: application/json',
	// 		'Content-Type: application/json'
	// 	);
		
	// 	$data_array = array(
	// 		'division_id' => (int)$data['DivisionID'],
	// 		'emailqueue_name' => $data['EmailQueueName'],
	// 		'remark' => $data['Remark'],
	// 		'created_user_id'=> (int)$ID,
	// 		'updated_user_id' => (int)$ID, 
	// 		'created_at' => (int)$Date, 
	// 		'updated_at'=> (int)$Date 
	// 	);

	// 	$data_json = json_encode($data_array);  

	// 	curl_setopt($ch, CURLOPT_URL, $url);
	// 	curl_setopt($ch, CURLOPT_POST, true);
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

	// 	$resp = curl_exec($ch);

	// 	if($e = curl_error($ch)){
	// 		echo $e;
	// 	}
	// 	else{
	// 		$decoded =json_decode($resp, true);
	// 		return $decoded;
	// 	}
	// 	curl_close($ch);
	// }

	// public function UpdateDataEmailQueue($token, $data, $ID, $Date){
	// 	$ch = curl_init();

	// 	$url = "http://localhost:8080/api/EmailQueue/".$data['emailqueue_id'];

	// 	$header = array(
	// 		'Authorization: '.$token,
	// 		'Accept: application/json',
	// 		'Content-Type: application/json'
	// 	);

	// 	$data_array = array( 
	// 		'division_id' => (int)$data['DivisionID'],
	// 		'emailqueue_name' => $data['EmailQueueName'],
	// 		'remark' => $data['Remark'],
	// 		'updated_user_id' => (int)$ID,  
	// 		'updated_at'=> (int)$Date  
	// 	); 

	// 	$data_json = json_encode($data_array);  

	// 	curl_setopt($ch, CURLOPT_URL, $url);
	// 	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

	// 	$resp = curl_exec($ch);

	// 	if($e = curl_error($ch)){
	// 		echo $e;
	// 	}
	// 	else{
	// 		$decoded =json_decode($resp, true);
	// 		return $decoded;
	// 	}
	// 	curl_close($ch);
	// }

	// public function DeleteEmailQueue($token, $id, $EmailQueueNewName, $ID, $Date){
	// 	$ch = curl_init();

	// 	$url = "http://localhost:8080/api/EmailQueue/".$id;  

	// 	$header = array(
	// 		'Authorization: '.$token,
	// 		'Accept: application/json',
	// 		'Content-Type: application/json'
	// 	);

	// 	$data_array = array( 
	// 		'emailqueue_name' => $EmailQueueNewName,
	// 		'deleted_user_id' => (int)$ID,  
	// 		'deleted_at'=> (int)$Date 
	// 	);

	// 	$data_json = json_encode($data_array);

	// 	curl_setopt($ch, CURLOPT_URL, $url);
	// 	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

	// 	$resp = curl_exec($ch);

	// 	if($e = curl_error($ch)){
	// 		echo $e;
	// 	}
	// 	else{
	// 		$decoded =json_decode($resp, true);
	// 		return $decoded;
	// 	}
	// 	curl_close($ch);
	// }
} 