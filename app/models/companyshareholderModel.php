<?php

class companyshareholderModel { 
	private $ID;
	private $CompanyShareholderID;
	private $ShareholderName;
	private $NumberOfShare;
	private $PercentageOfShare;
	private $ShareAmount; 
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;   

	public function __construct() {} 

	public function getDataCompanyShareholders($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyShareholder/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

    public function getDataCompanyShareholderByCompanyId($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyShareholder/byCompany/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataCompanyShareholder($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyShareholder/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getExcDataCompanyShareholder($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyShareholder/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
	
	public function CreateCompanyShareholder($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyShareholder/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'company_id' => (int)$data['company_id'],
			'shareholder_name' => $data['ShareholderName'],
			'number_of_share' => (float)$data['Number'],
			'percentage_of_share' => (float)$data['Percentage'],
			'share_amount' => (float)$data['Amount'], 
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array); 

		curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataCompanyShareholder($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyShareholder/".$data['company_shareholder_id'];

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'company_id' => (int)$data['company_id'],
			'shareholder_name' => $data['ShareholderName'],
			'number_of_share' => (float)$data['Number'],
			'percentage_of_share' => (float)$data['Percentage'],
			'share_amount' => (float)$data['Amount'], 
			'deactived_date' => (int)$data['DeactivatedDate'],
			'remark' => $data['Remark'],
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date  
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function DeleteCompanyShareholder($token, $id, $CompanyShareholderNewName, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/companyShareholder/".$id;  

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		); 

		$data_array = array( 
			'shareholder_name' => $CompanyShareholderNewName,
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
		); 
		
		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
} 