<?php

class laptopModel{ 
	private $ID;
	private $UserID;
	private $RoleID;
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;   

	public function __construct() {} 

	public function getDataLaptops($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Laptop/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataLaptop($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Laptop/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataLaptopByUserID($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Laptop/byUserId/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
 
	public function getExcDataLaptop($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Laptop/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function CreateLaptop($token, $data, $CD, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Laptop/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'laptop_code' => $data['LaptopCode'],
			'user_id' => (int)$data['Username'],
			'processor_id' => (int)$data['ProcessorName'], 
			'motherboard_id' => (int)$data['MotherboardName'], 
			'ram_id' => (int)$data['RAMName'], 
			'hdd_id' => (int)$data['HDDName'], 
			'cd' => (bool)$CD,  
			'monitor_id' => (int)$data['MonitorName'], 
			'os_id' => (int)$data['OSName'], 
			'product_key_os' => $data['ProductKeyOS'],
			'office_id' => (int)$data['OfficeName'], 
			'product_key_office' => $data['ProductKeyOffice'],
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataLaptop($token, $data, $CD, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Laptop/".$data['laptop_id'];

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'laptop_code' => $data['LaptopCode'],
			'user_id' => (int)$data['Username'],
			'processor_id' => (int)$data['ProcessorName'], 
			'motherboard_id' => (int)$data['MotherboardName'], 
			'ram_id' => (int)$data['RAMName'], 
			'hdd_id' => (int)$data['HDDName'], 
			'cd' => (bool)$CD,  
			'monitor_id' => (int)$data['MonitorName'], 
			'os_id' => (int)$data['OSName'], 
			'product_key_os' => $data['ProductKeyOS'],
			'office_id' => (int)$data['OfficeName'], 
			'product_key_office' => $data['ProductKeyOffice'],
			'remark' => $data['Remark'],
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
 
	public function DeleteLaptop($token, $id, $LaptopNewCode, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Laptop/".$id;  

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(  
			'laptop_code' => $LaptopNewCode,
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
} 