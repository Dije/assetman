<?php

class mailModel {  
	public function __construct() {} 
	
	public function sendMail($To, $Subject, $Body){
		$ch = curl_init();

		$url = "http://localhost:8080/api/auth/sendMail";
		
		$header = array( 
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'to' => $To,
			'subject' => $Subject, 
			'body'=> $Body
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	 
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);	  

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
}