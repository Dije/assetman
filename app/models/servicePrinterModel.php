<?php

class servicePrinterModel{ 
	private $ID;
	private $LaptopID;
	private $DateService;
	private $DateDone;
	private $Company;
	private $Problem;
	private $Price;
	private $Solved;
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;    
	public function __construct() {} 

	public function getDataServicePrinters($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/servicePrinter/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataServicePrinter($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/servicePrinter/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
 
	public function getExcDataServicePrinter($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/servicePrinter/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function CreateServicePrinter($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/servicePrinter/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'user_printer_id' => (int)$data['user_printer_id'],
			'date_service' => (int)strtotime($data['date_service']),
			'date_done' => (int)strtotime($data['date_done']), 
			'company' => $data['company'],
			'problem' => $data['problem'],
			'price' => (int)$data['price'],
			'solved' => $data['solved'], 
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataServicePrinter($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/servicePrinter/".$data['service_printer_id'];

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'user_printer_id' => (int)$data['user_printer_id'],
			'date_service' => (int)strtotime($data['date_service']),
			'date_done' => (int)strtotime($data['date_done']), 
			'company' => $data['company'],
			'problem' => $data['problem'],
			'price' => (int)$data['price'],
			'solved' => $data['solved'], 
			'remark' => $data['Remark'],
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
 
	public function DeleteServicePrinter($token, $id, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/servicePrinter/".$id;  

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(   
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
} 