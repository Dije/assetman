<?php

class monitorModel { 
	private $ID;
	private $MonitorName;
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;   

	public function __construct() {} 

	public function getDataMonitors($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/monitor/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataMonitor($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/monitor/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getExcDataMonitor($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/monitor/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
	
	public function CreateMonitor($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/monitor/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'monitor_name' => $data['MonitorName'],
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array); 

		curl_setopt($ch, CURLOPT_HTTPHEADER, $header); 
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);  
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataMonitor($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/monitor/".$data['monitor_id'];

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'monitor_name' => $data['MonitorName'],
			'remark' => $data['Remark'],  
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date  
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function DeleteMonitor($token, $id, $MonitorNewName, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/monitor/".$id;  

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		); 

		$data_array = array( 
			'monitor_name' => $MonitorNewName,
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
		); 
		
		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
} 