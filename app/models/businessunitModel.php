<?php

class businessunitModel { 
	private $ID;
	private $BusinessUnitName;
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;    

	public function __construct() {}  

    public function getDataBusinessUnits($token){
        $ch = curl_init();

        $url = "http://localhost:8080/api/BusinessUnit/";

        $header = array(
            'Authorization: '.$token
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }

    public function getDataBusinessUnit($token, $id){
        $ch = curl_init();

        $url = "http://localhost:8080/api/BusinessUnit/".$id;

        $header = array(
            'Authorization: '.$token
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }

    public function getExcDataBusinessUnit($token, $id){
        $ch = curl_init();

        $url = "http://localhost:8080/api/BusinessUnit/exc/".$id;

        $header = array(
            'Authorization: '.$token
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    } 
    
    public function CreateBusinessUnit($token, $data, $ID, $Date){
        $ch = curl_init();

        $url = "http://localhost:8080/api/BusinessUnit/";

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'business_unit_name' => $data['BusinessUnitName'],
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array); 

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }

    public function UpdateDataBusinessUnit($token, $data, $ID, $Date){
        $ch = curl_init();

        $url = "http://localhost:8080/api/BusinessUnit/".$data['business_unit_id'];

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'business_unit_name' => $data['BusinessUnitName'],
			'remark' => $data['Remark'], 
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array); 

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }
 
    public function DeleteBusinessUnit($token, $id, $BusinessUnitNewName, $ID, $Date){
        $ch = curl_init();

        $url = "http://localhost:8080/api/BusinessUnit/".$id;  

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
        $data_array = array( 
			'business_unit_name' => $BusinessUnitNewName,
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
        );

		$data_json = json_encode($data_array);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }
}