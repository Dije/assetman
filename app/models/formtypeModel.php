<?php

class formtypeModel { 
	private $ID;
	private $FormTypeCode;
	private $FormTypeDescription;
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;    
	
	public function __construct() {} 

	public function countDataFormType($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataFormTypes($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/all";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataFormTypeOffset($token, $limit, $offset, $order, $dir){
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/list/".$limit."/".$offset."/".$order."/".$dir;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function searchDataFormTypes($token, $limit, $offset, $order, $dir, $search) {
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/search/".$limit."/".$offset."/".$order."/".$dir."/".$search;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function CountSearchFormType($token, $search){
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/countSearch/".$search;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataFormType($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getExcDataFormType($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
	
	public function CreateFormType($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'form_type_code' => $data['FormTypeCode'],
			'form_type_description' => $data['FormTypeDescription'],
			'remark' => $data['Remark'], 
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);    

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataFormType($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/".$data['form_type_id'];

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'form_type_code' => $data['FormTypeCode'],
			'form_type_description' => $data['FormTypeDescription'],
			'remark' => $data['Remark'],  
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);   

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function DeleteFormType($token, $id, $FormTypeNewName, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/FormType/".$id;  

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'form_type_code' => $FormTypeNewName,  
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);   

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
}