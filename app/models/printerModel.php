<?php

class printerModel { 
	private $ID;
	private $PrinterName;
	private $BlackID;
	private $CyanID;
	private $MagentaID;
	private $YellowID;
	private $TricolorID;
	private $OthersID; 
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;   

	public function __construct() {} 

	public function getDataPrinters($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/printer/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataPrinter($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/printer/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
 
	public function getExcDataPrinter($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/printer/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
	
	public function CreatePrinter($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/printer/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array( 
			'printer_name' => $data['PrinterName'],
			'black_id'=> (int)$data['BlackID'],
			'cyan_id'=> (int)$data['CyanID'],
			'magenta_id'=> (int)$data['MagentaID'],
			'yellow_id'=> (int)$data['YellowID'],
			'color_id'=> (int)$data['ColorID'],
			'tricolor_id'=> (int)$data['TricolorID'],
			'others_id'=> (int)$data['OthersID'],
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataPrinter($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/printer/".$data['PrinterID'];

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array( 
			'printer_name' => $data['PrinterName'],
			'black_id'=> (int)$data['BlackID'],
			'cyan_id'=> (int)$data['CyanID'],
			'magenta_id'=> (int)$data['MagentaID'],
			'yellow_id'=> (int)$data['YellowID'],
			'color_id'=> (int)$data['ColorID'],
			'tricolor_id'=> (int)$data['TricolorID'],
			'others_id'=> (int)$data['OthersID'],
			'remark' => $data['Remark'],
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date  
		); 

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function DeletePrinter($token, $id, $PrinterNewName, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/printer/".$id;  

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array( 
			'printer_name' => $PrinterNewName,
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
} 