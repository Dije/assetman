<?php

class loginModel {
	
	private $table = 'user';
	private $db;

	public function __construct(){ } 

    public function login($data){
		$ch = curl_init();

		$url = "http://localhost:8080/api/auth/login";

		$header= array(
			'Accept: application/json',
			'Content-Type: application/json'
		); 

		$data_array = array(
			'username' => strtolower($data['username']),
			'password' => $data['password']
		); 

		$data_json = json_encode($data_array); 

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);	 

		$resp = curl_exec($ch); 

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataPassword($data, $Email, $Name, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/auth/password/".$Name."/".$Email;

		$header= array(
			'Accept: application/json',
			'Content-Type: application/json'
		); 

		$data_array = array(  
			'password' => $data['Password'],
			'confirm_password' => $data['ConPassword'],
			'updated_at'=> $Date 
		);

		$data_json = json_encode($data_array); 

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);	  

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function UpdateDataRequest($Email, $Name, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/auth/upReq/".$Name."/".$Email;

		$header= array(
			'Accept: application/json',
			'Content-Type: application/json'
		); 

		$data_array = array(  
			'request_change_at' => $Date
		);

		$data_json = json_encode($data_array); 

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);	  

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 

	public function CheckExisiting($data){
		$ch = curl_init();

		$url = "http://localhost:8080/api/auth/checkEx";

		$header= array(
			'Accept: application/json',
			'Content-Type: application/json'
		); 

		$data_array = array( 
			'username' => strtolower($data['Name']),
			'email' => $data['Email']
		);

		$data_json = json_encode($data_array); 

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);	  

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
}