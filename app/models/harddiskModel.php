<?php

class harddiskModel{ 
	private $ID;
	private $HarddiskCode;
	private $Merk;
	private $Type;
	private $Capacity;
	private $Warranty;
	private $UserID;
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;    

	public function __construct() {} 

	public function getDataHarddisks($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/harddisk/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataHarddisk($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/harddisk/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
 
	public function getExcDataHarddisk($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/harddisk/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function CreateHarddisk($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/harddisk/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array( 
			'harddisk_code' => $data['HarddiskCode'],
			'user_id' => (int)$data['UserID'],  
			'merk' => $data['Merk'], 
			'type' => $data['Type'],
			'capacity' => $data['Capacity'], 
			'warranty' => $data['Warranty'], 
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataHarddisk($token, $data, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/harddisk/".$data['harddisk_id'];

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array( 
			'harddisk_code' => $data['HarddiskCode'],
			'user_id' => (int)$data['UserID'],  
			'merk' => $data['Merk'], 
			'type' => $data['Type'],
			'capacity' => $data['Capacity'], 
			'warranty' => $data['Warranty'], 
			'remark' => $data['Remark'],
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
 
	public function DeleteHarddisk($token, $id, $HDDNewName, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/harddisk/".$id;  

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(  
			'harddisk_code' => $HDDNewName,
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
} 