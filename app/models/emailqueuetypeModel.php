<?php

class emailqueuetypeModel { 
	private $ID;
	private $EmailQueueTypeName;
	private $TableReference;
	private $FieldReference;
	private $RecipientName;
	private $EmailRecipient;
	private $EmailCC;
	private $EmailSubject;
	private $EmailBody;
	private $ParamValue; 
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;   
    
	public function __construct() {}  

    public function getDataEmailQueueTypes($token){
        $ch = curl_init();

        $url = "http://localhost:8080/api/EmailQueueType/";

        $header = array(
            'Authorization: '.$token
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }

    public function getDataEmailQueueType($token, $id){
        $ch = curl_init();

        $url = "http://localhost:8080/api/EmailQueueType/".$id;

        $header = array(
            'Authorization: '.$token
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }

    public function getExcDataEmailQueueType($token, $id){
        $ch = curl_init();

        $url = "http://localhost:8080/api/EmailQueueType/exc/".$id;

        $header = array(
            'Authorization: '.$token
        );

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    } 
    
    public function CreateEmailQueueType($token, $data, $ID, $Date){
        $ch = curl_init();

        $url = "http://localhost:8080/api/EmailQueueType/";

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'email_queue_type_name' => $data['EmailQueueTypeName'],
			'table_reference' => $data['TableReference'],
			'field_reference' => $data['FieldReference'],
			'recipient_name' => $data['RecipientName'],
			'email_recipient' => $data['EmailRecipient'],
			'email_cc' => $data['EmailCC'],
			'email_subject' => $data['EmailSubject'],
			'email_body' => $data['EmailBody'],
			'param_value' => $data['ParamValue'],
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date  
		);

		$data_json = json_encode($data_array); 

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }

    public function UpdateDataEmailQueueType($token, $data, $ID, $Date){
        $ch = curl_init();

        $url = "http://localhost:8080/api/EmailQueueType/".$data['email_queue_type_id'];

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'email_queue_type_name' => $data['EmailQueueTypeName'],
			'table_reference' => $data['TableReference'],
			'field_reference' => $data['FieldReference'],
			'recipient_name' => $data['RecipientName'],
			'email_recipient' => $data['EmailRecipient'],
			'email_cc' => $data['EmailCC'],
			'email_subject' => $data['EmailSubject'],
			'email_body' => $data['EmailBody'],
			'param_value' => $data['ParamValue'], 
			'remark' => $data['Remark'], 
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array); 

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }
 
    public function DeleteEmailQueueType($token, $id, $EmailQueueTypeNewName, $ID, $Date){
        $ch = curl_init();

        $url = "http://localhost:8080/api/EmailQueueType/".$id;  

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
        $data_array = array( 
			'email_queue_type_name' => $EmailQueueTypeNewName,
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
        );

		$data_json = json_encode($data_array);

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

        $resp = curl_exec($ch);

        if($e = curl_error($ch)){
            echo $e;
        }
        else{
            $decoded =json_decode($resp, true);
            return $decoded;
        }
        curl_close($ch);
    }
}