<?php

class computerModel{ 
	private $ID;
	private $UserID;
	private $RoleID;
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;   

	public function __construct() {} 

	public function getDataComputers($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Computer/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataComputer($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Computer/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataComputerByUserID($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Computer/byUserId/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
 
	public function getExcDataComputer($token, $id, $id2){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Computer/exc/".$id."/".$id2;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getExcDataComputerOnly($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/Computer/excOnly/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function CreateComputer($token, $data, $CD, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Computer/";

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(
			'computer_code' => $data['ComputerCode'],
			'user_id' => (int)$data['Username'],
			'processor_id' => (int)$data['ProcessorName'], 
			'motherboard_id' => (int)$data['MotherboardName'], 
			'ram_id' => (int)$data['RAMName'], 
			'hdd_id' => (int)$data['HDDName'], 
			'cd' => (bool)$CD, 
			'casing_id' => (int)$data['CasingName'], 
			'keyboard_id' => (int)$data['KeyboardName'], 
			'mouse_id' => (int)$data['MouseName'], 
			'monitor_id' => (int)$data['MonitorName'], 
			'os_id' => (int)$data['OSName'], 
			'product_key_os' => $data['ProductKeyOS'],
			'office_id' => (int)$data['OfficeName'], 
			'product_key_office' => $data['ProductKeyOffice'],
			'remark' => $data['Remark'],
			'created_user_id'=> (int)$ID,
			'updated_user_id' => (int)$ID, 
			'created_at' => (int)$Date, 
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function UpdateDataComputer($token, $data, $CD, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Computer/".$data['computer_id'];

        $header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);

		$data_array = array(
			'computer_code' => $data['ComputerCode'],
			'user_id' => (int)$data['Username'],
			'processor_id' => (int)$data['ProcessorName'], 
			'motherboard_id' => (int)$data['MotherboardName'], 
			'ram_id' => (int)$data['RAMName'], 
			'hdd_id' => (int)$data['HDDName'], 
			'cd' => (bool)$CD, 
			'casing_id' => (int)$data['CasingName'], 
			'keyboard_id' => (int)$data['KeyboardName'], 
			'mouse_id' => (int)$data['MouseName'], 
			'monitor_id' => (int)$data['MonitorName'], 
			'os_id' => (int)$data['OSName'], 
			'product_key_os' => $data['ProductKeyOS'],
			'office_id' => (int)$data['OfficeName'], 
			'product_key_office' => $data['ProductKeyOffice'],
			'remark' => $data['Remark'],
			'updated_user_id' => (int)$ID,  
			'updated_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
 
	public function DeleteComputer($token, $id, $ComputerNewCode, $ID, $Date){
		$ch = curl_init();

		$url = "http://localhost:8080/api/Computer/".$id;  

		$header = array(
			'Authorization: '.$token,
			'Accept: application/json',
			'Content-Type: application/json'
		);
		
		$data_array = array(  
			'computer_code' => $ComputerNewCode,
			'deleted_user_id' => (int)$ID,  
			'deleted_at'=> (int)$Date 
		);

		$data_json = json_encode($data_array);  

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
} 