<?php

class emailqueuereferenceModel { 
	private $ID;
	private $EmailQueueID;
	private $EmailQueueTypeID;
	private $ReferenceID;  
	private $Remark;
	private $CreatedUserID;
	private $UpdatedUserID;
	private $DeletedUserID;
	private $CreatedAt;
	private $UpdatedAt;
	private $DeletedAt;    

	public function __construct() {} 

	public function getDataEmailQueueReferences($token){
		$ch = curl_init();

		$url = "http://localhost:8080/api/EmailQueueReference/";

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}

	public function getDataEmailQueueReference($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/EmailQueueReference/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	}
 
	public function getExcDataEmailQueueReference($token, $id){
		$ch = curl_init();

		$url = "http://localhost:8080/api/EmailQueueReference/exc/".$id;

		$header = array(
			'Authorization: '.$token
		);

		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);		
		curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

		$resp = curl_exec($ch);

		if($e = curl_error($ch)){
			echo $e;
		}
		else{
			$decoded =json_decode($resp, true);
			return $decoded;
		}
		curl_close($ch);
	} 
	
	// public function CreateEmailQueueReference($token, $data, $ID, $Date){
	// 	$ch = curl_init();

	// 	$url = "http://localhost:8080/api/EmailQueueReference/";

	// 	$header = array(
	// 		'Authorization: '.$token,
	// 		'Accept: application/json',
	// 		'Content-Type: application/json'
	// 	);
		
	// 	$data_array = array(
	// 		'division_id' => (int)$data['DivisionID'],
	// 		'EmailQueueReference_name' => $data['EmailQueueReferenceName'],
	// 		'remark' => $data['Remark'],
	// 		'created_user_id'=> (int)$ID,
	// 		'updated_user_id' => (int)$ID, 
	// 		'created_at' => (int)$Date, 
	// 		'updated_at'=> (int)$Date 
	// 	);

	// 	$data_json = json_encode($data_array);  

	// 	curl_setopt($ch, CURLOPT_URL, $url);
	// 	curl_setopt($ch, CURLOPT_POST, true);
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);	
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

	// 	$resp = curl_exec($ch);

	// 	if($e = curl_error($ch)){
	// 		echo $e;
	// 	}
	// 	else{
	// 		$decoded =json_decode($resp, true);
	// 		return $decoded;
	// 	}
	// 	curl_close($ch);
	// }

	// public function UpdateDataEmailQueueReference($token, $data, $ID, $Date){
	// 	$ch = curl_init();

	// 	$url = "http://localhost:8080/api/EmailQueueReference/".$data['EmailQueueReference_id'];

	// 	$header = array(
	// 		'Authorization: '.$token,
	// 		'Accept: application/json',
	// 		'Content-Type: application/json'
	// 	);

	// 	$data_array = array( 
	// 		'division_id' => (int)$data['DivisionID'],
	// 		'EmailQueueReference_name' => $data['EmailQueueReferenceName'],
	// 		'remark' => $data['Remark'],
	// 		'updated_user_id' => (int)$ID,  
	// 		'updated_at'=> (int)$Date  
	// 	); 

	// 	$data_json = json_encode($data_array);  

	// 	curl_setopt($ch, CURLOPT_URL, $url);
	// 	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'PUT');
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

	// 	$resp = curl_exec($ch);

	// 	if($e = curl_error($ch)){
	// 		echo $e;
	// 	}
	// 	else{
	// 		$decoded =json_decode($resp, true);
	// 		return $decoded;
	// 	}
	// 	curl_close($ch);
	// }

	// public function DeleteEmailQueueReference($token, $id, $EmailQueueReferenceNewName, $ID, $Date){
	// 	$ch = curl_init();

	// 	$url = "http://localhost:8080/api/EmailQueueReference/".$id;  

	// 	$header = array(
	// 		'Authorization: '.$token,
	// 		'Accept: application/json',
	// 		'Content-Type: application/json'
	// 	);

	// 	$data_array = array( 
	// 		'EmailQueueReference_name' => $EmailQueueReferenceNewName,
	// 		'deleted_user_id' => (int)$ID,  
	// 		'deleted_at'=> (int)$Date 
	// 	);

	// 	$data_json = json_encode($data_array);

	// 	curl_setopt($ch, CURLOPT_URL, $url);
	// 	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE'); 
	// 	curl_setopt($ch, CURLOPT_POSTFIELDS, $data_json);
	// 	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	// 	curl_setopt($ch, CURLOPT_HTTPHEADER, $header);

	// 	$resp = curl_exec($ch);

	// 	if($e = curl_error($ch)){
	// 		echo $e;
	// 	}
	// 	else{
	// 		$decoded =json_decode($resp, true);
	// 		return $decoded;
	// 	}
	// 	curl_close($ch);
	// }
} 