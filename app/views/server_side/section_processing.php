<?php  
  
if(isset($data['token'])){
    $token = $data['token']; 
  
    $columns = array( 
        0 => 'division_name',
        1 => 'department_name',
        2 => 'section_name',
        3 => 'remark', 
        4 => 'id', 
    );
 
    $datacount  = $this->model('sectionModel')->countDataSection($token); 
    $totalData = $datacount['data']; 
             
    $totalFiltered = $totalData;  

    $limit = $_POST['length'];
    $offset = $_POST['start'];
    $order = $columns[$_POST['order']['0']['column']];
    $dir = $_POST['order']['0']['dir'];  
    if(empty($_POST['search']['value']))
    {           
        $query = $this->model('sectionModel')->getDataSectionOffset($token, $limit, $offset, $order, $dir); 
    }
    else {
        $search = $_POST['search']['value']; 
        $query = $this->model('sectionModel')->searchDataSections($token, $limit, $offset, $order, $dir, $search); 
        

        $querycount = $this->model('sectionModel')->CountSearchSection($token, $search); 
        $datacount = $querycount['data'];
        $totalFiltered = $datacount;
    }
    $dataa = array();
    if(!empty($query))
    { 
        $no = $offset;
        if(empty($data['Access'])){}
        else{ 
            for($x = 0; $x< count($data['Access']["data"]); $x++)
            { 
                if($data['Access']["data"][$x]["form_php"] == $data['PHP']){  
                    for($i = 0; $i< count($query['data']); $i++)
                    { 
                        $editId = base_url.'/Section/edit/'.$query['data'][$i]['section_id'];
                        $deleteId = base_url.'/Section/deleteSection/'.$query['data'][$i]['section_id']; 
                        $nestedData['division_name'] = $query['data'][$i]['division_name'];
                        $nestedData['department_name'] = $query['data'][$i]['department_name'];
                        $nestedData['section_name'] = $query['data'][$i]['section_name'];
                        $nestedData['remark'] = $query['data'][$i]['remark'];
                        if($data['Access']["data"][$x]["update_flag"] == true OR $data['Access']["data"][$x]["delete_flag"] == true) {  
                            if($data['Access']["data"][$x]["update_flag"] == true) {
                                $nestedButtonEdit = "<a href='$editId' class='btn-warning btn-sm'>Edit</a>&nbsp;";
                            }else{
                                $nestedButtonEdit = "";
                            }
                            if($data['Access']["data"][$x]["delete_flag"] == true) { 
                                $nestedButtonDelete = "<a href='#myModal' data-href='$deleteId' class='btn-danger btn-sm' data-toggle='modal' data-target='#myModal'>Delete</a>";
                            }else{
                                $nestedButtonDelete = "";
                            }
                        }
                        $nestedData['aksi'] = $nestedButtonEdit.$nestedButtonDelete;
                        $dataa[] = $nestedData;
                        $no++;
                    }
                }
            }
        }
    }
        
    $json_data = array(
        "draw"            => intval($_POST['draw']),  
        "recordsTotal"    => intval($totalData),  
        "recordsFiltered" => intval($totalFiltered), 
        "data"            => $dataa  
    );
            
    echo json_encode($json_data); 
 
}
?>