<?php  
  
if(isset($data['token'])){
    $token = $data['token']; 
  
    $columns = array( 
        0 => 'form_type_code',
        1 => 'form_type_description', 
        2 => 'remark', 
        3 => 'id', 
    );
 
    $datacount  = $this->model('formtypeModel')->countDataFormType($token); 
    $totalData = $datacount['data']; 
             
    $totalFiltered = $totalData;  

    $limit = $_POST['length'];
    $offset = $_POST['start'];
    $order = $columns[$_POST['order']['0']['column']];
    $dir = $_POST['order']['0']['dir'];  
    if(empty($_POST['search']['value']))
    {           
        $query = $this->model('formtypeModel')->getDataFormTypeOffset($token, $limit, $offset, $order, $dir); 
    }
    else {
        $search = $_POST['search']['value']; 
        $query = $this->model('formtypeModel')->searchDataFormTypes($token, $limit, $offset, $order, $dir, $search); 
        

        $querycount = $this->model('formtypeModel')->CountSearchFormType($token, $search); 
        $datacount = $querycount['data'];
        $totalFiltered = $datacount;
    }
    $dataa = array();
    if(!empty($query))
    { 
        $no = $offset;
        if(empty($data['Access'])){}
        else{ 
            for($x = 0; $x< count($data['Access']["data"]); $x++)
            { 
                if($data['Access']["data"][$x]["form_php"] == $data['PHP']){  
                    for($i = 0; $i< count($query['data']); $i++)
                    { 
                        $editId = base_url.'/FormType/edit/'.$query['data'][$i]['form_type_id'];
                        $deleteId = base_url.'/FormType/deleteFormType/'.$query['data'][$i]['form_type_id']; 
                        $nestedData['form_type_code'] = $query['data'][$i]['form_type_code'];
                        $nestedData['form_type_description'] = $query['data'][$i]['form_type_description'];
                        $nestedData['remark'] = $query['data'][$i]['remark'];
                        if($data['Access']["data"][$x]["update_flag"] == true OR $data['Access']["data"][$x]["delete_flag"] == true) {  
                            if($data['Access']["data"][$x]["update_flag"] == true) {
                                $nestedButtonEdit = "<a href='$editId' class='btn-warning btn-sm'>Edit</a>&nbsp;";
                            }else{
                                $nestedButtonEdit = "";
                            }
                            if($data['Access']["data"][$x]["delete_flag"] == true) { 
                                $nestedButtonDelete = "<a href='#myModal' data-href='$deleteId' class='btn-danger btn-sm' data-toggle='modal' data-target='#myModal'>Delete</a>";
                            }else{
                                $nestedButtonDelete = "";
                            }
                        }
                        $nestedData['aksi'] = $nestedButtonEdit.$nestedButtonDelete;
                        $dataa[] = $nestedData;
                        $no++;
                    }
                }
            }
        }
    }
        
    $json_data = array(
        "draw"            => intval($_POST['draw']),  
        "recordsTotal"    => intval($totalData),  
        "recordsFiltered" => intval($totalFiltered), 
        "data"            => $dataa  
    );
            
    echo json_encode($json_data); 
 
}
?>