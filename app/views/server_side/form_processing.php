<?php  
  
if(isset($data['token'])){
    $token = $data['token']; 
  
    $columns = array( 
        0 => 'form_code',
        1 => 'form_description',
        2 => 'form_type_code', 
        3 => 'form_parent_id', 
        4 => 'sequence_no', 
        5 => 'class_tag', 
        6 => 'remark', 
        7 => 'id', 
    );
 
    $datacount  = $this->model('formModel')->countDataForm($token); 
    $totalData = $datacount['data']; 
             
    $totalFiltered = $totalData;  

    $limit = $_POST['length'];
    $offset = $_POST['start'];
    $order = $columns[$_POST['order']['0']['column']];
    $dir = $_POST['order']['0']['dir'];  
    if(empty($_POST['search']['value']))
    {           
        $query = $this->model('formModel')->getDataFormOffset($token, $limit, $offset, $order, $dir); 
    }
    else {
        $search = $_POST['search']['value']; 
        $query = $this->model('formModel')->searchDataForms($token, $limit, $offset, $order, $dir, $search); 
        

        $querycount = $this->model('formModel')->CountSearchForm($token, $search); 
        $datacount = $querycount['data'];
        $totalFiltered = $datacount;
    }
    $dataa = array();
    if(!empty($query))
    { 
        $no = $offset;
        if(empty($data['Access'])){}
        else{ 
            for($x = 0; $x< count($data['Access']["data"]); $x++)
            { 
                if($data['Access']["data"][$x]["form_php"] == $data['PHP']){  
                    for($i = 0; $i< count($query['data']); $i++)
                    { 
                        $editId = base_url.'/Form/edit/'.$query['data'][$i]['form_id'];
                        $deleteId = base_url.'/Form/deleteForm/'.$query['data'][$i]['form_id']; 
                        $nestedData['form_code'] = $query['data'][$i]['form_code'];
                        $nestedData['form_description'] = $query['data'][$i]['form_description'];
                        $nestedData['form_type_code'] = $query['data'][$i]['form_type_code'];
                        if($query['data'][$i]['form_parent_id'] == 0 ){
                            $formParent = "";
                        }else{
                            $head = $this->model('formModel')->getDataFormHeadDetail($token, $query['data'][$i]['form_parent_id']);
                            $formParent = $head["data"]["form_code"];
                        }
                        $nestedData['form_parent_id'] = $formParent;
                        $nestedData['sequence_no'] = $query['data'][$i]['sequence_no']; 
                        $nestedData['class_tag'] = $query['data'][$i]['class_tag']; 
                        $nestedData['remark'] = $query['data'][$i]['remark'];
                        if($data['Access']["data"][$x]["update_flag"] == true OR $data['Access']["data"][$x]["delete_flag"] == true) {  
                            if($data['Access']["data"][$x]["update_flag"] == true) {
                                $nestedButtonEdit = "<a href='$editId' class='btn-warning btn-sm'>Edit</a>&nbsp;";
                            }else{
                                $nestedButtonEdit = "";
                            }
                            if($data['Access']["data"][$x]["delete_flag"] == true) { 
                                $nestedButtonDelete = "<a href='#myModal' data-href='$deleteId' class='btn-danger btn-sm' data-toggle='modal' data-target='#myModal'>Delete</a>";
                            }else{
                                $nestedButtonDelete = "";
                            }
                        }
                        $nestedData['aksi'] = $nestedButtonEdit.$nestedButtonDelete;
                        $dataa[] = $nestedData;
                        $no++;
                    }
                }
            }
        }
    }
        
    $json_data = array(
        "draw"            => intval($_POST['draw']),  
        "recordsTotal"    => intval($totalData),  
        "recordsFiltered" => intval($totalFiltered), 
        "data"            => $dataa  
    );
            
    echo json_encode($json_data); 
 
}
?>  