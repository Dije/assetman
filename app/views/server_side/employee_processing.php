<?php  
  
if(isset($data['token'])){
    $token = $data['token']; 
  
    $columns = array( 
        0 => 'nik',
        1 => 'firstname',
        2 => 'lastname', 
        3 => 'division_name', 
        4 => 'department_name', 
        5 => 'section_name', 
        6 => 'position_name', 
        7 => 'remark', 
        8 => 'id',
    );
 
    $datacount  = $this->model('employeeModel')->countDataEmployee($token); 
    $totalData = $datacount['data']; 
             
    $totalFiltered = $totalData;  

    $limit = $_POST['length'];
    $offset = $_POST['start'];
    $order = $columns[$_POST['order']['0']['column']];
    $dir = $_POST['order']['0']['dir'];  
    if(empty($_POST['search']['value']))
    {           
        $query = $this->model('employeeModel')->getDataEmployeeOffset($token, $limit, $offset, $order, $dir); 
    }
    else {
        $search = $_POST['search']['value']; 
        $query = $this->model('employeeModel')->searchDataEmployees($token, $limit, $offset, $order, $dir, $search); 
        

        $querycount = $this->model('employeeModel')->CountSearchEmployee($token, $search); 
        $datacount = $querycount['data'];
        $totalFiltered = $datacount;
    }
    $dataa = array();
    if(!empty($query))
    { 
        $no = $offset;
        if(empty($data['Access'])){}
        else{ 
            for($x = 0; $x< count($data['Access']["data"]); $x++)
            { 
                if($data['Access']["data"][$x]["form_php"] == $data['PHP']){  
                    for($i = 0; $i< count($query['data']); $i++)
                    { 
                        $editId = base_url.'/Employee/edit/'.$query['data'][$i]['employee_id'];
                        $deleteId = base_url.'/Employee/deleteEmployee/'.$query['data'][$i]['employee_id']; 
                        $nestedData['nik'] = $query['data'][$i]['nik'];
                        $nestedData['firstname'] = $query['data'][$i]['first_name'];
                        $nestedData['lastname'] = $query['data'][$i]['last_name'];
                        $nestedData['division_name'] = $query['data'][$i]['division_name'];
                        $nestedData['department_name'] = $query['data'][$i]['department_name'];
                        $nestedData['section_name'] = $query['data'][$i]['section_name'];
                        $nestedData['position_name'] = $query['data'][$i]['position_name'];
                        $nestedData['remark'] = $query['data'][$i]['remark'];
                        if($data['Access']["data"][$x]["update_flag"] == true OR $data['Access']["data"][$x]["delete_flag"] == true) {  
                            if($data['Access']["data"][$x]["update_flag"] == true) {
                                $nestedButtonEdit = "<a href='$editId' class='btn-warning btn-sm'>Edit</a>&nbsp;";
                            }else{
                                $nestedButtonEdit = "";
                            }
                            if($data['Access']["data"][$x]["delete_flag"] == true) { 
                                $nestedButtonDelete = "<a href='#myModal' data-href='$deleteId' class='btn-danger btn-sm' data-toggle='modal' data-target='#myModal'>Delete</a>";
                            }else{
                                $nestedButtonDelete = "";
                            }
                        }
                        $nestedData['aksi'] = $nestedButtonEdit.$nestedButtonDelete;
                        $dataa[] = $nestedData;
                        $no++;
                    }
                }
            }
        }
    }
        
    $json_data = array(
        "draw"            => intval($_POST['draw']),  
        "recordsTotal"    => intval($totalData),  
        "recordsFiltered" => intval($totalFiltered), 
        "data"            => $dataa  
    );
            
    echo json_encode($json_data); 
 
}
?>