<?php  
  
if(isset($data['token'])){
    $token = $data['token']; 
  
    $columns = array( 
        0 => 'role_code',
        1 => 'form_code',
        2 => 'create_flag', 
        3 => 'read_flag', 
        4 => 'update_flag', 
        5 => 'delete_flag', 
        6 => 'remark', 
        7 => 'id', 
    );
 
    $datacount  = $this->model('roleformModel')->countDataRoleForm($token); 
    $totalData = $datacount['data']; 
             
    $totalFiltered = $totalData;  

    $limit = $_POST['length'];
    $offset = $_POST['start'];
    $order = $columns[$_POST['order']['0']['column']];
    $dir = $_POST['order']['0']['dir'];  
    if(empty($_POST['search']['value']))
    {           
        $query = $this->model('roleformModel')->getDataRoleFormOffset($token, $limit, $offset, $order, $dir); 
    }
    else {
        $search = $_POST['search']['value']; 
        $query = $this->model('roleformModel')->searchDataRoleForms($token, $limit, $offset, $order, $dir, $search); 
        

        $querycount = $this->model('roleformModel')->CountSearchRoleForm($token, $search); 
        $datacount = $querycount['data'];
        $totalFiltered = $datacount;
    }
    $dataa = array();
    if(!empty($query))
    { 
        $no = $offset;
        if(empty($data['Access'])){}
        else{ 
            for($x = 0; $x< count($data['Access']["data"]); $x++)
            { 
                if($data['Access']["data"][$x]["form_php"] == $data['PHP']){  
                    for($i = 0; $i< count($query['data']); $i++)
                    { 
                        $editId = base_url.'/RoleForm/edit/'.$query['data'][$i]['role_form_id'];
                        $deleteId = base_url.'/RoleForm/deleteRoleForm/'.$query['data'][$i]['role_form_id']; 
                        $nestedData['role_code'] = $query['data'][$i]['role_code'];
                        $nestedData['form_code'] = $query['data'][$i]['form_code'];
                         if($query['data'][$i]["create_flag"] == true) { 
                            $create = "TRUE";
                         }else{ 
                            $create = "FALSE";
                         } 
                         $nestedData['create_flag'] = $create;
                         if($query['data'][$i]["read_flag"] == true) { 
                            $read = "TRUE";
                         }else{ 
                            $read = "FALSE";
                         } 
                         $nestedData['read_flag'] = $read;
                         if($query['data'][$i]["update_flag"] == true) { 
                            $update = "TRUE";
                         }else{ 
                            $update = "FALSE";
                         } 
                         $nestedData['update_flag'] = $update;
                         if($query['data'][$i]["delete_flag"] == true) { 
                            $delete = "TRUE";
                         }else{ 
                            $delete = "FALSE";
                         } 
                         $nestedData['delete_flag'] = $delete;
                        $nestedData['remark'] = $query['data'][$i]['remark'];
                        if($data['Access']["data"][$x]["update_flag"] == true OR $data['Access']["data"][$x]["delete_flag"] == true) {  
                            if($data['Access']["data"][$x]["update_flag"] == true) {
                                $nestedButtonEdit = "<a href='$editId' class='btn-warning btn-sm'>Edit</a>&nbsp;";
                            }else{
                                $nestedButtonEdit = "";
                            }
                            if($data['Access']["data"][$x]["delete_flag"] == true) { 
                                $nestedButtonDelete = "<a href='#myModal' data-href='$deleteId' class='btn-danger btn-sm' data-toggle='modal' data-target='#myModal'>Delete</a>";
                            }else{
                                $nestedButtonDelete = "";
                            }
                        }
                        $nestedData['aksi'] = $nestedButtonEdit.$nestedButtonDelete;
                        $dataa[] = $nestedData;
                        $no++;
                    }
                }
            }
        }
    }
        
    $json_data = array(
        "draw"            => intval($_POST['draw']),  
        "recordsTotal"    => intval($totalData),  
        "recordsFiltered" => intval($totalFiltered), 
        "data"            => $dataa  
    );
            
    echo json_encode($json_data); 
 
}
?>
 