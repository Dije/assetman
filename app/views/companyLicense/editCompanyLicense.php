<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			 
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Edit Data</h3>
												</div>
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/CompanyLicense/updateCompanyLicense" method="POST" name="form1" enctype="multipart/form-data" > 
												<div class="row">
										<!-- left column -->
										<div class="col-md-6">
													<div class="card-body"> 
													<input type="hidden" name="company_id" class="form-control"  placeholder="Enter Company Name" value="<?php echo $data['companyLicense']["data"]["company_id"]; ?>">
													<input type="hidden" name="company_license_id" class="form-control"  placeholder="Enter Company Name" value="<?php echo $data['companyLicense']["data"]["company_license_id"]; ?>">
													<?php $Company = $this->model('companyModel')->getDataCompany($token, $data['companyLicense']["data"]["company_id"]); ?>
														<div class="form-group">
															<label for="exampleInputEmail1">Company</label> 
															<input type="text" name="companytemp" class="form-control" id="exampleInputOSName" placeholder="Enter License No" value="<?php echo $Company["data"]["company_name"]; ?>" disabled>
														</div>  
														<div class="form-group">
															<label for="exampleInputEmail1">Business Unit</label> 
															<input type="text" name="businessunittemp" class="form-control" id="exampleInputOSName" placeholder="Enter License No" value="<?php echo $Company["data"]["business_unit_name"]; ?>" disabled>
														</div>  
														<div class="form-group">
															<label for="exampleInputEmail1">License Type*</label>
															<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																<input type="hidden" name="LicenseTypeID" class="form-control"  value="<?php echo $data['companyLicense']["data"]["license_type_id"]; ?>">
															<select name="LicenseTypeIDshow" class="form-control select2" style="width: 100%;" disabled>  
																<option value="<?php echo $data['companyLicense']["data"]["license_type_id"]; ?>"><?php echo $data['companyLicense']["data"]["license_type_name"]; ?></option> 
															</select>  
															<?php } else{ ?>
															<select name="LicenseTypeID" class="form-control select2" style="width: 100%;">  
																<option value="<?php echo $data['companyLicense']["data"]["license_type_id"]; ?>"><?php echo $data['companyLicense']["data"]["license_type_name"]; ?></option> 
																<?php for ($x = 0; $x < count($data['licenseType']['data']); $x++) { ?>  
																	<option value="<?php echo $data['licenseType']["data"][$x]["license_type_id"]; ?>"><?php echo $data['licenseType']["data"][$x]["license_type_name"]; ?></option> 
																<?php } ?>
															</select>  
															<?php } ?>
														</div>     
														<div class="form-group">
															<label for="exampleInputEmail1">License No</label> 
															<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																<input type="hidden" name="LicenseNo" class="form-control" id="exampleInputOSName" placeholder="Enter License No" value="<?php echo $data['companyLicense']["data"]["license_no"]; ?>">
																<input type="text" name="LicenseNoshow" class="form-control" id="exampleInputOSName" placeholder="Enter License No" value="<?php echo $data['companyLicense']["data"]["license_no"]; ?>" disabled>
															<?php } else{ ?>
																<input type="text" name="LicenseNo" class="form-control" id="exampleInputOSName" placeholder="Enter License No" value="<?php echo $data['companyLicense']["data"]["license_no"]; ?>">
															<?php } ?>
														</div>    
															<div class="form-group"> 
														<label for="exampleInputEmail1">Parent License No</label>
															<div class="input-group"> 
																<input type="hidden" id="mydata1" name="ParentLN" class="form-control" value="<?php echo $data['companyLicense']["data"]["parent_license_id"]; ?>">
																<?php if($data['companyLicense']["data"]["parent_license_id"] == 0 ){ ?>
																	<input type="text" id="LicenseNotemp" name="temp" class="form-control" disabled>
																<?php }else{$LName = $this->model('companylicenseModel')->getDataCompanyLicense($token, $data['companyLicense']["data"]["parent_license_id"]); ?>
																<input type="text" id="LicenseNotemp" name="temp" class="form-control" value="<?php echo $LName["data"]["license_no"]; ?>" disabled>
																<?php } ?>
																<span class="input-group-append"> 
																<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																	<input type="button" class="btn btn-info btn-flat" value="&#128269" data-toggle="modal" data-target="#myModal2" data-book-id="<?php echo $data['companyLicense']["data"]["company_id"]; ?>" disabled>
																<?php } else{ ?>
																	<input type="button" class="btn btn-info btn-flat" value="&#128269" data-toggle="modal" data-target="#myModal2" data-book-id="<?php echo $data['companyLicense']["data"]["company_id"]; ?>">
																<?php } ?>
																</span>
															</div>       
															</div>     
															<div class="form-group">
															<div class="form-check">
																<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																<?php if($data['companyLicense']["data"]["renewable"] == true) { ?>
																	<input type="hidden" name="Renewable" class="form-check-input" value="true"> 
																	<input type="checkbox" name="Renewableshow" class="form-check-input" value="true" checked disabled> 
																<?php }else{ ?>
																	<input type="hidden" name="Renewable" class="form-check-input" value="false"> 
																	<input type="checkbox" name="Renewableshow" class="form-check-input" value="false" disabled> 
																<?php } ?>  
																<label class="form-check-label">Renewable</label>
																<?php } else{ ?>
																	<?php if($data['companyLicense']["data"]["renewable"] == true) { ?>
																	<input type="checkbox" name="Renewable" class="form-check-input" value="true" checked>  
																<?php }else{ ?>
																	<input type="checkbox" name="Renewable" class="form-check-input" value="true">  
																<?php } ?>  
																<label class="form-check-label">Renewable</label>
																<?php } ?>
															</div>
														</div>   														   
														<div class="form-group">
															<label for="exampleInputEmail1">Reminder Counter</label> 
															<input type="hidden" id="exampleInputOSName" name="ReminderCounter" class="form-control" value="<?php echo $data['companyLicense']["data"]["reminder_counter"]; ?>">
															<input type="text" name="ReminderCountershow" class="form-control" id="exampleInputOSName" placeholder="Enter Reminder Counter" value="<?php echo $data['companyLicense']["data"]["reminder_counter"]; ?>" disabled>
														</div>    
														<div class="form-group">
															<label for="exampleInputEmail1">Issued By</label>  
																<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																	<input type="hidden" name="IssuedBy" class="form-control" id="exampleInputOSName" placeholder="Enter Issued By" value="<?php echo $data['companyLicense']["data"]["issued_by"]; ?>">
																	<input type="text" name="IssuedByshow" class="form-control" id="exampleInputOSName" placeholder="Enter Issued By" value="<?php echo $data['companyLicense']["data"]["issued_by"]; ?>" disabled>
																<?php }else{ ?>
																	<input type="text" name="IssuedBy" class="form-control" id="exampleInputOSName" placeholder="Enter Issued By" value="<?php echo $data['companyLicense']["data"]["issued_by"]; ?>">
																<?php } ?>
														</div>   
														<div class="form-group">
															<label>Issued Date</label>
																<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																	<div class="input-group date" name="IssuedDate" id="reservationdate" data-target-input="nearest" >
																		<?php if($data['companyLicense']["data"]["issued_date"] != 0) {?>
																		<input type="hidden" id="dateee" name="IssuedDate" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["issued_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate" >
																		<input type="text" id="dateee" name="IssuedDateshow" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["issued_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate" disabled>
																		<?php } else { ?> 
																			<input type="hidden" id="dateee" name="IssuedDate" value="0" class="form-control datetimepicker-input" data-target="#reservationdate1" >
																			<input type="text" id="ksng" name="IssuedDateshow" value="-" class="form-control datetimepicker-input" data-target="#reservationdate1" disabled>
																		<?php } ?>
																		<div class="input-group-append" data-target="#reservationdate">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																		</div>
																	</div>
																<?php }else{ ?>
																	<div class="input-group date" name="IssuedDate" id="reservationdate" data-target-input="nearest" >
																		<?php if($data['companyLicense']["data"]["issued_date"] != 0) {?>
																			<input type="text" id="dateee" name="IssuedDate" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["issued_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate" />
																		<?php } else { ?> 
																			<input type="text" id="dateee" name="IssuedDate" value="" class="form-control datetimepicker-input" data-target="#reservationdate" />
																			<?php } ?>
																		<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																		</div>
																	</div>
																<?php } ?>
														</div>      
														<div class="form-group">
															<label>Expired Date</label>
																<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																	<div class="input-group date" name="ExpiredDate" id="reservationdate1" data-target-input="nearest" >
																		<?php if($data['companyLicense']["data"]["expired_date"] != 0) {?>
																			<input type="hidden" id="datee" name="ExpiredDate" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["expired_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate1" >
																			<input type="text" id="datee" name="ExpiredDateshow" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["expired_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate1" disabled>
																		<?php } else { ?> 
																			<input type="hidden" id="datee" name="ExpiredDate" value="0" class="form-control datetimepicker-input" data-target="#reservationdate1" >
																			<input type="text" id="ksng" name="ExpiredDateshow" value="-" class="form-control datetimepicker-input" data-target="#reservationdate1" disabled>
																		<?php } ?>
																		<div class="input-group-append" data-target="#reservationdate1">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																		</div>
																	</div>
																<?php }else{ ?>
																	<div class="input-group date" name="ExpiredDate" id="reservationdate1" data-target-input="nearest" >
																		<?php if($data['companyLicense']["data"]["expired_date"] != 0) {?>
																			<input type="text" id="datee" name="ExpiredDate" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["expired_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate1" />
																		<?php } else { ?> 
																			<input type="text" id="datee" name="ExpiredDate" value="" class="form-control datetimepicker-input" data-target="#reservationdate1" />
																			<?php } ?>
																		<div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																		</div>
																	</div>
																<?php } ?>
														</div>      
														<div class="form-group">
															<label>Earliest Renewal Date</label>
																<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																	<div class="input-group date" name="EarliestRenewalDate" id="reservationdate2" data-target-input="nearest" >
																		<?php if($data['companyLicense']["data"]["earliest_renewal_date"] != 0) {?>
																			<input type="hidden" id="dateeee" name="EarliestRenewalDate" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["earliest_renewal_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate2" />
																			<input type="text" id="dateeee" name="EarliestRenewalDateshow" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["earliest_renewal_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate2" disabled>
																		<?php } else { ?> 
																			<input type="hidden" id="dateeee" name="EarliestRenewalDate" value="0" class="form-control datetimepicker-input" data-target="#reservationdate1" >
																			<input type="text" id="ksng" name="EarliestRenewalDateshow" value="-" class="form-control datetimepicker-input" data-target="#reservationdate1" disabled>
																		<?php } ?>
																		<div class="input-group-append" data-target="#reservationdate2">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																		</div>
																	</div>
																<?php }else{ ?>
																	<div class="input-group date" name="EarliestRenewalDate" id="reservationdate2" data-target-input="nearest" >
																		<?php if($data['companyLicense']["data"]["earliest_renewal_date"] != 0) {?>
																			<input type="text" id="dateeee" name="EarliestRenewalDate" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["earliest_renewal_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate2" />
																		<?php } else { ?> 
																			<input type="text" id="dateeee" name="EarliestRenewalDate" value="" class="form-control datetimepicker-input" data-target="#reservationdate3" />
																		<?php } ?>
																		<div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																		</div>
																	</div>
																<?php } ?>
														</div>  
													<!-- /.card-body -->
													</div>
																</div> 
																  
										<div class="col-md-6">
													<div class="card-body">   
														<div class="form-group">
															<label>Last Renewal Date</label>
																<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																	<div class="input-group date" name="LastRenewalDate" id="reservationdate3" data-target-input="nearest" >
																		<?php if($data['companyLicense']["data"]["last_renewal_date"] != 0) {?>
																			<input type="hidden" id="dateeeee" name="LastRenewalDate" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["last_renewal_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate3" />
																			<input type="text" id="dateeeee" name="LastRenewalDateshow" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["last_renewal_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate3" disabled>
																		<?php } else { ?> 
																			<input type="hidden" id="dateeeee" name="LastRenewalDate" value="0" class="form-control datetimepicker-input" data-target="#reservationdate1" >
																			<input type="text" id="ksng" name="LastRenewalDateshow" value="-" class="form-control datetimepicker-input" data-target="#reservationdate1" disabled>
																		<?php } ?>
																		<div class="input-group-append" data-target="#reservationdate3">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																		</div>
																	</div>
																<?php }else{ ?>
																	<div class="input-group date" name="LastRenewalDate" id="reservationdate3" data-target-input="nearest" >
																		<?php if($data['companyLicense']["data"]["last_renewal_date"] != 0) {?>
																			<input type="text" id="dateeeee" name="LastRenewalDate" value="<?php echo date("d-m-Y", $data['companyLicense']["data"]["last_renewal_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate3" />
																		<?php } else { ?> 
																			<input type="text" id="dateeeee" name="LastRenewalDate" value="" class="form-control datetimepicker-input" data-target="#reservationdate3" />
																		<?php } ?>
																		<div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																		</div>
																	</div>
																<?php } ?>
														</div>   
														<div class="form-group">
															<label for="exampleInputEmail1">Status</label>
																<?php if($data["userRole"]["data"]["role_code"] == "LEGALSTAFF" && $data["companyLicense"]["data"]["status"] > 2) { ?>
																<input type="hidden" name="Status" class="form-control"  value="<?php echo $data['companyLicense']["data"]["status"]; ?>">
																	<select name="Statusshow" class="form-control select2" style="width: 100%;" disabled>   
																		<?php if($data["companyLicense"]["data"]["status"] == 3){ ?> 
																			<option value="3">Active</option>  
																		<?php }elseif($data["companyLicense"]["data"]["status"] == 4){ ?>
																			<option value="4">Not Active</option>  
																		<?php } ?>
																	</select>    
																<?php }else{ ?>
																	<select name="Status" class="form-control select2" style="width: 100%;">   
																	<?php if($data["companyLicense"]["data"]["status"] == 1){ ?> 
																			<option value="1">Draft</option> 
																			<option value="2">Need Approval</option> 
																			<?php if($data["userRole"]["data"]["role_code"] == "LEGALMGR"){?>
																				<option value="3">Active</option> 
																				<option value="4">Not Active</option> 
																			<?php } ?>
																		<?php }elseif($data["companyLicense"]["data"]["status"] == 2){ ?> 
																			<option value="2">Need Approval</option> 
																			<option value="1">Draft</option> 
																			<?php if($data["userRole"]["data"]["role_code"] == "LEGALMGR"){?>
																				<option value="3">Active</option> 
																				<option value="4">Not Active</option> 
																			<?php } ?>
																		<?php }elseif($data["companyLicense"]["data"]["status"] == 3){ ?> 
																			<option value="3">Active</option> 
																			<option value="1">Draft</option> 
																			<option value="2">Need Approval</option> 
																			<option value="4">Not Active</option> 
																		<?php }elseif($data["companyLicense"]["data"]["status"] == 4){ ?>
																			<option value="4">Not Active</option> 
																			<option value="1">Draft</option> 
																			<option value="2">Need Approval</option> 
																			<option value="3">Active</option> 
																		<?php } ?>
																	</select>    
																<?php } ?>
														</div>  
														<div class="form-group">
															<label for="exampleInputEmail1">Renewal Status</label>
															<select id="Stat" name="RenStatus" class="form-control select2" style="width: 100%;"> 
																<?php if($data["companyLicense"]["data"]["renewal_status"] == 1){ ?> 
																	<option value="1">Open</option>  
																	<option value="2">Follow Up</option>  
																	<option value="3">Renew</option>  
																	<option value="4">Close Permit</option>  
																<?php }elseif($data["companyLicense"]["data"]["renewal_status"] == 2){ ?> 
																	<option value="2">Follow Up</option>  
																	<option value="1">Open</option>  
																	<option value="3">Renew</option>  
																	<option value="4">Close Permit</option>  
																<?php }elseif($data["companyLicense"]["data"]["renewal_status"] == 3){ ?> 
																	<option value="3">Renew</option>  
																	<option value="1">Open</option>  
																	<option value="2">Follow Up</option>  
																	<option value="4">Close Permit</option> 
																<?php }elseif($data["companyLicense"]["data"]["renewal_status"] == 4){ ?>
																	<option value="4">Close Permit</option> 
																	<option value="1">Open</option>  
																	<option value="2">Follow Up</option>  
																	<option value="3">Renew</option>  
																<?php }elseif($data["companyLicense"]["data"]["renewal_status"] == 5){ ?>
																	<option value="5">Renew Approved</option>  
																	<option value="1">Open</option>  
																	<option value="2">Follow Up</option>  
																	<option value="4">Close Permit</option>  
																<?php }elseif($data["companyLicense"]["data"]["renewal_status"] == 6){ ?>
																	<option value="6">Close Permit Approved</option>  
																	<option value="1">Open</option>  
																	<option value="2">Follow Up</option>  
																	<option value="3">Renew</option>   
																<?php } ?>
															</select>     
														</div> 
														<div class="form-group">
															<label for="exampleInputEmail1">Approved By</label> 
															<?php if($data["companyLicense"]["data"]["approved_user_id"] == 0) { ?>
															<input type="text" name="ApprovedBy" class="form-control" id="exampleInputOSName" placeholder="" value="" disabled>
															<?php }else{ $UserApp = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"]["approved_user_id"])?>
															<input type="text" name="ApprovedBy" class="form-control" id="exampleInputOSName" placeholder="Enter Approved By" value="<?php echo $UserApp["data"]["username"]; ?>" disabled>
															<?php }?> 
														</div>   
														<div class="form-group">
															<label for="exampleInputEmail1">Approved Date</label> 
															<?php if($data["companyLicense"]["data"]["approved_date"] == 0) { ?>
															<input type="text" name="ApprovedDate" class="form-control" id="exampleInputOSName" placeholder="" value="" disabled>
															<?php }else{ ?>
															<input type="text" name="ApprovedDate" class="form-control" id="exampleInputOSName" placeholder="Enter Approved Date" value="<?php echo date("d-m-Y", $data["companyLicense"]["data"]["approved_date"]); ?>" disabled>
															<?php }?>
														</div>   
														<div class="form-group">
															<label for="exampleInputEmail1">Renewal Approved By</label> 
															<?php if($data["companyLicense"]["data"]["renewal_approved_user_id"] == 0) { ?>
															<input type="text" name="RenewalApprovedBy" class="form-control" id="exampleInputOSName" placeholder="" value="" disabled>
															<?php }else{ $UserRen = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"]["renewal_approved_user_id"])?>
															<input type="text" name="RenewalApprovedBy" class="form-control" id="exampleInputOSName" placeholder="Enter Approved Date" value="<?php echo $UserRen["data"]["username"]; ?>" disabled>
															<?php }?> 
														</div>   
														<div class="form-group">
															<label for="exampleInputEmail1">Renewal Approved Date</label> 
															<?php if($data["companyLicense"]["data"]["renewal_approved_date"] == 0) { ?>
															<input type="text" name="RenewalApprovedBy" class="form-control" id="exampleInputOSName" placeholder="" value="" disabled>
															<?php }else{ ?>
															<input type="text" name="RenewalApprovedBy" class="form-control" id="exampleInputOSName" placeholder="Enter Approved Date" value="<?php echo date("d-m-Y", $data["companyLicense"]["data"]["renewal_approved_date"]); ?>" disabled>
															<?php }?> 
														</div>   
														<div class="form-group">
															<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"><?php echo $data["companyLicense"]["data"]["remark"]; ?></textarea>  
														</div>  														
														</div>
																</div>
																</div>

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<?php $id = $data["companyLicense"]["data"]["company_license_id"];
														$CompanyID = $data["companyLicense"]["data"]["company_id"];
														if(($data["companyLicense"]["data"]["status"] == 2 || $data["companyLicense"]["data"]["renewal_status"] == 3) && $data["userRole"]["data"]["role_code"] == "LEGALMGR") {  ?>
														<input type="button" name="cancel" class="btn btn-danger" value="Approve" onclick="approveLicense(<?php echo $id; ?>)" />
														<?php } 
														if($data["companyLicense"]["data"]["renewal_status"] == 4 && $data["userRole"]["data"]["role_code"] == "LEGALMGR") {  ?> 
															<input type="button" name="cancel" class="btn btn-danger" value="Approve" data-toggle="modal" data-target="#myModal3" />
														<?php } 
														if($data["companyLicense"]["data"]["status"] != 4 && $data["userRole"]["data"]["role_code"] == "LEGALMGR") { ?>
														<input type="button" name="cancel" class="btn btn-danger" value="Deactivate" onclick="deactivateLicense(<?php echo $id; ?>)" />
														<?php } ?>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="license()">
													</div>
												</form> 
											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
							<!-- /.content -->   
					</div>
					<!-- /.content -->  

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>
 