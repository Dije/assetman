<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			
			<section class="content">
				<div class="container-fluid">
					<div class="row">
						<!-- left column -->
						<div class="col-md-12">
							<!-- general form elements -->
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Create New Data</h3>
								</div>
								<!-- /.card-header -->
								<!-- form start -->
								<form action="<?= base_url; ?>/CompanyLicense/addCompanyLicense" method="POST" name="form1"> 
									
									<div class="row">
										<!-- left column -->
										<div class="col-md-6">
											<div class="card-body"> 
												<input type="hidden" name="company_id" class="form-control" id="companyid" placeholder="Enter Company Name" value="<?php echo $data["CompanyID"]; ?>">
												<div class="form-group">
													<label for="exampleInputEmail1">License Type*</label>
													<select name="LicenseTypeID" class="form-control select2" style="width: 100%;"> 
														<?php for ($x = 0; $x < count($data['licenseType']['data']); $x++) { ?>  
															<option value="<?php echo $data['licenseType']["data"][$x]["license_type_id"]; ?>"><?php echo $data['licenseType']["data"][$x]["license_type_name"]; ?></option> 
														<?php } ?>
													</select>  
												</div>     
												<div class="form-group">
													<label for="exampleInputEmail1">License No</label> 
													<input type="text" name="LicenseNo" class="form-control" id="exampleInputOSName" placeholder="Enter License No">
												</div>    
												<div class="form-group"> 
													<label for="exampleInputEmail1">Parent License No</label>
													<div class="input-group"> 
														<input type="hidden" id="mydata1" name="ParentLN" class="form-control">
														<input type="text" id="LicenseNotemp" name="temp" class="form-control" disabled>
														<span class="input-group-append"> 
															<input type="button" class="btn btn-info btn-flat" value="&#128269" data-toggle="modal" data-target="#myModal2" data-book-id="<?php echo $data["CompanyID"]; ?>">
														</span>
													</div>       
												</div>     
												<div class="form-group">
													<div class="form-check">
														<input type="checkbox" name="Renewable" class="form-check-input" value="true">
														<label class="form-check-label">Renewable</label>
													</div>
												</div>   														   
												<div class="form-group">
													<label for="exampleInputEmail1">Reminder Counter</label> 
													<input type="hidden" name="ReminderCounter" class="form-control" id="exampleInputOSName" placeholder="Enter Reminder Counter">
													<input type="text" name="ReminderCountershow" class="form-control" id="exampleInputOSName" disabled>
												</div>    
												<div class="form-group">
													<label for="exampleInputEmail1">Issued By</label> 
													<input type="text" name="IssuedBy" class="form-control" id="exampleInputOSName" placeholder="Enter Issued By">
												</div>   
												<div class="form-group">
													<label>Issued Date</label>
													<div class="input-group date" name="IssuedDate" id="reservationdate" data-target-input="nearest" >
														<input type="text" id="dateee" name="IssuedDate" class="form-control datetimepicker-input" data-target="#reservationdate" />
														<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
															<div class="input-group-text"><i class="fa fa-calendar"></i></div>
														</div>
													</div>
												</div>      
												<div class="form-group">
													<label>Expired Date</label>
													<div class="input-group date" name="ExpiredDate" id="reservationdate1" data-target-input="nearest" >
														<input type="text" id="datee" name="ExpiredDate" class="form-control datetimepicker-input" data-target="#reservationdate1" />
														<div class="input-group-append" data-target="#reservationdate1" data-toggle="datetimepicker">
															<div class="input-group-text"><i class="fa fa-calendar"></i></div>
														</div>
													</div>
												</div>      
												<div class="form-group">
													<label>Earliest Renewal Date</label>
													<div class="input-group date" name="EarliestRenewalDate" id="reservationdate2" data-target-input="nearest" >
														<input type="text" id="dateeee" name="EarliestRenewalDate" class="form-control datetimepicker-input" data-target="#reservationdate2" />
														<div class="input-group-append" data-target="#reservationdate2" data-toggle="datetimepicker">
															<div class="input-group-text"><i class="fa fa-calendar"></i></div>
														</div>
													</div>
												</div>  
												<!-- /.card-body -->
											</div>
										</div> 
										
										<div class="col-md-6">
											<div class="card-body">   
												<div class="form-group">
													<label>Last Renewal Date</label>
													<div class="input-group date" name="LastRenewalDate" id="reservationdate3" data-target-input="nearest" >
														<input type="text" id="dateeeee" name="LastRenewalDate" class="form-control datetimepicker-input" data-target="#reservationdate3" />
														<div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
															<div class="input-group-text"><i class="fa fa-calendar"></i></div>
														</div>
													</div>
												</div>   
												<div class="form-group">
													<label for="exampleInputEmail1">Status</label>
													<select id="Stat" name="Status" class="form-control select2" style="width: 100%;">  
														<option value="1">Draft</option>  
														<option value="2">Need Approval</option>  
														<option value="3">Active</option>  
														<option value="4">Not Active</option>  
													</select>  
												</div>  
												<div class="form-group">
													<label for="exampleInputEmail1">Renewal Status</label>
													<select id="Stat" name="RenStatus" class="form-control select2" style="width: 100%;">  
														<option value="1">Open</option>  
														<option value="2">Follow Up</option>  
														<option value="3">Renew</option>  
														<option value="4">Close Permit</option>  
													</select>  
												</div> 
												<div class="form-group">
													<label for="exampleInputEmail1">Approved By</label> 
													<input type="text" name="ApprovedBy" class="form-control" id="exampleInputOSName" placeholder="" disabled>
												</div>   
												<div class="form-group">
													<label for="exampleInputEmail1">Approved Date</label> 
													<input type="text" name="ApprovedDate" class="form-control" id="exampleInputOSName" placeholder="" disabled>
												</div>   
												<div class="form-group">
													<label for="exampleInputEmail1">Renewal Approved By</label> 
													<input type="text" name="RenewalApprovedBy" class="form-control" id="exampleInputOSName" placeholder="" disabled>
												</div>   
												<div class="form-group">
													<label for="exampleInputEmail1">Renewal Approved Date</label> 
													<input type="text" name="RenewalApprovedDate" class="form-control" id="exampleInputOSName" placeholder="" disabled>
												</div>   
												<div class="form-group">
													<label for="exampleInputEmail1">Remark</label>
													<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea>  
												</div>  														
											</div>
										</div>
									</div>

									<div class="card-footer">
										<button type="submit" name="submit" class="btn btn-danger">Submit</button>
										<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="company()">
									</div>
								</form> 
							</div>
							<!-- /.card -->
						</div>
					</div>
					<!-- /.row -->
				</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->   
		</div>
		<!-- /.content -->  

		<!-- Control Sidebar -->
		<aside class="control-sidebar control-sidebar-dark">
			<!-- Control sidebar content goes here -->
		</aside>
		<!-- /.control-sidebar --> 
		<?php 
		require_once '../app/views/templates/modal.php';
		?>   
	</body>
	</html>
	