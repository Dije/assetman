<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>

			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
						if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<!-- Main content -->
							<section class="content">
							<div class="row">
								<div class="col-sm-12">
								<?php
									Flasher::Message();
								?>
								</div>
							</div>
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">  
											<div class="card">
												<!-- <div class="card-header"> --> 
													<!-- =============================== CREATE ================================ --> 
													<!-- </?php if($data['Access']["data"][$i]["create_flag"] == true ) { ?> 
														<a href="</?= base_url; ?>/CompanyLicense/add" class="btn btn-primary">Add</a> 
													</?php } ?> -->
													<!-- =============================== CREATE ================================ --> 
												<!-- </div> --> 
												<!-- /.card-header -->
												<div class="card-body"> 
													<?php if ($data["userRole"]["data"]["role_code"] == "BOD"){ ?>
														<table id="licenseBOD" class="table table-bordered table-striped">
													<?php }else{ ?>
														<table id="license" class="table table-bordered table-striped">
													<?php } ?>
														<thead>
															<tr>  
																<th>Company</th>
																<th>Business Unit</th>
																<th>License Type</th> 
																<th>Status</th> 
																<th>Severity</th> 
																<th>Renewal Status</th>  
																<th>License No</th> 
																<th>License Parent No</th>
																<th>Renewable</th> 
																<th>Issued By</th> 
																<th>Issued Date</th> 
																<th>Expired Date</th> 
																<th>Earliest Renewal Date</th> 
																<th>Last Renewal Date</th> 
																<th>Approved By</th> 
																<th>Approved Date</th> 
																<th>Remark</th> 
																<th>Created By</th> 
																<th>Created Date</th> 
																<th>Updated By</th> 
																<th>Updated Date</th> 
															</tr>
														</thead>
														<tbody>
															<?php if(empty($data["companyLicense"]["data"])) { ?>
																<tr>  
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>  
																	<td>-</td>  
																	<td>-</td>
																	<td>-</td>   
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>  
																	<td>-</td>  
																	<td>-</td>
																	<td>-</td>
																	<td>-</td> 
																	<td>-</td>
																	<td>-</td>  
																	<td>-</td>  
																	<td>-</td>
																	<td>-</td> 
																</tr> 
															<?php }else{  for ($x = 0; $x < count($data["companyLicense"]["data"]); $x++) { ?>   
																<tr> 
																	<?php $Company = $this->model('companyModel')->getDataCompany($token, $data['companyLicense']["data"][$x]["company_id"]); ?>
																	<td><?php echo $Company["data"]["company_name"]; ?></td> 
																	<td><?php echo $Company["data"]["business_unit_name"]; ?></td>
																	<td><?php echo $data["companyLicense"]["data"][$x]["license_type_name"]; ?></td>  
																	<?php if($data["companyLicense"]["data"][$x]["status"] == 1){ ?> 
																		<td>Draft</td>    
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 2){ ?>  
																		<td>Need Approval</td>        
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] < $data["companyLicense"]["data"][$x]["expired_date"]){ ?> 
																		<td style="background-color:#FFFF00;">Open to Renew</td> 
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["expired_date"] <= time()){ ?> 
																		<td style="background-color:#FF0000;">Expired</td>   
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3){ ?> 
																		<td style="background-color:#00FF00;">Active</td>     
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 4){ ?>
																		<td>Not Active</td>      
																	<?php } ?>
																	<?php if($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] < $data["companyLicense"]["data"][$x]["last_renewal_date"]){ ?>
																		<td>LOW</td> 
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["last_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["last_renewal_date"] < $data["companyLicense"]["data"][$x]["expired_date"]){ ?>
																		<td>HIGH</td>   
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["expired_date"] < time()){ ?> 
																		<td>CRITICAL</td>        
																	<?php }else{?>
																		<td> </td>
																	<?php } ?>
																	<?php if($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 1){ ?>  
																		<td>Open</td>   
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 2){ ?>  
																		<td>Follow Up</td>   
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 3){ ?>  
																		<td>Renew</td>  
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 4){ ?> 
																		<td>Close Permit</td>  
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 5){ ?> 
																		<td>Renew Approved</td>  
																	<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 6){ ?> 
																		<td>Close Permit Approved</td>  
																	<?php }else{ ?>
																		<td> </td>
																	<?php } ?>
																	<td><?php echo $data["companyLicense"]["data"][$x]["license_no"]; ?></td>  
																	<?php $Pid = $data["companyLicense"]["data"][$x]["parent_license_id"];
																	$data["ParentLicense"] = $this->model('companylicenseModel')->getDataCompanyLicense($token, $Pid); 
																	if($data["ParentLicense"]["status"] == false) {?>
																	<td> </td>
																	<?php }else{?>
																	<td><?php echo $data["ParentLicense"]["data"]["license_no"]; ?></td>    
																	<?php } ?>  
																	<?php if($data["companyLicense"]["data"][$x]["renewable"] == 1){ ?> 
																		<td>True</td>    
																	<?php }else{ ?>  
																		<td>False</td> 
																	<?php } ?>
																	<td><?php echo $data["companyLicense"]["data"][$x]["issued_by"]; ?></td>  
																	<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$x]["issued_date"]); ?></td> 
																	<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$x]["expired_date"]); ?></td>  
																	<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$x]["earliest_renewal_date"]); ?></td> 
																	<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$x]["last_renewal_date"]); ?></td>   
																	<?php if($data["companyLicense"]["data"][$x]["approved_user_id"] == 0) { ?>
																		<td> </td>
																	<?php }else{ $UserApp = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"][$x]["approved_user_id"])?>
																		<td><?php echo $UserApp["data"]["username"];  ?></td>
																	<?php }?>
																	<?php if($data["companyLicense"]["data"][$x]["approved_date"] == 0) { ?>
																		<td> </td>
																	<?php }else{ ?>
																		<td><?php echo date("d-m-Y", $data["companyLicense"]["data"][$x]["approved_date"]); ?></td> 
																	<?php }?>
																	<td style="white-space: pre-line;"><?php echo $data["companyLicense"]["data"][$x]["remark"]; ?></td>  
																	<?php if($data["companyLicense"]["data"][$x]["created_user_id"] == 0) { ?>
																		<td> </td>
																	<?php }else{ $UserCreate = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"][$x]["created_user_id"])?>
																		<td><?php echo $UserCreate["data"]["username"];  ?></td>
																	<?php }?>
																	<?php if($data["companyLicense"]["data"][$x]["created_at"] == 0) { ?>
																		<td> </td>
																	<?php }else{ ?>
																		<td><?php echo date("d-m-Y", $data["companyLicense"]["data"][$x]["created_at"]); ?></td> 
																	<?php }?>
																	<?php if($data["companyLicense"]["data"][$x]["updated_user_id"] == 0) { ?>
																		<td> </td>
																	<?php }else{ $UserUpdt = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"][$x]["updated_user_id"])?>
																		<td><?php echo $UserUpdt["data"]["username"];  ?></td>
																	<?php }?>
																	<?php if($data["companyLicense"]["data"][$x]["updated_at"] == 0) { ?>
																		<td> </td>
																	<?php }else{ ?>
																		<td><?php echo date("d-m-Y", $data["companyLicense"]["data"][$x]["updated_at"]); ?></td> 
																	<?php }?>
																</tr>
																<?php }} ?>
															</tbody>
															<tfoot>
																<tr>  
																	<th>Company</th>
																	<th>Business Unit</th>
																	<th>License Type</th> 
																	<th>Status</th> 
																	<th>Severity</th> 
																	<th>Renewal Status</th>  
																	<th>License No</th> 
																	<th>License Parent No</th>
																	<th>Renewable</th> 
																	<th>Issued By</th> 
																	<th>Issued Date</th> 
																	<th>Expired Date</th> 
																	<th>Earliest Renewal Date</th> 
																	<th>Last Renewal Date</th> 
																	<th>Approved By</th> 
																	<th>Approved Date</th> 
																	<th>Remark</th> 
																	<th>Created By</th> 
																	<th>Created Date</th> 
																	<th>Updated By</th> 
																	<th>Updated Date</th> 
																</tr>
															</tfoot>
														</table> 
													</div>
													<!-- /.card-body -->
												</div>
												<!-- /.card -->
											</div>
											<!-- /.col -->
										</div>
										<!-- /.row -->
									</div>
									<!-- /.container-fluid -->
								</section>
								<!-- /.content -->
							<?php }}}}?>
							<!-- /.content --> 
						</div>
						<!-- /.content-wrapper -->

						<!-- Control Sidebar -->
						<aside class="control-sidebar control-sidebar-dark">
							<!-- Control sidebar content goes here -->
						</aside>
						<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
				</body>
				</html>
