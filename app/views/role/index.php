<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>


			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
						if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<!-- Main content -->
							<section class="content">
							<div class="row">
								<div class="col-sm-12">
								<?php
									Flasher::Message();
								?>
								</div>
							</div>
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">  
											<div class="card">
												<div class="card-header">
													<!-- =============================== CREATE ================================ --> 
													<?php if($data['Access']["data"][$i]["create_flag"] == true ) { ?> 
														<a href="<?= base_url; ?>/Role/add" class="btn btn-primary">Add</a> 
													<?php } ?>
													<!-- =============================== CREATE ================================ --> 
												</div>
												<!-- /.card-header -->
												<div class="card-body"> 
													<table id="roleTable" class="table table-bordered table-striped">
														<thead>
															<tr>
																<th>Role Code</th>
																<th>Role Description</th>
																<th>Remark</th> 
																<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<th class="action">Action</th>  
																<?php } ?> 
															</tr>
														</thead> 
														</table> 
													</div>
													<!-- /.card-body -->
												</div>
												<!-- /.card -->
											</div>
											<!-- /.col -->
										</div>
										<!-- /.row -->
									</div>
									<!-- /.container-fluid -->
								</section>
								<!-- /.content -->
							<?php }}}}?>
						</div>
						<!-- /.content-wrapper -->

						<!-- Control Sidebar -->
						<aside class="control-sidebar control-sidebar-dark">
							<!-- Control sidebar content goes here -->
						</aside>
						<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
				</body>
				</html>
 