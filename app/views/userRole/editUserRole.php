<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= EDIT ================================= 
						if($data['Access']["data"][$i]["update_flag"] == true) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Edit Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start --> 
													<form action="<?= base_url; ?>/UserRole/updateUserRole" method="POST"  name="form1">
														<div class="card-body">
														<input type="hidden" name="user_role_id" value="<?php echo $data['userRole']["data"]["user_role_id"]; ?>">
															<div class="form-group">
																<label for="exampleInputEmail1">Full Name *</label> 
																<select id="UsrID" name="UsrID" class="form-control select2" style="width: 100%;">  
																	<option value="<?php echo $data["userRole"]["data"]["user_id"]; ?>">
																	<?php $EmployeeID = $data["userRole"]["data"]["employee_id"];
																	$data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $EmployeeID);
																	 echo $data["employee"]["data"]["first_name"] .' '. $data["employee"]["data"]["last_name"];?></option> 
																	<?php for ($y = 0; $y < count($data["user"]["data"]); $y++) { ?>  
																	<?php $EmployeeID = $data["user"]["data"][$y]["employee_id"];
																	if($EmployeeID == 0){} else{ ?>
																	<option value="<?php echo $data["user"]["data"][$y]["user_id"]; ?>">
																	<?php $data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $EmployeeID);
																	 echo $data["employee"]["data"]["first_name"] .' '. $data["employee"]["data"]["last_name"];}?></option>  
																<?php } ?>
															</select> 
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Role *</label>
															<select id="RoleID" name="RoleID" class="form-control select2" style="width: 100%;">  
																<option value="<?php echo $data["userRole"]["data"]["role_id"]; ?>"><?php echo $data["userRole"]["data"]["role_code"]; ?></option> 
																<?php for ($z = 0; $z < count($data["role"]["data"]); $z++) { ?>   ?>  
																<option value="<?php echo $data["role"]["data"][$z]["role_id"]; ?>"><?php echo $data["role"]["data"][$z]["role_code"]; ?></option> 
															<?php } ?>
														</select>
													</div>
													<div class="form-group">
														<label for="exampleInputEmail1">Remark</label>
														<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"><?php echo $data["userRole"]["data"]["remark"]; ?></textarea> 
													</div>
												</div>
												<!-- /.card-body -->

												<div class="card-footer">
													<button type="submit" name="submit" class="btn btn-danger">Submit</button>
													<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="userRole()">
												</div>
											</form> 
									</div>
									<!-- /.card -->
								</div>
							</div>
							<!-- /.row -->
						</div><!-- /.container-fluid -->
					</section>
						<?php }}}} ?>
						<!-- /.content --> 
					</div>
					<!-- /.content-wrapper -->

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html> 