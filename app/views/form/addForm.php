<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= CREATE ================================= 
						if($data['Access']["data"][$i]["create_flag"] == true ) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/Form/addForm" method="POST" name="form1">
													<div class="row">
														<div class="card-body col-md-6">
															<div class="form-group">
																<label for="exampleInputEmail1">Form Code *</label>
																<input type="text" name="FormCode" class="form-control" id="exampleInputOSName" placeholder="Enter Form Code">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Form PHP</label>
																<input type="text" name="form_php" class="form-control" id="exampleInputOSName" placeholder="Enter Form PHP">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Form Description *</label>
																<input type="text" name="FormDescription" class="form-control" id="exampleInputOSName" placeholder="Enter Form Description">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Form Type *</label>
																<select id="FormTypeID" name="FormTypeID" class="form-control select2" style="width: 100%;"> 
																	<option value="" >Select Form Type</option>
																	<?php for ($x = 0; $x < count($data["formtype"]["data"]); $x++) { ?>  
																		<option value="<?php echo $data["formtype"]["data"][$x]["form_type_id"]; ?>"><?php echo $data["formtype"]["data"][$x]["form_type_code"]; ?></option> 
																	<?php } ?>
																</select>
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Form Parent</label>
																<select id="FormParentID" name="FormParentID" class="form-control select2" style="width: 100%;"> 
																	<option value="" >Select Form Parent</option>
																</select>
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Sequence No *</label>
																<input type="text" name="SequenceNo" class="form-control" id="exampleInputOSName" placeholder="Enter Sequence No">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Class Tag</label>
																<input type="text" name="ClassTag" class="form-control" id="exampleInputOSName" placeholder="Enter Class Tag">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Remark</label>
																<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea> 
															</div>
														</div>
													</div>
													<!-- /.card-body -->

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="formss()">
													</div>
												</form>

											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>  
							<!-- /.content -->  
						<?php }}}} ?>
					</div>
					<!-- /.content -->  

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>
 