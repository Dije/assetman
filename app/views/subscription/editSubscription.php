<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= EDIT ================================= 
						if($data['Access']["data"][$i]["update_flag"] == true) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Edit Data</h3>
												</div>  
												<!-- /.card-header --> 
												<!-- form start --> 
													<form action="<?= base_url; ?>/DataSubscriptions/updateSubscription" method="POST"  name="form1">
													<div class="row">
														<div class="card-body col-md-6">
															<input type="hidden" name="subscription_id" value="<?php echo $data['subscription']["data"]["subscription_id"]; ?>">
															<div class="form-group">
																<label for="exampleInputEmail1">Subscription Name</label>
																<input type="text" name="subscription_name" class="form-control" id="exampleInputOSName" placeholder="Enter Subscription Name" value="<?php echo $data['subscription']["data"]["subscription_name"]; ?>">
															</div>  
															<div class="form-group">
																<label>Start Date</label>
																<div class="input-group date" name="date_start" id="reservationdate" data-target-input="nearest" >
																	<?php if($data['subscription']["data"]["date_start"] != 0) {?>
																		<input type="text" id="dateee" name="date_start" value="<?php echo date("d-m-Y", $data['subscription']["data"]["date_start"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate" />
																	<?php } else { ?> 
																		<input type="text" id="dateee" name="date_start" value="" class="form-control datetimepicker-input" data-target="#reservationdate" />
																	<?php } ?>
																	<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																	</div> 
																</div>
															</div>
															<div class="form-group">
																<label>End Date</label>
																<div class="input-group date" name="end_date" id="reservationdate3" data-target-input="nearest" >
																<?php if($data['subscription']["data"]["end_date"] != 0) {?>
																	<input type="text" id="dateeee" name="end_date" value="<?php echo date("d-m-Y", $data['subscription']["data"]["end_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate3" />
																<?php } else { ?> 
																	<input type="text" id="dateeee" name="end_date" value="" class="form-control datetimepicker-input" data-target="#reservationdate3" />
																<?php } ?>
																<div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
																	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																	</div>
																</div>
															</div> 
														</div> 
														<div class="card-body col-md-6">
															<div class="form-group"> 
																<label for="exampleInputEmail1">Price</label>
																<input type="text" name="price" class="form-control" id="exampleInputOSName" placeholder="Enter Price" value="<?php echo $data['subscription']["data"]["price"]; ?>">
															</div>   
															<div class="form-group">
																<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"><?php echo $data['subscription']["data"]["remark"]; ?></textarea> 
															</div> 
														</div>
														<!-- /.card-body -->
													</div> 

												<div class="card-footer">
													<button type="submit" name="submit" class="btn btn-danger">Submit</button>
													<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="subscription()">
												</div>
											</form> 
									</div>
									<!-- /.card -->
								</div>
							</div>
							<!-- /.row -->
						</div><!-- /.container-fluid -->
					</section>
						<?php }}}} ?>
						<!-- /.content --> 
					</div>
					<!-- /.content-wrapper -->

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html> 