<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>

			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
						if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<!-- Main content -->
							<section class="content">
							<div class="row">
								<div class="col-sm-12">
								<?php
									Flasher::Message();
								?>
								</div>
							</div>
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">  
											<div class="card">
												<div class="card-header">
													<!-- =============================== CREATE ================================ --> 
													<?php if($data['Access']["data"][$i]["create_flag"] == true ) { ?> 
														<a href="<?= base_url; ?>/UserUPS/add" class="btn btn-primary">Add</a> 
													<?php } ?>
													<!-- =============================== CREATE ================================ --> 
												</div>
												<!-- /.card-header -->
												<div class="card-body"> 
													<table id="example1" class="table table-bordered table-striped">
														<thead>
															<tr>
																<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<th class="action">Action</th>  
																<?php } ?>
																<!-- <th>Number</th> -->
																<th>UPS Code</th>
																<th>UPS Type</th>
																<th>UPS Vol</th>
																<th>Supplier</th>
																<th>Full Name</th>
																<th>Date Bought</th>
																<th>Price</th>
																<th>Warranty</th>
																<th>Remark</th>
																<!-- <th>Created At</th>  
																<th>Updated At</th>  -->  
															</tr>
														</thead>
														<tbody>
															<?php if(empty($data["ups"]["data"])) { ?>
																<tr>
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																		<th class="action">-</th>  
																	<?php } ?> 
																	<td>-</td>
																	<td>-</td>
																	<td>-</td> 
																	<td>-</td>
																	<td>-</td>
																	<td>-</td> 
																	<td>-</td>
																	<td>-</td> 
																</tr> 
															<?php }else{  for ($x = 0; $x < count($data["ups"]["data"]); $x++) { ?>   
																<tr>
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>  
																		<td class="action"> 
																			<?php if($data['Access']["data"][$i]["update_flag"] == true) { ?>
																				<a href="<?= base_url; ?>/UserUPS/edit/<?php echo $data["ups"]["data"][$x]["ups_id"]; ?>" name="edit" class="btn btn-primary">Edit</a> 
																			<?php } ?>
																			<?php if($data['Access']["data"][$i]["delete_flag"] == true) { ?>
																				<a href="#myModal" class="btn btn-danger" data-href="<?= base_url; ?>/UserUPS/deleteUPS/<?php echo $data["ups"]["data"][$x]["ups_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a> 
																			<?php } ?>
																		</td>    
																	<?php } ?> 
																	<td><?php echo $data["ups"]["data"][$x]["ups_code"]; ?></td> 
																	<td><?php echo $data["ups"]["data"][$x]["ups_type"]; ?></td> 
																	<td><?php echo $data["ups"]["data"][$x]["ups_vol"]; ?></td> 
																	<td><?php echo $data["ups"]["data"][$x]["supplier"]; ?></td> 
																	<?php $UserID = $data["ups"]["data"][$x]["user_id"];
																	$data['user'] = $this->model('userModel')->getDataUser($token, $UserID); 
																	$EmployeeID = $data["user"]["data"]["employee_id"];
																	$data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $EmployeeID); ?>
																	<td><?php echo $data["employee"]["data"]["first_name"] .' '. $data["employee"]["data"]["last_name"]; ?></td>   
																	<td><?php echo date('d-m-Y', $data["ups"]["data"][$x]["date_bought"]); ?></td>
																	<td>Rp.<?php echo number_format($data["ups"]["data"][$x]["price"],0,',','.'); ?></td> 
																	<td><?php echo $data["ups"]["data"][$x]["warranty"]; ?></td> 
																	<td style="white-space: pre-line;"><?php echo $data["ups"]["data"][$x]["remark"]; ?></td>   
																</tr>
																<?php }} ?>
															</tbody>
															<tfoot>
																<tr>
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																		<th class="action">Action</th>  
																	<?php } ?> 
																	<!-- <th>Number</th> -->
																	<th>UPS Code</th>
																	<th>UPS Type</th>
																	<th>UPS Vol</th>
																	<th>Supplier</th>
																	<th>Full Name</th>
																	<th>Date Bought</th>
																	<th>Price</th>
																	<th>Warranty</th>
																	<th>Remark</th>
																	<!-- <th>Created At</th>  
																	<th>Updated At</th>  -->  
																</tr>
															</tfoot>
														</table> 
													</div>
													<!-- /.card-body -->
												</div>
												<!-- /.card -->
											</div>
											<!-- /.col -->
										</div>
										<!-- /.row -->
									</div>
									<!-- /.container-fluid -->
								</section>
								<!-- /.content --> 
							<?php }}}}?>
							<!-- /.content --> 
						</div>
						<!-- /.content-wrapper -->

						<!-- Control Sidebar -->
						<aside class="control-sidebar control-sidebar-dark">
							<!-- Control sidebar content goes here -->
						</aside>
						<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
				</body>
				</html>
