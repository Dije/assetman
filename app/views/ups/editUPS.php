<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= EDIT ================================= 
						if($data['Access']["data"][$i]["update_flag"] == true) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Edit Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start --> 
													<form action="<?= base_url; ?>/UserUPS/updateUPS" method="POST"  name="form1">
													<div class="row">
														<div class="card-body col-md-6">
															<input type="hidden" name="ups_id" value="<?php echo $data['ups']["data"]["ups_id"]; ?>">
															<div class="form-group">
																<label for="exampleInputEmail1">UPS Code</label>
																<input type="text" name="UPSCode" class="form-control" id="exampleInputOSName" placeholder="Enter UPS Code" value="<?php echo $data['ups']["data"]["ups_code"]; ?>">
															</div> 
															<div class="form-group"> 
																<label for="exampleInputEmail1">UPS Type</label>
																<select name="UPSType" class="form-control select2" style="width: 100%;">  
																	<option value="<?php echo $data['ups']["data"]["ups_type"]; ?>"><?php echo $data['ups']["data"]["ups_type"]; ?></option>  
																	<?php if($data['ups']["data"]["ups_type"] == "ICA") { ?> 
																		<option value="AVAPRO">AVAPRO</option>  
																		<option value="PROLINK">PROLINK</option>  
																		<option value="VEKTOR ABLEREX MS II 6kVA">VEKTOR ABLEREX MS II 6kVA</option>  
																	<?php }elseif ($data['ups']["data"]["ups_type"] == "AVAPRO") { ?>
																		<option value="ICA">ICA</option>   
																		<option value="PROLINK">PROLINK</option>  
																		<option value="VEKTOR ABLEREX MS II 6kVA">VEKTOR ABLEREX MS II 6kVA</option>  
																	<?php }elseif ($data['ups']["data"]["ups_type"] == "PROLINK") { ?>
																		<option value="ICA">ICA</option>  
																		<option value="AVAPRO">AVAPRO</option>   
																		<option value="VEKTOR ABLEREX MS II 6kVA">VEKTOR ABLEREX MS II 6kVA</option>  
																	<?php }else { ?>
																		<option value="ICA">ICA</option>  
																		<option value="AVAPRO">AVAPRO</option>  
																		<option value="PROLINK">PROLINK</option>    
																	<?php } ?>
																</select>  
															</div>
															<div class="form-group"> 
																<label for="exampleInputEmail1">UPS Vol</label>
																<select name="UPSVol" class="form-control select2" style="width: 100%;">  
																	<option value="<?php echo $data['ups']["data"]["ups_vol"]; ?>"><?php echo $data['ups']["data"]["ups_vol"]; ?></option> 
																	<?php if($data['ups']["data"]["ups_vol"] == "700 V") { ?>  
																		<option value="1200 V">1200 V</option>  
																		<option value="1300 V">1300 V</option>  
																		<option value="1400 V">1400 V</option>  
																		<option value="6000 V">6000 V</option> 
																	<?php }elseif($data['ups']["data"]["ups_vol"] == "1200 V") { ?> 
																		<option value="700 V">700 V</option>   
																		<option value="1300 V">1300 V</option>  
																		<option value="1400 V">1400 V</option>  
																		<option value="6000 V">6000 V</option> 
																	<?php }elseif($data['ups']["data"]["ups_vol"] == "1300 V") { ?> 
																		<option value="700 V">700 V</option>  
																		<option value="1200 V">1200 V</option>   
																		<option value="1400 V">1400 V</option>  
																		<option value="6000 V">6000 V</option> 
																	<?php }elseif($data['ups']["data"]["ups_vol"] == "1400 V") { ?> 
																		<option value="700 V">700 V</option>  
																		<option value="1200 V">1200 V</option>  
																		<option value="1300 V">1300 V</option>   
																		<option value="6000 V">6000 V</option> 
																	<?php }else { ?>
																		<option value="700 V">700 V</option>  
																		<option value="1200 V">1200 V</option>  
																		<option value="1300 V">1300 V</option>  
																		<option value="1400 V">1400 V</option>    
																	<?php } ?> 
																</select> 
															</div>  
															<div class="form-group"> 
																<label for="exampleInputEmail1">Supplier</label>
																<select name="Supplier" class="form-control select2" style="width: 100%;">  
																	<option value="<?php echo $data['ups']["data"]["supplier"]; ?>"><?php echo $data['ups']["data"]["supplier"]; ?></option> 
																	<?php if($data['ups']["data"]["supplier"] == "Z Tech Indonesia") { ?>   
																		<option value="Zebe Computer">Zebe Computer</option>   
																		<option value="N/A">N/A</option>  
																	<?php }elseif($data['ups']["data"]["supplier"] == "Zebe Computer") { ?> 
																		<option value="Z Tech Indonesia">Z Tech Indonesia</option>   
																		<option value="N/A">N/A</option>  
																	<?php }else { ?>
																		<option value="Z Tech Indonesia">Z Tech Indonesia</option>  
																		<option value="Zebe Computer">Zebe Computer</option>    
																	<?php } ?> 
																</select> 
															</div>   
															<div class="form-group">
															<label>Date Bought</label>
																<div class="input-group date" name="date_bought" id="reservationdate" data-target-input="nearest" >
																	<?php if($data['ups']["data"]["date_bought"] != 0) {?>
																		<input type="text" id="dateee" name="date_bought" value="<?php echo date("d-m-Y", $data['ups']["data"]["date_bought"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate" />
																	<?php } else { ?> 
																		<input type="text" id="dateee" name="date_bought" value="" class="form-control datetimepicker-input" data-target="#reservationdate" />
																	<?php } ?>
																	<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
																		<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																	</div> 
																</div>
															</div>   
														</div>
														<div class="card-body col-md-6"> 
															<div class="form-group"> 
																<label for="exampleInputEmail1">Price</label>
																<input type="text" name="Price" class="form-control" id="exampleInputOSName" placeholder="Enter Price" value="<?php echo $data['ups']["data"]["price"]; ?>">
															</div>
															<div class="form-group"> 
																<label for="exampleInputEmail1">Warranty</label>
																<input type="text" name="Warranty" class="form-control" id="exampleInputOSName" placeholder="Enter Warranty" value="<?php echo $data['ups']["data"]["warranty"]; ?>">
															</div>    
															<div class="form-group">   
																<label for="exampleInputEmail1">User</label>
																<select id="UserID" name="UserID" class="form-control select2" style="width: 100%;">  
																		<option value="<?php echo $data["ups"]["data"]["user_id"]; ?>">
																		<?php $UserID = $data["ups"]["data"]["user_id"];
																		$data['upsUser'] = $this->model('userModel')->getDataUser($token, $UserID); 
																		$EmployeeID = $data["upsUser"]["data"]["employee_id"];
																		$data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $EmployeeID);
																			echo $data["employee"]["data"]["first_name"] .' '. $data["employee"]["data"]["last_name"];?></option> 
																		<?php for ($x = 0; $x < count($data["user"]["data"]); $x++) { ?>  
																		<?php $EmployeeID = $data["user"]["data"][$x]["employee_id"];
																		if($EmployeeID == 0){} else{ ?>
																		<option value="<?php echo $data["user"]["data"][$x]["user_id"]; ?>">
																		<?php $data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $EmployeeID);
																			echo $data["employee"]["data"]["first_name"] .' '. $data["employee"]["data"]["last_name"];}?></option>  
																	<?php } ?>
																</select>    
															</div>    
															<div class="form-group">
																<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"><?php echo $data['ups']["data"]["remark"]; ?></textarea> 
															</div>
														</div>
														<!-- /.card-body -->
													</div> 

												<div class="card-footer">
													<button type="submit" name="submit" class="btn btn-danger">Submit</button>
													<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="ups()">
												</div>
											</form> 
									</div>
									<!-- /.card -->
								</div>
							</div>
							<!-- /.row -->
						</div><!-- /.container-fluid -->
					</section>
						<?php }}}} ?>
						<!-- /.content --> 
					</div>
					<!-- /.content-wrapper -->

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html> 