<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			 
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/CompanyShareholder/addCompanyShareholder" method="POST" name="form1" enctype="multipart/form-data" > 
													<div class="card-body"> 
													<input type="hidden" name="company_id" class="form-control" id="exampleInputOSName" placeholder="Enter Company Name" value="<?php echo $data["CompanyID"]; ?>">
														<div class="form-group">
															<label for="exampleInputEmail1">Name*</label>
															<input type="text" name="ShareholderName" class="form-control" id="exampleInputOSName" placeholder="Enter Name">
														</div>     
														<div class="form-group">
															<label for="exampleInputEmail1">Number</label> 
															<input type="text" name="Number" class="form-control" id="exampleInputOSName" placeholder="Enter Number of Share">
														</div>      
														<div class="form-group">
															<label for="exampleInputEmail1">Percentage</label>
															<input type="text" name="Percentage" class="form-control" id="exampleInputOSName" placeholder="Enter Percentage">
														</div>    
														<div class="form-group">
							         						<label for="exampleInputEmail1">Amount</label>
															<input type="text" name="Amount" class="form-control" id="exampleInputOSName" placeholder="Enter Amount">  
														</div>                       
														<div class="form-group">
															<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Address"></textarea>  
													</div>
													<!-- /.card-body -->
													</div>

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="company()">
													</div>
												</form> 
											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
							<!-- /.content -->   
					</div>
					<!-- /.content -->  

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>
 