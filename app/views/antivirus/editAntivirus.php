<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= EDIT ================================= 
						if($data['Access']["data"][$i]["update_flag"] == true) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Edit Data</h3>
												</div>
												<!-- /.card-header -->
												<!-- form start --> 
												<form action="<?= base_url; ?>/UserAntivirus/updateAntivirus" method="POST" name="form1">
												<div class="row">
					                   				 <div class="card-body col-md-6">
														<input type="hidden" name="antivirus_id" value="<?php echo $data['antivirus']["data"]["antivirus_id"]; ?>">
														<div class="form-group">
															<label for="exampleInputEmail1">Device Code</label>
															<input type="text" name="device" class="form-control" id="exampleInputOSName" placeholder="Enter Device Code" value="<?php echo $data["antivirus"]["data"]["device"]; ?>">
														</div>
														</div>
													</div>
													<div class="row">
														<div class="card-body col-md-6">
														<div class="form-group"> 
															<label for="exampleInputEmail1">Licence Key</label>
															<input type="text" name="license_key" class="form-control" id="exampleInputOSName" placeholder="Enter Licence Key" value="<?php echo $data["antivirus"]["data"]["license_key"]; ?>">
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Activation ID</label>
															<input type="text" name="activation_id" class="form-control" id="exampleInputOSName" placeholder="Enter Activation ID" value="<?php echo $data["antivirus"]["data"]["activation_id"]; ?>"> 
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Activation Code Old</label>
															<input type="text" name="activation_code_old" class="form-control" id="exampleInputOSName" placeholder="Enter Activation Code Old" value="<?php echo $data["antivirus"]["data"]["activation_code_old"]; ?>">
														</div>
														</div>
														<div class="card-body col-md-6">
														<div class="form-group">
															<label for="exampleInputEmail1">Activation Code New</label>
															<input type="text" name="activation_code_new" class="form-control" id="exampleInputOSName" placeholder="Enter Activation Code New" value="<?php echo $data["antivirus"]["data"]["activation_code_new"]; ?>">   
														</div>
														<div class="form-group">
														<label>Activation Date</label>
															<div class="input-group date" name="activation_date" id="reservationdate" data-target-input="nearest" >
																<?php if($data['antivirus']["data"]["activation_date"] != 0) {?>
																	<input type="text" id="dateee" name="activation_date" value="<?php echo date("d-m-Y", $data['antivirus']["data"]["activation_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate" />
																<?php } else { ?> 
																	<input type="text" id="dateee" name="activation_date" value="" class="form-control datetimepicker-input" data-target="#reservationdate" />
																<?php } ?>
																<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
																	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																</div> 
															</div>
														</div>
														<div class="form-group">
														<label>Expiration Date</label>
															<div class="input-group date" name="expiration_date" id="reservationdate3" data-target-input="nearest" >
															<?php if($data['antivirus']["data"]["expiration_date"] != 0) {?>
																<input type="text" id="dateeee" name="expiration_date" value="<?php echo date("d-m-Y", $data['antivirus']["data"]["expiration_date"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate3" />
															<?php } else { ?> 
																<input type="text" id="dateeee" name="expiration_date" value="" class="form-control datetimepicker-input" data-target="#reservationdate3" />
															<?php } ?>
															<div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
															<div class="input-group-text"><i class="fa fa-calendar"></i></div>
															</div>
														</div>
														</div> 
															<div class="form-group">
																<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"><?php echo $data["antivirus"]["data"]["remark"]; ?></textarea> 
															</div>
														</div>
														<!-- /.card-body -->
													</div>
													<!-- /.card-body -->
													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="antivirus()">
													</div>
												</form> 
											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
						<?php }}}} ?>
						<!-- /.content --> 
					</div>
					<!-- /.content-wrapper -->

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>