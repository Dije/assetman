<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= EDIT ================================= 
						if($data['Access']["data"][$i]["update_flag"] == true) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Edit Data</h3>
												</div>
												<!-- /.card-header -->
												<!-- form start --> 
												<form action="<?= base_url; ?>/PrinterData/updatePrinter" method="POST" name="form1">
												<div class="row">
													<div class="card-body col-md-6"> 
													<input type="hidden" name="PrinterID" class="form-control" id="exampleInputOSName" placeholder="Enter Printer Name" value="<?php echo $data["printer"]["data"]["printer_id"]; ?>">
														<div class="form-group">
															<label for="exampleInputEmail1">Printer Name *</label>
															<input type="text" name="PrinterName" class="form-control" id="exampleInputOSName" placeholder="Enter Printer Name" value="<?php echo $data["printer"]["data"]["printer_name"]; ?>">
														</div>    
														<div class="form-group"> 
														<label for="exampleInputEmail1">Black Code</label>
														<select name="BlackID" class="form-control select2" style="width: 100%;">  
															<option value="<?php echo $data['printer']["data"]["black_id"]; ?>"><?php echo $data['printer']["data"]["black_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['black']["data"]); $x++) { ?>  
															<option value="<?php echo $data['black']["data"][$x]["ink_id"]; ?>"><?php echo $data['black']["data"][$x]["ink_name"]; ?></option> 
															<?php } ?>
														</select> 
														</div>    
														<div class="form-group"> 
														<label for="exampleInputEmail1">Cyan Code</label>
														<select name="CyanID" class="form-control select2" style="width: 100%;">  
															<option value="<?php echo $data['printer']["data"]["cyan_id"]; ?>"><?php echo $data['printer']["data"]["cyan_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['cyan']["data"]); $x++) { ?>  
															<option value="<?php echo $data['cyan']["data"][$x]["ink_id"]; ?>"><?php echo $data['cyan']["data"][$x]["ink_name"]; ?></option> 
															<?php } ?>
														</select> 
														</div>       
														<div class="form-group"> 
														<label for="exampleInputEmail1">Magenta Code</label>
														<select name="MagentaID" class="form-control select2" style="width: 100%;">  
															<option value="<?php echo $data['printer']["data"]["magenta_id"]; ?>"><?php echo $data['printer']["data"]["magenta_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['magenta']["data"]); $x++) { ?>  
															<option value="<?php echo $data['magenta']["data"][$x]["ink_id"]; ?>"><?php echo $data['magenta']["data"][$x]["ink_name"]; ?></option> 
															<?php } ?>
														</select> 
														</div>     
														<div class="form-group"> 
														<label for="exampleInputEmail1">Yellow Code</label>
														<select name="YellowID" class="form-control select2" style="width: 100%;">  
															<option value="<?php echo $data['printer']["data"]["yellow_id"]; ?>"><?php echo $data['printer']["data"]["yellow_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['yellow']["data"]); $x++) { ?>  
															<option value="<?php echo $data['yellow']["data"][$x]["ink_id"]; ?>"><?php echo $data['yellow']["data"][$x]["ink_name"]; ?></option> 
															<?php } ?>
														</select> 
														</div>     
													</div>
													<div class="card-body col-md-6">   
														<div class="form-group"> 
														<label for="exampleInputEmail1">Color Code</label>
														<select name="ColorID" class="form-control select2" style="width: 100%;">  
															<option value="<?php echo $data['printer']["data"]["color_id"]; ?>"><?php echo $data['printer']["data"]["color_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['color']["data"]); $x++) { ?>  
															<option value="<?php echo $data['color']["data"][$x]["ink_id"]; ?>"><?php echo $data['color']["data"][$x]["ink_name"]; ?></option> 
															<?php } ?>
														</select> 
														</div>     
														<div class="form-group"> 
														<label for="exampleInputEmail1">Tricolor Code</label>
														<select name="TricolorID" class="form-control select2" style="width: 100%;">  
															<option value="<?php echo $data['printer']["data"]["tricolor_id"]; ?>"><?php echo $data['printer']["data"]["tricolor_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['tricolor']["data"]); $x++) { ?>  
															<option value="<?php echo $data['tricolor']["data"][$x]["ink_id"]; ?>"><?php echo $data['tricolor']["data"][$x]["ink_name"]; ?></option> 
															<?php } ?>
														</select> 
														</div>     
														<div class="form-group"> 
														<label for="exampleInputEmail1">Others Code</label>
														<select name="OthersID" class="form-control select2" style="width: 100%;">  
															<option value="<?php echo $data['printer']["data"]["others_id"]; ?>"><?php echo $data['printer']["data"]["others_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['others']["data"]); $x++) { ?>  
															<option value="<?php echo $data['others']["data"][$x]["ink_id"]; ?>"><?php echo $data['others']["data"][$x]["ink_name"]; ?></option> 
															<?php } ?>
														</select> 
														</div>                              
														<div class="form-group">
															<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"><?php echo $data['printer']["data"]["remark"]; ?></textarea> 
														</div>
													</div>
												</div>
													<!-- /.card-body -->
													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="department()">
													</div>
												</form> 
											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
						<?php }}}} ?>
						<!-- /.content --> 
					</div>
					<!-- /.content-wrapper -->

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>