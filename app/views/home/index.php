<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<section class="content">
				<div class="row">
					<div class="col-sm-12">
						<?php
						Flasher::Message();
						?>
					</div>
				</div>
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
				if($data['Access']["data"][$i]["form_php"] == "Company" && $data["userRole"]["data"]["role_code"] == "LEGALMGR"){  
					if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<div class="container-fluid">
								<div class="row">
									<div class="col-12">  
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">Company You Need To Approve</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body"> 
													<table id="AppCompany" class="table table-bordered table-striped">
														<thead>
															<tr> 
																<th>Company</th>
																<th>Business Unit</th>
																<th>Address</th> 
																<th>Legal & License File URL</th> 
																<th>Status</th> 
																<th>Approved By</th> 
																<th>Approved Date</th> 
																<th>Remark</th> 
																<th>Created By</th> 
																<th>Created Date</th> 
																<th>Updated By</th> 
																<th>Updated Date</th>  
																<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<th>Action</th>  
																<?php } ?>
															</tr>
														</thead>
														<tbody>
															<?php if(empty($data["company"]["data"])) { ?>
																<tr> 
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>  
																	<td>-</td>
																	<td>-</td> 
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>  
																	<td>-</td>  
																	<td>-</td>
																	<td>-</td> 
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																		<th>-</th>  
																	<?php } ?>
																</tr> 
															<?php }else{  for ($y = 0; $y < count($data["company"]["data"]); $y++) { ?>   
																<tr>
																	<td><?php echo $data["company"]["data"][$y]["company_name"]; ?></td> 
																	<td><?php echo $data["company"]["data"][$y]["business_unit_name"]; ?></td>
																	<td><?php echo $data["company"]["data"][$y]["address"]; ?></td>  
																	<td><?php echo $data["company"]["data"][$y]["legal_license_file_url"]; ?></td>  
																	<?php if($data["company"]["data"][$y]["status"] == 1){ ?> 
																		<td>Draft</td> 
																	<?php }elseif($data["company"]["data"][$y]["status"] == 2){ ?> 
																		<td>Need Approval</td>  
																	<?php }elseif($data["company"]["data"][$y]["status"] == 3){ ?> 
																		<td>Active</td> 
																	<?php }elseif($data["company"]["data"][$y]["status"] == 4){ ?>
																		<td>NotActive</td> 
																	<?php } ?>
																	<?php if($data["company"]["data"][$y]["approved_user_id"] == 0) { ?>
																		<td> </td>
																	<?php }else{ $UserApp = $this->model('userModel')->getDataUser($token, $data["company"]["data"][$y]["approved_user_id"])?>
																		<td><?php echo $UserApp["data"]["username"];  ?></td>
																	<?php }?>
																	<?php if($data["company"]["data"][$y]["approved_date"] == 0) { ?>
																		<td> </td>
																	<?php }else{ ?>
																		<td><?php echo date("d-m-Y", $data["company"]["data"][$y]["approved_date"]); ?></td> 
																	<?php }?>
																	<td><?php echo $data["company"]["data"][$y]["remark"]; ?></td>  
																	<?php if($data["company"]["data"][$y]["created_user_id"] == 0) { ?>
																		<td> </td>
																	<?php }else{ $UserCreate = $this->model('userModel')->getDataUser($token, $data["company"]["data"][$y]["created_user_id"])?>
																		<td><?php echo $UserCreate["data"]["username"];  ?></td>
																	<?php }?>
																	<?php if($data["company"]["data"][$y]["created_at"] == 0) { ?>
																		<td> </td>
																	<?php }else{ ?>
																		<td><?php echo date("d-m-Y", $data["company"]["data"][$y]["created_at"]); ?></td> 
																	<?php }?>
																	<?php if($data["company"]["data"][$y]["updated_user_id"] == 0) { ?>
																		<td> </td>
																	<?php }else{ $UserUpdt = $this->model('userModel')->getDataUser($token, $data["company"]["data"][$y]["updated_user_id"])?>
																		<td><?php echo $UserUpdt["data"]["username"];  ?></td>
																	<?php }?>
																	<?php if($data["company"]["data"][$y]["updated_at"] == 0) { ?>
																		<td> </td>
																	<?php }else{ ?>
																		<td><?php echo date("d-m-Y", $data["company"]["data"][$y]["updated_at"]); ?></td> 
																	<?php }?>																	
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>  
																		<td> 
																			<?php if($data['Access']["data"][$i]["update_flag"] == true) { ?>
																				<a href="<?= base_url; ?>/Company/edit/<?php echo $data["company"]["data"][$y]["company_id"]; ?>" name="edit" class="btn btn-primary">Edit</a> 
																			<?php } ?>
																			<?php if($data['Access']["data"][$i]["delete_flag"] == true) { ?>
																				<a href="#myModal" class="btn btn-danger" data-href="<?= base_url; ?>/Company/deleteCompany/<?php echo $data["company"]["data"][$y]["company_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a> 
																			<?php } ?>
																		<?php if($data["userRole"]["data"]["role_code"] == "LEGALMGR") { ?>
																			<input type="button" name="cancel" class="btn btn-danger" value="Approve" onclick="approve(<?php echo $data["company"]["data"][$y]["company_id"]; ?>)" />
																		<?php } ?>
																		</td>    
																	<?php } ?>
																</tr>
																<?php }} ?>
															</tbody>
															<tfoot>
																<tr> 
																	<th>Company</th>
																	<th>Business Unit</th>
																	<th>Address</th> 
																	<th>Legal & License File URL</th> 
																	<th>Status</th> 
																	<th>Approved By</th> 
																	<th>Approved Date</th> 
																	<th>Remark</th> 
																	<th>Created By</th> 
																	<th>Created Date</th> 
																	<th>Updated By</th> 
																	<th>Updated Date</th>  
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																		<th>Action</th>  
																	<?php } ?>
																</tr>
															</tfoot>
														</table> 
													</div>
													<!-- /.card-body -->
										</div>
										<!-- /.card -->
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container-fluid -->
					<?php }}
				if($data['Access']["data"][$i]["form_php"] == "CompanyLicense" && $data["userRole"]["data"]["role_code"] == "LEGALMGR"){  
					if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<div class="container-fluid">
								<div class="row">
									<div class="col-12">  
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">Company License You Need To Approve</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body"> 
											<table id="Approvelicense" class="table table-bordered table-striped">
												<thead>
													<tr>
														<th>Company</th> 
														<th>License Type</th> 
														<th>License No</th>   
														<th>Expired Date</th> 
														<th>Earliest Renewal Date</th> 
														<th>Last Renewal Date</th> 
														<th>Status</th> 
														<th>Renewal Status</th>   
														<th>Remark</th> 
														<th>Reminder No</th> 
														<th>Created By</th> 
														<th>Created Date</th> 
														<th>Updated By</th> 
														<th>Updated Date</th> 
														<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
															<th>Action</th>  
														<?php } ?>
													</tr>
												</thead>
												<tbody>
													<?php if(empty($data["companyLicenseApp"]["data"])) { ?>
														<tr>	 
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>  
															<td>-</td>  
															<td>-</td>
															<td>-</td>   
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>
															<td>-</td>  
															<td>-</td>  
															<td>-</td> 
															<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																<th>-</th>  
															<?php } ?>  
														</tr> 
													<?php }else{ for ($a = 0; $a< count($data["companyLicenseApp"]["data"]); $a++) { ?>   
														<tr>
															<?php $Company = $this->model('companyModel')->getDataCompany($token, $data['companyLicenseApp']["data"][$a]["company_id"]); ?>
															<td><?php echo $Company["data"]["company_name"]; ?></td>  
															<td><?php echo $data["companyLicenseApp"]["data"][$a]["license_type_name"]; ?></td>
															<td><?php echo $data["companyLicenseApp"]["data"][$a]["license_no"]; ?></td>  
															<td><?php echo date("d-m-Y", $data['companyLicenseApp']["data"][$a]["expired_date"]); ?></td>  
															<td><?php echo date("d-m-Y", $data['companyLicenseApp']["data"][$a]["earliest_renewal_date"]); ?></td> 
															<td><?php echo date("d-m-Y", $data['companyLicenseApp']["data"][$a]["last_renewal_date"]); ?></td>   
															<?php if($data["companyLicenseApp"]["data"][$a]["status"] == 1){ ?> 
																<td>Draft</td>    
															<?php }elseif($data["companyLicenseApp"]["data"][$a]["status"] == 2){ ?>  
																<td>Need Approval</td>    
															<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] <= time()){ ?> 
																<td>Open to Renew</td> 
															<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["expired_date"] <= time()){ ?> 
																<td>Expired</td>   
															<?php }elseif($data["companyLicenseApp"]["data"][$a]["status"] == 3){ ?> 
																<td>Active</td>     
															<?php }elseif($data["companyLicenseApp"]["data"][$a]["status"] == 4){ ?>
																<td>Not Active</td>      
															<?php } ?>
															<?php if($data["companyLicenseApp"]["data"][$a]["renewal_status"] == 1){ ?>  
																<td>Open</td>   
															<?php }elseif($data["companyLicenseApp"]["data"][$a]["renewal_status"] == 2){ ?>  
																<td>Follow Up</td>   
															<?php }elseif($data["companyLicenseApp"]["data"][$a]["renewal_status"] == 3){ ?>  
																<td>Renew</td>  
															<?php }elseif($data["companyLicenseApp"]["data"][$a]["renewal_status"] == 4){ ?> 
																<td>Close Permit</td>  
															<?php }elseif($data["companyLicenseApp"]["data"][$a]["renewal_status"] == 5){ ?> 
																<td>Renew Approved</td>  
															<?php }elseif($data["companyLicenseApp"]["data"][$a]["renewal_status"] == 6){ ?> 
																<td>Close Permit Approved</td>  
															<?php } ?>
															<td><?php echo $data["companyLicenseApp"]["data"][$a]["remark"]; ?></td>
															<td><?php echo $data["companyLicenseApp"]["data"][$a]["reminder_counter"]; ?></td>  
															<?php if($data["companyLicenseApp"]["data"][$a]["created_user_id"] == 0) { ?>
																<td> </td>
															<?php }else{ $UserCreate = $this->model('userModel')->getDataUser($token, $data["companyLicenseApp"]["data"][$a]["created_user_id"])?>
																<td><?php echo $UserCreate["data"]["username"];  ?></td>
															<?php }?>
															<?php if($data["companyLicenseApp"]["data"][$a]["created_at"] == 0) { ?>
																<td> </td>
															<?php }else{ ?>
																<td><?php echo date("d-m-Y", $data["companyLicenseApp"]["data"][$a]["created_at"]); ?></td> 
															<?php }?>
															<?php if($data["companyLicenseApp"]["data"][$a]["updated_user_id"] == 0) { ?>
																<td> </td>
															<?php }else{ $UserUpdt = $this->model('userModel')->getDataUser($token, $data["companyLicenseApp"]["data"][$a]["updated_user_id"])?>
																<td><?php echo $UserUpdt["data"]["username"];  ?></td>
															<?php }?>
															<?php if($data["companyLicenseApp"]["data"][$a]["updated_at"] == 0) { ?>
																<td> </td>
															<?php }else{ ?>
																<td><?php echo date("d-m-Y", $data["companyLicenseApp"]["data"][$a]["updated_at"]); ?></td> 
															<?php }?>
															<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>  
															<td>  
																<?php if($data['Access']["data"][$i]["update_flag"] == true) { ?>
																	<a href="<?= base_url; ?>/companyLicense/edit/<?php echo $data["companyLicenseApp"]["data"][$a]["company_license_id"]; ?>" name="edit" class="btn btn-primary">Edit</a>   
																<?php } ?>
																<?php if($data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<a href="#myModal" class="btn btn-danger" data-href="<?= base_url; ?>/CompanyLicense/deleteCompanyLicense/<?php echo $data["companyLicenseApp"]["data"][$a]["company_license_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a>  
																<?php } ?> 
																<?php $id = $data["companyLicenseApp"]["data"][$a]["company_license_id"];
																$CompanyID = $data["companyLicenseApp"]["data"][$a]["company_id"];
																if(($data["companyLicenseApp"]["data"][$a]["status"] == 2 || $data["companyLicenseApp"]["data"][$a]["renewal_status"] == 3) && $data["userRole"]["data"]["role_code"] == "LEGALMGR") {  ?>
																<input type="button" name="cancel" class="btn btn-danger" value="Approve" onclick="approveLicense(<?php echo $id; ?>)" />
																<?php } 
																if($data["companyLicenseApp"]["data"][$a]["renewal_status"] == 4 && $data["userRole"]["data"]["role_code"] == "LEGALMGR") {   ?>  
																	<button type="button" class="btn btn-danger" data-toggle="modal" data-target="#myModal3" data-companylicenseid="<?php echo $data["companyLicenseApp"]["data"][$a]["company_license_id"]; ?>">Approve</button>
																<?php } ?>
															</td>     
														<?php } ?>
														</tr>
													<?php }} ?>
												</tbody>
												<tfoot>
													<tr>
														<th>Company</th> 
														<th>License Type</th> 
														<th>License No</th>   
														<th>Expired Date</th> 
														<th>Earliest Renewal Date</th> 
														<th>Last Renewal Date</th> 
														<th>Status</th> 
														<th>Renewal Status</th>   
														<th>Remark</th> 
														<th>Reminder No</th> 
														<th>Created By</th> 
														<th>Created Date</th> 
														<th>Updated By</th> 
														<th>Updated Date</th> 
														<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
															<th>Action</th>  
														<?php } ?>
													</tr>
												</tfoot>
											</table> 
													</div>
													<!-- /.card-body -->
										</div>
										<!-- /.card -->
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container-fluid -->
					<?php }}
				if($data['Access']["data"][$i]["form_php"] == "CompanyLicense"){  
					if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<div class="container-fluid">
								<div class="row">
									<div class="col-12">  
										<div class="card">
											<div class="card-header">
												<h3 class="card-title">Company License You Need To Concern</h3>
											</div>
											<!-- /.card-header -->
											<div class="card-body"> 
												<?php if ($data["userRole"]["data"]["role_code"] == "BOD"){ ?>
													<table id="ConcernlicenseBOD" class="table table-bordered table-striped">
												<?php }else{ ?>
													<table id="Concernlicense" class="table table-bordered table-striped">
												<?php } ?>
													<thead>
														<tr> 
															<th>Company</th> 
															<th>License Type</th> 
															<th>Status</th> 
															<th>Severity</th> 
															<th>Renewal Status</th>  
															<th>License No</th>   
															<th>Expired Date</th> 
															<th>Earliest Renewal Date</th> 
															<th>Last Renewal Date</th>  
															<th>Remark</th> 
															<th>Reminder No</th> 
															<th>Created By</th> 
															<th>Created Date</th> 
															<th>Updated By</th> 
															<th>Updated Date</th> 
															<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																<th>Action</th>  
															<?php } ?>
														</tr>
													</thead>
													<tbody>
														<?php if(empty($data["companyLicense"]["data"])) { ?>
															<tr> 	 
																<td>-</td>
																<td>-</td>
																<td>-</td>
																<td>-</td>  
																<td>-</td>  
																<td>-</td>
																<td>-</td>   
																<td>-</td>
																<td>-</td>
																<td>-</td>
																<td>-</td>
																<td>-</td>
																<td>-</td>  
																<td>-</td>  
																<td>-</td>
																<td>-</td> 
																<td>-</td> 
																<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<th>-</th>  
																<?php } ?> 
															</tr> 
														<?php }else{  for ($x = 0; $x < count($data["companyLicense"]["data"]); $x++) { ?>   
															<tr>
																<td><?php echo $data["companyLicense"]["data"][$x]["company_name"]; ?></td> 
																<td><?php echo $data["companyLicense"]["data"][$x]["license_type_name"]; ?></td>
																<?php if($data["companyLicense"]["data"][$x]["status"] == 1){ ?> 
																	<td>Draft</td>    
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 2){ ?>  
																	<td>Need Approval</td>        
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] < $data["companyLicense"]["data"][$x]["expired_date"]){ ?> 
																	<td style="background-color:#FFFF00;">Open to Renew</td> 
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["expired_date"] <= time()){ ?> 
																	<td style="background-color:#FF0000;">Expired</td>   
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3){ ?> 
																	<td style="background-color:#00FF00;">Active</td>     
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 4){ ?>
																	<td>Not Active</td>      
																<?php } ?>
																<?php if($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] < $data["companyLicense"]["data"][$x]["last_renewal_date"]){ ?>
																	<td>LOW</td> 
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["last_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["last_renewal_date"] < $data["companyLicense"]["data"][$x]["expired_date"]){ ?>
																	<td>HIGH</td>   
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["expired_date"] < time()){ ?> 
																	<td>CRITICAL</td>        
																<?php }else{?>
																	<td> </td>
																<?php } ?>
																<?php if($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 1){ ?>  
																	<td>Open</td>   
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 2){ ?>  
																	<td>Follow Up</td>   
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 3){ ?>  
																	<td>Renew</td>  
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 4){ ?> 
																	<td>Close Permit</td>  
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 5){ ?> 
																	<td>Renew Approved</td>  
																<?php }elseif($data["companyLicense"]["data"][$x]["status"] == 3 && $data["companyLicense"]["data"][$x]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$x]["renewal_status"] == 6){ ?> 
																	<td>Close Permit Approved</td>  
																<?php }else{ ?>
																	<td> </td>
																<?php } ?>
																<td><?php echo $data["companyLicense"]["data"][$x]["license_no"]; ?></td>  
																<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$x]["expired_date"]); ?></td>  
																<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$x]["earliest_renewal_date"]); ?></td> 
																<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$x]["last_renewal_date"]); ?></td>   
																<td><?php echo $data["companyLicense"]["data"][$x]["remark"]; ?></td>
																<td><?php $Earliest = $data['companyLicense']["data"][$x]["earliest_renewal_date"];
																$Hmin1Exp = $data['companyLicense']["data"][$x]["expired_date"] - 86400;
																$cut = $Hmin1Exp - $Earliest;
																 echo $data["companyLicense"]["data"][$x]["reminder_counter"]; ?>/<?php echo round($cut/604800); ?> </td>  
																<?php if($data["companyLicense"]["data"][$x]["created_user_id"] == 0) { ?>
																	<td> </td>
																<?php }else{ $UserCreate = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"][$x]["created_user_id"])?>
																	<td><?php echo $UserCreate["data"]["username"];  ?></td>
																<?php }?>
																<?php if($data["companyLicense"]["data"][$x]["created_at"] == 0) { ?>
																	<td> </td>
																<?php }else{ ?>
																	<td><?php echo date("d-m-Y", $data["companyLicense"]["data"][$x]["created_at"]); ?></td> 
																<?php }?>
																<?php if($data["companyLicense"]["data"][$x]["updated_user_id"] == 0) { ?>
																	<td> </td>
																<?php }else{ $UserUpdt = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"][$x]["updated_user_id"])?>
																	<td><?php echo $UserUpdt["data"]["username"];  ?></td>
																<?php }?>
																<?php if($data["companyLicense"]["data"][$x]["updated_at"] == 0) { ?>
																	<td> </td>
																<?php }else{ ?>
																	<td><?php echo date("d-m-Y", $data["companyLicense"]["data"][$x]["updated_at"]); ?></td> 
																<?php }?>
																<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>  
																	<td> 
																		<?php if($data['Access']["data"][$i]["update_flag"] == true) { ?>
																			<a href="<?= base_url; ?>/CompanyLicense/edit/<?php echo $data["companyLicense"]["data"][$x]["company_license_id"]; ?>" name="edit" class="btn btn-primary">Edit</a> 
																		<?php } ?>
																		<?php if($data['Access']["data"][$i]["delete_flag"] == true) { ?>
																			<a href="#myModal" class="btn btn-danger" data-href="<?= base_url; ?>/CompanyLicense/deleteCompany/<?php echo $data["companyLicense"]["data"][$x]["company_license_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a> 
																		<?php } ?>
																	</td>    
																<?php } ?>
															</tr>
														<?php }} ?>
													</tbody>
													<tfoot>
														<tr> 
															<th>Company</th> 
															<th>License Type</th> 
															<th>License No</th> 
															<th>Status</th> 
															<th>Severity</th> 
															<th>Renewal Status</th>   
															<th>Expired Date</th> 
															<th>Earliest Renewal Date</th> 
															<th>Last Renewal Date</th>   
															<th>Remark</th> 
															<th>Reminder No</th> 
															<th>Created By</th> 
															<th>Created Date</th> 
															<th>Updated By</th> 
															<th>Updated Date</th> 
															<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																<th>Action</th>  
															<?php } ?>
														</tr>
													</tfoot>
												</table> 
											</div>
											<!-- /.card-body -->
										</div>
										<!-- /.card -->
									</div>
									<!-- /.col -->
								</div>
								<!-- /.row -->
							</div>
							<!-- /.container-fluid -->
					<?php }}}}?>
					<!-- /.content --> 
						</section>
						<!-- /.content --> 
				</div>
				<!-- /.content-wrapper -->

				<!-- Control Sidebar -->
				<aside class="control-sidebar control-sidebar-dark">
					<!-- Control sidebar content goes here -->
				</aside>
				<!-- /.control-sidebar --> 
				<?php 
				require_once '../app/views/templates/modal.php';
				?>   
			</body>
			</html>
