<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>

			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
						if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<!-- Main content -->
							<section class="content">
							<div class="row">
								<div class="col-sm-12">
								<?php
									Flasher::Message();
								?>
								</div>
							</div>
				<div class="container-fluid">
					<div class="row">
						<!-- left column -->
						<div class="col-md-12">
							<!-- general form elements -->
							<div class="card card-primary">
								<div class="card-header">
									<h3 class="card-title">Edit Data</h3>
								</div>
								<!-- /.card-header -->
								<!-- form start -->  
									<form action="<?= base_url; ?>/ChangePassword/prosesChangePassword" method="POST" name="form1">
										<div class="row">
											<div class="card-body col-md-12">
												<div class="form-group">
													<label for="exampleInputEmail1">New Password</label>
													<input type="password" name="Password" class="form-control" id="exampleInputOSName" placeholder="Enter New Password">
												</div> 
												<div class="form-group">
													<label for="exampleInputEmail1">Confirm Password</label>
													<input type="password" name="ConPassword" class="form-control" id="exampleInputOSName" placeholder="Enter Confirm Passowrd">
												</div> 
											</div>
										</div>
										<!-- /.card-body -->
										<div class="card-footer">
											<button type="submit" name="submit" class="btn btn-danger">Submit</button> 
										</div>
									</form> 
							</div>
							<!-- /.card -->
						</div>
					</div>
					<!-- /.row -->
				</div><!-- /.container-fluid -->
			</section>
			<!-- /.content -->
							<?php }}}}?>
							<!-- /.content --> 
						</div>
						<!-- /.content-wrapper -->

						<!-- Control Sidebar -->
						<aside class="control-sidebar control-sidebar-dark">
							<!-- Control sidebar content goes here -->
						</aside>
						<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
				</body>
				</html>
