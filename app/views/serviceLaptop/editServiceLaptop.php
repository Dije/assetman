<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= EDIT ================================= 
						if($data['Access']["data"][$i]["update_flag"] == true) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Edit Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start -->  
													<form action="<?= base_url; ?>/ServiceLaptop/updateServiceLaptop" method="POST"  name="form1">
												<div class="row">
													<div class="card-body col-md-6">
													<input type="hidden" name="service_laptop_id" class="form-control" id="exampleInputOSName" placeholder="Enter Laptop Code" value="<?php echo $data["serviceLaptop"]["data"]["service_laptop_id"]; ?>"> 
														<div class="form-group"> 
														<label for="exampleInputEmail1">Laptop Code</label>
														<select name="laptop_id" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['serviceLaptop']["data"]["laptop_id"]; ?>"><?php echo $data['serviceLaptop']["data"]["laptop_code"]; ?></option> 
															<?php for ($x = 0; $x < count($data['laptop']["data"]); $x++) { ?>  
															<option value="<?php echo $data['laptop']["data"][$x]["laptop_id"]; ?>"><?php echo $data['laptop']["data"][$x]["laptop_code"]; ?></option> 
															<?php } ?>
														</select> 
														</div>
														<div class="form-group">
														<label>Service Date</label>
															<div class="input-group date" name="date_service" id="reservationdate" data-target-input="nearest" >
																<?php if($data['serviceLaptop']["data"]["date_service"] != 0) {?>
																	<input type="text" id="dateee" name="date_service" value="<?php echo date("d-m-Y", $data['serviceLaptop']["data"]["date_service"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate" />
																<?php } else { ?> 
																	<input type="text" id="dateee" name="date_service" value="" class="form-control datetimepicker-input" data-target="#reservationdate" />
																<?php } ?>
																<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
																	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																</div> 
															</div>
														</div>
														<div class="form-group">
														<label>Done Date</label>
															<div class="input-group date" name="date_done" id="reservationdate3" data-target-input="nearest" >
															<?php if($data['serviceLaptop']["data"]["date_done"] != 0) {?>
																<input type="text" id="dateeee" name="date_done" value="<?php echo date("d-m-Y", $data['serviceLaptop']["data"]["date_done"]); ?>" class="form-control datetimepicker-input" data-target="#reservationdate3" />
															<?php } else { ?> 
																<input type="text" id="dateeee" name="date_done" value="" class="form-control datetimepicker-input" data-target="#reservationdate3" />
															<?php } ?>
															<div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
															<div class="input-group-text"><i class="fa fa-calendar"></i></div>
															</div>
														</div>
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Company</label>
															<input type="text" name="company" class="form-control" id="exampleInputOSName" placeholder="Enter Company" value="<?php echo $data["serviceLaptop"]["data"]["company"]; ?>"> 
														</div> 
													</div>
													<div class="card-body col-md-6"> 
														<div class="form-group">
														<label for="exampleInputEmail1">Price</label>
														<input type="text" name="price" class="form-control" id="exampleInputOSName" placeholder="Enter Price" value="<?php echo $data["serviceLaptop"]["data"]["price"]; ?>"> 
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">problem</label>
														<input type="text" name="problem" class="form-control" id="exampleInputOSName" placeholder="Enter Problem" value="<?php echo $data["serviceLaptop"]["data"]["problem"]; ?>"> 
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">solved</label>
														<input type="text" name="solved" class="form-control" id="exampleInputOSName" placeholder="Enter Solved" value="<?php echo $data["serviceLaptop"]["data"]["solved"]; ?>"> 
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Remark</label>
														<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"><?php echo $data["serviceLaptop"]["data"]["remark"];?></textarea> 
														</div>   
													</div>
													<!-- /.card-body --> 

													</div>
													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="servicelaptop()">
													</div>
												</form>

									</div>
									<!-- /.card -->
								</div>
							</div>
							<!-- /.row -->
						</div><!-- /.container-fluid -->
					</section>
						<?php }}}} ?>
						<!-- /.content --> 
					</div>
					<!-- /.content-wrapper -->

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html> 