<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= CREATE ================================= 
						if($data['Access']["data"][$i]["create_flag"] == true ) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/Ink/addInk" method="POST" name="form1">
													<div class="card-body">
														<div class="form-group">
															<label for="exampleInputEmail1">Ink Name *</label>
															<input type="text" name="InkName" class="form-control" id="exampleInputOSName" placeholder="Enter Ink Name">
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea> 
														</div> 
														<div class="row">
															<div class="col-md-6">
																<p>BK- : Black</p> 
																<p>C-  : Cyan</p> 
																<p>M-  : Magenta</p> 
																<p>Y-  : Yellow</p> 
															</div>
															<div class="col-md-6">
																<p>CL- : Color</p> 
																<p>Tricolor- : Tricolor</p> 
																<p>Others- : Others</p>
															</div>
														</div> 
													</div>
													<!-- /.card-body -->

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="ink()">
													</div>
												</form>

											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
						<?php }}}} ?>
					</div>
					<!-- /.content -->  

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>
 