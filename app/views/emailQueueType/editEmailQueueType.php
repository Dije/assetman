<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= EDIT ================================= 
						if($data['Access']["data"][$i]["update_flag"] == true) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Edit Data</h3>
												</div>
												<!-- /.card-header -->
												<!-- form start --> 
													<form action="<?= base_url; ?>/EmailQueueType/updateEmailQueueType" method="POST" name="form1">
														<div class="row">
															<div class="card-body col-md-6">
															<input type="hidden" name="email_queue_type_id" class="form-control" id="exampleInputOSName" placeholder="Enter Email Queue Type Name" value="<?php echo $data["emailQueueType"]["data"]["email_queue_type_id"]; ?>">
																<div class="form-group">
																	<label for="exampleInputEmail1">Email Queue Type Name *</label>
																	<input type="text" name="EmailQueueTypeName" class="form-control" id="exampleInputOSName" placeholder="Enter Email Queue Type Name" value="<?php echo $data["emailQueueType"]["data"]["email_queue_type_name"]; ?>">
																</div>
																<div class="form-group">
																	<label for="exampleInputEmail1">Table Reference *</label>
																	<input type="text" name="TableReference" class="form-control" id="exampleInputOSName" placeholder="Enter Table Reference Name" value="<?php echo $data["emailQueueType"]["data"]["table_reference"]; ?>">
																</div>
																<div class="form-group">
																	<label for="exampleInputEmail1">Field Reference *</label>
																	<input type="text" name="FieldReference" class="form-control" id="exampleInputOSName" placeholder="Enter Field Reference Name" value="<?php echo $data["emailQueueType"]["data"]["field_reference"]; ?>">
																</div>
																<div class="form-group">
																	<label for="exampleInputEmail1">Recipient Name *</label>
																	<input type="text" name="RecipientName" class="form-control" id="exampleInputOSName" placeholder="Enter Recipient Name" value="<?php echo $data["emailQueueType"]["data"]["recipient_name"]; ?>">
																</div>
																<div class="form-group">
																	<label for="exampleInputEmail1">Remark</label>
																	<textarea id="Remark" name="Remark" class="form-control" rows="8" cols="50" placeholder="Enter Remark"><?php echo $data["emailQueueType"]["data"]["remark"]; ?></textarea> 
																</div>
															</div>
															<div class="card-body col-md-6">
																<div class="form-group">
																	<label for="exampleInputEmail1">Email Recipient *</label>
																	<input type="text" name="EmailRecipient" class="form-control" id="exampleInputOSName" placeholder="Enter Email Recipient" value="<?php echo $data["emailQueueType"]["data"]["email_recipient"]; ?>">
																</div>
																<div class="form-group">
																	<label for="exampleInputEmail1">Email CC</label>
																	<input type="text" name="EmailCC" class="form-control" id="exampleInputOSName" placeholder="Enter Email CC" value="<?php echo $data["emailQueueType"]["data"]["email_cc"]; ?>">
																</div> 
																<div class="form-group">
																	<label for="exampleInputEmail1">Email Subject *</label>
																	<input type="text" name="EmailSubject" class="form-control" id="exampleInputOSName" placeholder="Enter Email Subject" value="<?php echo $data["emailQueueType"]["data"]["email_subject"]; ?>">
																</div>
																<div class="form-group">
																	<label for="exampleInputEmail1">Email Body</label>
																	<textarea id="Remark" name="EmailBody" class="form-control" rows="4" cols="50" placeholder="Enter Email Body"><?php echo $data["emailQueueType"]["data"]["email_body"]; ?></textarea> 
																</div>
																<div class="form-group">
																	<label for="exampleInputEmail1">Parameter Value</label>
																	<input type="text" name="ParamValue" class="form-control" id="exampleInputOSName" placeholder="Enter Email Parameter Value" value="<?php echo $data["emailQueueType"]["data"]["param_value"]; ?>">
																	<p style="position: absolute;right: 0;padding-right: 20px; font-size: 12px;">Seperate by ||</p>
																</div>
															</div>
														</div> 
														<!-- /.card-body -->
														<div class="card-footer">
															<button type="submit" name="submit" class="btn btn-danger">Submit</button>
															<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="emailQueueType()">
														</div>
													</form> 
											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
						<?php }}}} ?>
						<!-- /.content --> 
					</div>
					<!-- /.content-wrapper -->

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html> 