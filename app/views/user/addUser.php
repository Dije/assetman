<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= CREATE ================================= 
						if($data['Access']["data"][$i]["create_flag"] == true ) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/User/addUser" method="POST" enctype="multipart/form-data" name="form1">
													<div class="row">
														<div class="card-body col-md-6">
															<div class="form-group">
																<label for="exampleInputEmail1">Username *</label>
																<input type="text" name="Username" class="form-control" id="exampleInputUsername" placeholder="Enter Username">
															</div> 
															<input type="hidden" name="EmployeeID" class="form-control" id="EmpID" placeholder="Enter Employee ID" value=""> 
															<label for="exampleInputEmail1">NIK</label>
															<div class="input-group"> 
																<input type="text" id="mydata" name="NIK" class="form-control">
																<span class="input-group-append"> 
																	<input type="button" class="btn btn-info btn-flat" value="&#128269" data-toggle="modal" data-target="#myModal1">
																</span>
															</div>   
															<div class="form-group">
																<label for="exampleInputEmail1">Firstname</label>
																<input type="text" name="firstname" class="form-control" id="firstname" placeholder="Choose NIK" disabled>
															</div> 
															<div class="form-group">
																<label for="exampleInputEmail1">Lastname</label>
																<input type="text" name="Lastname" class="form-control" id="Lastname" placeholder="Choose NIK" disabled>
															</div> 
															<div class="form-group">
																<label for="exampleInputEmail1">Email *</label>
																<input type="text" name="Email" class="form-control" id="exampleInputEmail" placeholder="Enter Email">
															</div> 
														</div> 
														<div class="card-body col-md-6">  
															<div class="form-group">
																<label for="exampleInputEmail1">Division</label>
																<input type="text" name="Division" class="form-control" id="Division" placeholder="Choose NIK" disabled>
															</div> 
															<div class="form-group">
																<label for="exampleInputEmail1">Department</label>
																<input type="text" name="Department" class="form-control" id="Department" placeholder="Choose NIK" disabled>
															</div> 
															<div class="form-group">
																<label for="exampleInputEmail1">Section</label>
																<input type="text" name="Section" class="form-control" id="Section" placeholder="Choose NIK" disabled>
															</div> 
															<div class="form-group">
																<label for="exampleInputEmail1">Position</label>
																<input type="text" name="Position" class="form-control" id="Position" placeholder="Choose NIK" disabled>
															</div> 
															<div class="form-group">
																<label for="exampleInputEmail1">Remark</label>
																<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea> 
															</div>
														</div>
													</div>
													<!-- /.card-body -->

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="user()"> 	
													</div>
												</form>

											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
							<!-- /.content -->  
						<?php }}}} ?>
					</div>
					<!-- /.content -->  

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
				</body>
				</html>
 