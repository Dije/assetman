<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>

			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
						if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<!-- Main content -->
							<section class="content">
							<div class="row">
								<div class="col-sm-12">
								<?php
									Flasher::Message();
								?>
								</div>
							</div>
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">  
											<div class="card">
												<div class="card-header">
													<!-- ================================= CREATE =================================  -->
													<?php if($data['Access']["data"][$i]["create_flag"] == true ) { ?>  
														<a href="<?= base_url; ?>/Employee/add" class="btn btn-primary">Add</a> 
													<?php } ?>
													<!-- ================================= CREATE =================================  -->
												</div>
												<!-- /.card-header -->
												<div class="card-body"> 
													<table id="employeeTable" class="table table-bordered table-striped">
														<thead>
															<tr>
																<th>NIK</th>
																<th>Firstname</th>
																<th>Lastname</th> 
																<!-- <th>Initials</th>  
																<th>Signature</th>  -->
																<th>Division</th>
																<th>Department</th>
																<th>Section</th>
																<th>Position</th> 
																<th>Remark</th>  
																<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<th class="action">Action</th>  
																<?php } ?> 
															</tr> 
														</thead>
													</table> 
												</div>
												<!-- /.card-body -->
											</div>
											<!-- /.card -->
										</div>
										<!-- /.col -->
									</div>
									<!-- /.row -->
								</div>
								<!-- /.container-fluid -->
							</section>
							<!-- /.content --> 
							<?php }}}}?>
							<!-- /.content --> 
						</div>
						<!-- /.content-wrapper -->

						<!-- Control Sidebar -->
						<aside class="control-sidebar control-sidebar-dark">
							<!-- Control sidebar content goes here -->
						</aside>
						<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
				</body>
				</html>

				

  <!-- 		FOR INITIAL AND SIGNATURE
    <td>
        <//?php    
        $imgdata = $data["employee"]["data"][$x]["initials"]; 
        if($imgdata == null){}
            else{
                echo "<img src=\"data:image/png;base64,". $imgdata ."\" width='50' height='40' />";
            }
        ?> 
    </td> 
    <td>
        <//?php   
        $imgdata1 = $data["employee"]["data"][$x]["signature"]; 
        if($imgdata1 == null){}
            else{
        echo "<img src=\"data:image/png;base64,". $imgdata1 ."\" width='50' height='40' />";
    }
        ?> 
    </td>   -->