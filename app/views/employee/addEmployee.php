<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= CREATE ================================= 
						if($data['Access']["data"][$i]["create_flag"] == true ) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/Employee/addEmployee" method="POST" enctype="multipart/form-data" name="form1">
													<div class="row">
														<div class="card-body col-md-6">
															<div class="form-group">
																<label for="exampleInputEmail1">NIK *</label>
																<input type="text" name="NIK" class="form-control" id="exampleInputUsername" placeholder="Enter NIK">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Firstname *</label>
																<input type="text" name="Firstname" class="form-control" id="exampleInputFisrtname" placeholder="Enter Fistname">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Lastname</label>
																<input type="text" name="Lastname" class="form-control" id="exampleInputLastname" placeholder="Enter Lastname">
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Email *</label>
																<input type="text" name="Email" class="form-control" id="exampleInputEmail" placeholder="Enter Email">
															</div>
															<div class="form-group">
																<label>Division</label>
																<select id="divisionCB" name="DivisionID" class="form-control select2" style="width: 100%;"> 
																	<option value="" >Select Division</option>
																	<?php for ($x = 0; $x < count($data["division"]["data"]); $x++) { ?>  
																		<option value="<?php echo $data["division"]["data"][$x]["division_id"]; ?>"><?php echo $data["division"]["data"][$x]["division_name"]; ?></option> 
																	<?php } ?>
																</select>
															</div> 
															<div class="form-group">
																<label>Department</label>
																<select class="form-control" id="DepartmentCB" name="DepartmentID">
																	<option value="">Select Department</option>
																</select>
															</div> 
															<div class="form-group">
																<label>Section</label>
																<select class="form-control" id="SectionCB" name="SectionId">
																	<option value="">Select Section</option>
																</select>
															</div> 
															<div class="form-group">
																<label>Position</label>
																<select id="Position" name="PositionID" class="form-control select2" style="width: 100%;">  
																	<?php for ($xs = 0; $xs < count($data["position"]["data"]); $xs++) { ?>  
																		<option value="<?php echo $data["position"]["data"][$xs]["position_id"]; ?>"><?php echo $data["position"]["data"][$xs]["position_name"]; ?></option> 
																	<?php } ?>
																</select>
															</div> 
														</div>
														<div class="card-body col-md-6">
															<div class="form-group">
																<label>Location</label>
																<select id="Location" name="LocationID" class="form-control select2" style="width: 100%;">  
																	<?php for ($x = 0; $x < count($data["location"]["data"]); $x++) { ?>  
																		<option value="<?php echo $data["location"]["data"][$x]["location_id"]; ?>"><?php echo $data["location"]["data"][$x]["location_name"]; ?></option> 
																	<?php } ?>
																</select>
															</div> 
																<div class="form-group">
																<label for="exampleInputEmail1">Initial</label> 
																<div class="custom-file">
																	<label class="custom-file-label" for="exampleInputEmail1">Initial</label> 
																	<input type="file" name="Initials" class="custom-file-input" id="initial" placeholder="Enter Initial">
																	<img id="Initimg" width='100' height='80' />
																</div>
															</div> </br>
															<div class="form-group">
																<label for="exampleInputEmail1">Signature</label>
																<div class="custom-file">
																	<label class="custom-file-label" for="exampleInputEmail1">Signature</label> 
																	<input type="file" name="Signature" class="custom-file-input" id="signature" placeholder="Enter Signature">
																	<img id="Signimg" width='100' height='80' />
																</div>
															</div>  </br> 
															<div class="form-group">
																<label for="exampleInputEmail1">Remark</label>
																<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea> 
															</div>
														</div>
													</div>
													<!-- /.card-body -->

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="employee()">
													</div>
												</form>

											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
							<!-- /.content --> 
						<?php }}}} ?>
					</div>
					<!-- /.content -->  

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>
 