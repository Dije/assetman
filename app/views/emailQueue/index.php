<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>

			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
						if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<!-- Main content -->
							<section class="content">
							<div class="row">
								<div class="col-sm-12">
								<?php
									Flasher::Message();
								?>
								</div>
							</div>
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">  
											<div class="card">
												<!-- <div class="card-header"> --> 
													<!-- =============================== CREATE ================================ --> 
												<!-- 	<//?php if($data['Access']["data"][$i]["create_flag"] == true ) { ?> 
														<a href="</?= base_url; ?>/EmailQueue/add" class="btn btn-primary">Add</a> 
													<//?php } ?> -->
													<!-- =============================== CREATE ================================ --> 
												<!-- </div> -->
												<!-- /.card-header -->
												<div class="card-body"> 
													<table id="example1" class="table table-bordered table-striped">
														<thead>
															<tr>
																<!-- <//?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<th>Action</th>  
																<//?php } ?> -->
																<th>Email Queue Id</th>
																<th>Email Queue Type Name</th>
																<th>Email Recipient</th>
																<th>Email CC</th> 
																<th>Email Subject</th>
																<th>Email Body</th> 
																<th>Status</th>
																<th>Error Message</th> 
																<!-- <th>Remark</th>  -->
															</tr>
														</thead>
														<tbody>
															<?php if(empty($data["emailQueue"]["data"])) { ?>
																<tr>
																	<!-- <//?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																		<th>-</th>  
																	<//?php } ?> -->
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>  
																	<td>-</td>
																	<td>-</td> 
																	<td>-</td>   
																	<!-- <td>-</td>      -->
																</tr> 
															<?php }else{  for ($x = 0; $x < count($data["emailQueue"]["data"]); $x++) { ?>   
																<tr>
																	<!-- <//?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>  
																	 <td> 
																			<//?php if($data['Access']["data"][$i]["update_flag"] == true) { ?>
																				<a href="</?= base_url; ?>/EmailQueue/edit/<//?php echo $data["emailQueue"]["data"][$x]["email_queue_id"]; ?>" name="edit" class="btn btn-primary">Edit</a> 
																			<//?php } ?>
																			<//?php if($data['Access']["data"][$i]["delete_flag"] == true) { ?>
																				<a href="#myModal" class="btn btn-danger" data-href="</?= base_url; ?>/EmailQueue/deleteEmailQueue/<//?php echo $data["emailQueue"]["data"][$x]["email_queue_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a> 
																			<//?php } ?>
																		</td>  
																	<//?php } ?>  -->  
																	<td><?php echo $data["emailQueue"]["data"][$x]["email_queue_id"]; ?></td> 
																	<td><?php echo $data["emailQueue"]["data"][$x]["email_queue_type_name"]; ?></td> 
																	<td><?php echo $data["emailQueue"]["data"][$x]["email_recipient"]; ?></td> 
																	<td><?php echo $data["emailQueue"]["data"][$x]["email_cc"]; ?></td>  
																	<td><?php echo $data["emailQueue"]["data"][$x]["email_subject"]; ?></td> 
																	<td><?php echo $data["emailQueue"]["data"][$x]["email_body"]; ?></td>  
																	<td><?php echo $data["emailQueue"]["data"][$x]["status"]; ?></td> 
																	<td><?php echo $data["emailQueue"]["data"][$x]["error_message"]; ?></td>  
																	<!-- <td style="white-space: pre-line;"></?php echo $data["emailQueue"]["data"][$x]["remark"]; ?></td>    -->
																</tr>
																<?php }} ?>
															</tbody>
															<tfoot>
																<tr> 
																	<!-- <//?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																		<th>Action</th>  
																	<//?php } ?> --> 
																	<th>Email Queue Id</th>
																	<th>Email Queue Type Name</th>
																	<th>Email Recipient</th>
																	<th>Email CC</th> 
																	<th>Email Subject</th>
																	<th>Email Body</th> 
																	<th>Status</th>
																	<th>Error Message</th> 
																	<!-- <th>Remark</th>   -->
																</tr>
															</tfoot>
														</table> 
													</div>
													<!-- /.card-body -->
												</div>
												<!-- /.card -->
											</div>
											<!-- /.col -->
										</div>
										<!-- /.row -->
									</div>
									<!-- /.container-fluid -->
								</section>
								<!-- /.content -->
							<?php }}}}?>
							<!-- /.content --> 
						</div>
						<!-- /.content-wrapper -->

						<!-- Control Sidebar -->
						<aside class="control-sidebar control-sidebar-dark">
							<!-- Control sidebar content goes here -->
						</aside>
						<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
				</body>
				</html>
