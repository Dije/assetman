<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= CREATE ================================= 
						if($data['Access']["data"][$i]["create_flag"] == true ) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/ServiceUPS/addServiceUPS" method="POST"  name="form1">
												<div class="row">
													<div class="card-body col-md-6"> 
														<div class="form-group"> 
														<label for="exampleInputEmail1">UPS Code</label>
														<select name="ups_id" class="form-control select2" style="width: 100%;">  
															<?php for ($x = 0; $x < count($data['ups']["data"]); $x++) { ?>  
															<option value="<?php echo $data['ups']["data"][$x]["ups_id"]; ?>"><?php echo $data['ups']["data"][$x]["ups_code"]; ?></option> 
															<?php } ?>
														</select> 
														</div>
														<div class="form-group">
														<label>Service Date</label>
															<div class="input-group date" name="date_service" id="reservationdate" data-target-input="nearest" > 
																<input type="text" id="dateee" name="date_service" value="" class="form-control datetimepicker-input" data-target="#reservationdate" /> 
																<div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
																	<div class="input-group-text"><i class="fa fa-calendar"></i></div>
																</div> 
															</div>
														</div>
														<div class="form-group">
														<label>Done Date</label>
															<div class="input-group date" name="date_done" id="reservationdate3" data-target-input="nearest" > 
															<input type="text" id="dateeee" name="date_done" value="" class="form-control datetimepicker-input" data-target="#reservationdate3" /> 
															<div class="input-group-append" data-target="#reservationdate3" data-toggle="datetimepicker">
															<div class="input-group-text"><i class="fa fa-calendar"></i></div>
															</div>
														</div>
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Company</label>
															<input type="text" name="company" class="form-control" id="exampleInputOSName" placeholder="Enter Company"> 
														</div> 
													</div>
													<div class="card-body col-md-6"> 
														<div class="form-group">
														<label for="exampleInputEmail1">Price</label>
														<input type="text" name="price" class="form-control" id="exampleInputOSName" placeholder="Enter Price"> 
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">problem</label>
														<input type="text" name="problem" class="form-control" id="exampleInputOSName" placeholder="Enter Problem"> 
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">solved</label>
														<input type="text" name="solved" class="form-control" id="exampleInputOSName" placeholder="Enter Solved"> 
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Remark</label>
														<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea> 
														</div>   
													</div>
													<!-- /.card-body --> 

													</div>
													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="servicelaptop()">
													</div>
												</form>

											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
							<!-- /.content -->  
					<?php }}}} ?>
				</div>
			<!-- /.content -->  

			<!-- Control Sidebar -->
			<aside class="control-sidebar control-sidebar-dark">
			<!-- Control sidebar content goes here -->
			</aside>
		<!-- /.control-sidebar --> 
	</body>
</html> 
