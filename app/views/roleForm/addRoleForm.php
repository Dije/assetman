<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= CREATE ================================= 
						if($data['Access']["data"][$i]["create_flag"] == true ) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/RoleForm/addRoleForm" method="POST"  name="form1">
													<div class="card-body">
														<div class="form-group">
															<label for="exampleInputEmail1">Role *</label>
															<select id="RoleID" name="RoleID" class="form-control select2" style="width: 100%;">  
																<?php for ($y = 0; $y < count($data['role']["data"]); $y++) { ?>  
																	<option value="<?php echo $data['role']["data"][$y]["role_id"]; ?>"><?php echo $data['role']["data"][$y]["role_code"]; ?></option> 
																<?php } ?>
															</select>
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Form *</label>
															<select id="FormId" name="FormId" class="form-control select2" style="width: 100%;">  
																<?php for ($x = 0; $x < count($data['form']["data"]); $x++) { ?>  
																	<option value="<?php echo $data['form']["data"][$x]["form_id"]; ?>"><?php echo $data['form']["data"][$x]["form_code"]?></option> 
																<?php } ?>
															</select>
														</div>
														<div class="form-group">
															<div class="form-check">
																<input type="checkbox" name="Create" class="form-check-input" value="true">
																<label class="form-check-label">Create</label>
															</div>
														</div>
														<div class="form-group">
															<div class="form-check">
																<input type="checkbox" name="Read" class="form-check-input" value="true">
																<label class="form-check-label">Read</label>
															</div>
														</div>
														<div class="form-group">
															<div class="form-check">
																<input type="checkbox" name="Update" class="form-check-input" value="true">
																<label class="form-check-label">Update</label>
															</div>
														</div>
														<div class="form-group">
															<div class="form-check">
																<input type="checkbox" name="Delete" class="form-check-input" value="true">
																<label class="form-check-label">Delete</label>
															</div>
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea> 
														</div>
													</div>
													<!-- /.card-body -->

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="roleForm()">
													</div>
												</form>

											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
							<!-- /.content --> 
						<?php }}}} ?>
					</div>
					<!-- /.content -->  

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>
 