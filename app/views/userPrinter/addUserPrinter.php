<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= CREATE ================================= 
						if($data['Access']["data"][$i]["create_flag"] == true ) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/UserPrinter/addUserPrinter" method="POST"  name="form1">
													<div class ="row">
														<div class="card-body col-md-6">  
															<div class="form-group">
																<label for="exampleInputEmail1">Printer Code</label> 
																<input type="text" name="PrinterCode" class="form-control" id="exampleInputOSName" placeholder="Enter Printer Code Name"> 
															</div>
															<div class="form-group">
																<label for="exampleInputEmail1">Printer Type</label> 
																<select id="PrinterID" name="PrinterID" class="form-control select2" style="width: 100%;">  
																	<?php for ($y = 0; $y < count($data["printer"]["data"]); $y++) { ?>   ?>  
																		<option value="<?php echo $data["printer"]["data"][$y]["printer_id"]; ?>"><?php echo $data["printer"]["data"][$y]["printer_name"]?></option> 
																	<?php } ?>
																</select> 
															</div>															
															<div class="form-group">
																<label for="exampleInputEmail1">User</label> 
																<select id="UserID" name="UserID" class="form-control select2" style="width: 100%;"> 
																	<?php for ($x = 0; $x < count($data["user"]["data"]); $x++) { ?>  
																		<?php $EmployeeID = $data["user"]["data"][$x]["employee_id"];
																		if($EmployeeID == 0){} else{ ?>
																		<option value="<?php echo $data["user"]["data"][$x]["user_id"]; ?>">
																		<?php $data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $EmployeeID);
																			echo $data["employee"]["data"]["first_name"] .' '. $data["employee"]["data"]["last_name"];}?></option>  
																	<?php } ?> 
																</select>  
															</div>
														</div>
														<div class="card-body col-md-6"> 
															<div class="form-group">
																<label for="exampleInputEmail1">Price</label> 
																<input type="text" name="Price" class="form-control" id="exampleInputOSName" placeholder="Enter Price Name">  
															</div> 
															<div class="form-group">
																<label for="exampleInputEmail1">Warranty</label> 
																<input type="text" name="Warranty" class="form-control" id="exampleInputOSName" placeholder="Enter Warranty">  
															</div>   
															<div class="form-group">
																<label for="exampleInputEmail1">Remark</label>
																<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea> 
															</div>
														</div>
													</div> 
													<!-- /.card-body -->

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="userPrinter()">
													</div>
												</form>

											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
							<!-- /.content -->  
					<?php }}}} ?>
				</div>
			<!-- /.content -->  

			<!-- Control Sidebar -->
			<aside class="control-sidebar control-sidebar-dark">
			<!-- Control sidebar content goes here -->
			</aside>
		<!-- /.control-sidebar --> 
	</body>
</html> 
