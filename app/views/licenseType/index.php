<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>


			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
						if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<!-- Main content -->
							<section class="content">
							<div class="row">
								<div class="col-sm-12">
								<?php
									Flasher::Message();
								?>
								</div>
							</div>
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">  
											<div class="card">
												<div class="card-header">
													<!-- =============================== CREATE ================================ --> 
													<?php if($data['Access']["data"][$i]["create_flag"] == true ) { ?> 
														<a href="<?= base_url; ?>/LicenseType/add" class="btn btn-primary">Add</a> 
													<?php } ?>
													<!-- =============================== CREATE ================================ --> 
												</div>
												<!-- /.card-header -->
												<div class="card-body"> 
													<table id="example1" class="table table-bordered table-striped"> 
														<thead>
															<tr>
																<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<th class="action">Action</th>  
																<?php } ?> 
																<th>License Type</th>  
																<th>Reminder Before Month</th> 
																<th>Management Reminder Before Month</th> 
																<th>Reminder Frequency Day</th> 
																<th>Management Reminder Frequency Day</th>   
																<th>Remark</th>  
															</tr> 
														</thead>
														<tbody>
															<?php if(empty($data["licenseType"]["data"])) { ?>
																<tr>
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																		<th class="action">-</th>  
																	<?php } ?> 
																	<td>-</td>
																	<td>-</td>
																	<td>-</td> 
																	<td>-</td>  
																	<td>-</td>
																	<td>-</td> 
																</tr> 
															<?php }else{  for ($x = 0; $x < count($data["licenseType"]["data"]); $x++) { ?>   
																<tr class="action">
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>  
																		<td class="center"> 
																			<?php if($data['Access']["data"][$i]["update_flag"] == true) { ?>
																				<a href="<?= base_url; ?>/LicenseType/edit/<?php echo $data["licenseType"]["data"][$x]["license_type_id"]; ?>" name="edit" class="btn btn-primary">Edit</a> 
																			<?php } ?>
																			<?php if($data['Access']["data"][$i]["delete_flag"] == true) { ?>
																				<a href="#myModal" class="btn btn-danger" data-href="<?= base_url; ?>/LicenseType/deleteLicenseType/<?php echo $data["licenseType"]["data"][$x]["license_type_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a> 
																			<?php } ?>
																		</td>    
																	<?php } ?>
																	<td class="center"><?php echo $data["licenseType"]["data"][$x]["license_type_name"]; ?></td>  
																	<td class="center"><?php echo $data["licenseType"]["data"][$x]["reminder_before_month"]; ?></td>  
																	<td class="center"><?php echo $data["licenseType"]["data"][$x]["management_reminder_before_month"]; ?></td>  
																	<td class="center"><?php echo $data["licenseType"]["data"][$x]["reminder_frequency_day"]; ?></td>  
																	<td class="center"><?php echo $data["licenseType"]["data"][$x]["management_reminder_frequency_day"]; ?></td>  
																	<td class="center" style="white-space: pre-line;"><?php echo $data["licenseType"]["data"][$x]["remark"]; ?></td>  
																</tr>
																<?php }} ?>
															</tbody>
															<tfoot>
																<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<th class="action">Action</th>  
																<?php } ?> 
																<th>License Type</th>  
																<th>Reminder Before Month</th> 
																<th>Management Reminder Before Month</th> 
																<th>Reminder Frequency Day</th> 
																<th>Management Reminder Frequency Day</th>   
																<th>Remark</th>    
															</tr> 
														</tfoot>
													</table> 
												</div>
												<!-- /.card-body -->
											</div>
											<!-- /.card -->
										</div>
										<!-- /.col -->
									</div>
									<!-- /.row -->
								</div>
								<!-- /.container-fluid -->
							</section>
							<!-- /.content -->
							<?php }}}}?>
						</div>
						<!-- /.content-wrapper -->

						<!-- Control Sidebar -->
						<aside class="control-sidebar control-sidebar-dark">
							<!-- Control sidebar content goes here -->
						</aside>
						<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
				</body>
				</html>
 