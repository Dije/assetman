<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= CREATE ================================= 
						if($data['Access']["data"][$i]["create_flag"] == true ) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/LicenseType/addLicenseType" method="POST" name="form1">
													<div class="row">
													<div class="card-body col-md-6">
														<div class="form-group">
															<label for="exampleInputEmail1">License Type Name *</label>
															<input type="text" name="LicenseTypeName" class="form-control" id="exampleInputOSName" placeholder="Enter License Type Name">
														</div> 
														<div class="form-group">
															<label for="exampleInputEmail1">Reminder Before Month</label>  
															<input type="text" name="ReminderBeforeMonth" class="form-control" id="exampleInputOSName" placeholder="Enter Reminder Before Month">
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Management Reminder Before Month</label> 
															<input type="text" name="ManagementReminderBeforeMonth" class="form-control" id="exampleInputOSName" placeholder="Enter Management Reminder Before Month">
														</div>
														<div class="form-group">
															<label for="exampleInputEmail1">Reminder Frequency Before Day</label> 
															<input type="text" name="ReminderFrequencyDay" class="form-control" id="exampleInputOSName" placeholder="Enter Reminder Frequency Before Day">
														</div>  
														<div class="form-group">
															<label for="exampleInputEmail1">Management Reminder Frequency Before Day</label> 
															<input type="text" name="ManagementReminderFrequencyDay" class="form-control" id="exampleInputOSName" placeholder="Enter Management Reminder Frequency Before Day"> 
														</div>
													</div>
													<div class="card-body col-md-6">
														<div class="form-group">
															<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea> 
														</div>
													</div>
													</div>
													<!-- /.card-body -->

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="licenseType()">
													</div>
												</form>

											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
						<?php }}}} ?>
					</div>
					<!-- /.content -->  

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>
 