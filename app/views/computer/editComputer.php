<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= EDIT ================================= 
						if($data['Access']["data"][$i]["update_flag"] == true) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Edit Data</h3>
												</div>  
												<!-- /.card-header -->
												<!-- form start -->  
													<form action="<?= base_url; ?>/UserComputer/updateComputer" method="POST"  name="form1">
												<div class="row">
													<div class="card-body col-md-6">
													<input type="hidden" name="computer_id" class="form-control" id="exampleInputOSName" placeholder="Enter Computer Code" value="<?php echo $data["computer"]["data"]["computer_id"]; ?>">
														<div class="form-group">
														<label for="exampleInputEmail1">Computer Code</label>
														<input type="text" name="ComputerCode" class="form-control" id="exampleInputOSName" placeholder="Enter Computer Code" value="<?php echo $data["computer"]["data"]["computer_code"]; ?>">
														</div> 
														<div class="form-group"> 
														<label for="exampleInputEmail1">Username</label>
														<select name="Username" class="form-control select2" style="width: 100%;">  
																<option value="<?php echo $data["computer"]["data"]["user_id"]; ?>">
																<?php $UserID = $data["computer"]["data"]["user_id"];
																$data['computerUser'] = $this->model('userModel')->getDataUser($token, $UserID); 
																$EmployeeID = $data["computerUser"]["data"]["employee_id"];
																$data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $EmployeeID);
																	echo $data["employee"]["data"]["first_name"] .' '. $data["employee"]["data"]["last_name"];?></option> 
																<?php for ($x = 0; $x < count($data["user"]["data"]); $x++) { ?>  
																<?php $EmployeeID = $data["user"]["data"][$x]["employee_id"];
																if($EmployeeID == 0){} else{ ?>
																<option value="<?php echo $data["user"]["data"][$x]["user_id"]; ?>">
																<?php $data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $EmployeeID);
																	echo $data["employee"]["data"]["first_name"] .' '. $data["employee"]["data"]["last_name"];}?></option>  
															<?php } ?>
														</select>  
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Processor Name</label>
														<select name="ProcessorName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["processor_id"]; ?>"><?php echo $data['computer']["data"]["processor_name"]; ?></option> 
															<?php for ($y = 0; $y < count($data['processor']["data"]); $y++) { ?>  
															<option value="<?php echo $data['processor']["data"][$y]["processor_id"]; ?>"><?php echo $data['processor']["data"][$y]["processor_name"]; ?></option> 
															<?php } ?>
														</select>  
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Motherboard Name</label>
														<select name="MotherboardName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["motherboard_id"]; ?>"><?php echo $data['computer']["data"]["motherboard_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['motherboard']["data"]); $x++) { ?>  
															<option value="<?php echo $data['motherboard']["data"][$x]["motherboard_id"]; ?>"><?php echo $data['motherboard']["data"][$x]["motherboard_name"]; ?></option> 
															<?php } ?>
														</select>   
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">RAM Name</label>
														<select name="RAMName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["ram_id"]; ?>"><?php echo $data['computer']["data"]["ram_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['ram']["data"]); $x++) { ?>  
															<option value="<?php echo $data['ram']["data"][$x]["ram_id"]; ?>"><?php echo $data['ram']["data"][$x]["ram_name"]; ?></option> 
															<?php } ?>
														</select>    
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">HDD Name</label>
														<select name="HDDName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["hdd_id"]; ?>"><?php echo $data['computer']["data"]["hdd_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['hdd']["data"]); $x++) { ?>  
															<option value="<?php echo $data['hdd']["data"][$x]["hdd_id"]; ?>"><?php echo $data['hdd']["data"][$x]["hdd_name"]; ?></option> 
															<?php } ?>
														</select> 
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">CD</label>
														<select name="CD" class="form-control select2" style="width: 100%;"> 
														<?php if($data['computer']["data"]["cd"] == 1){?>
															<option value="YES">YES</option>  
															<option value="NO">NO</option>  
															<?php }else{ ?>
															<option value="NO">NO</option>  
															<option value="YES">YES</option>  
															<?php } ?>
														</select>  
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Casing Name</label>
														<select name="CasingName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["casing_id"]; ?>"><?php echo $data['computer']["data"]["casing_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['casing']["data"]); $x++) { ?>  
															<option value="<?php echo $data['casing']["data"][$x]["casing_id"]; ?>"><?php echo $data['casing']["data"][$x]["casing_name"]; ?></option> 
															<?php } ?>
														</select>  
														</div>
													</div>
													<div class="card-body col-md-6">
														<div class="form-group">
														<label for="exampleInputEmail1">Keyboard Name</label>
														<select name="KeyboardName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["keyboard_id"]; ?>"><?php echo $data['computer']["data"]["keyboard_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['keyboard']["data"]); $x++) { ?>  
															<option value="<?php echo $data['keyboard']["data"][$x]["keyboard_id"]; ?>"><?php echo $data['keyboard']["data"][$x]["keyboard_name"]; ?></option> 
															<?php } ?>
														</select>   
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Mouse Name</label>
														<select name="MouseName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["mouse_id"]; ?>"><?php echo $data['computer']["data"]["mouse_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['mouse']["data"]); $x++) { ?>  
															<option value="<?php echo $data['mouse']["data"][$x]["mouse_id"]; ?>"><?php echo $data['mouse']["data"][$x]["mouse_name"]; ?></option> 
															<?php } ?>
														</select>   
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Monitor Name</label>
														<select name="MonitorName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["monitor_id"]; ?>"><?php echo $data['computer']["data"]["monitor_name"]; ?></option>
															<?php for ($x = 0; $x < count($data['monitor']["data"]); $x++) { ?>  
															<option value="<?php echo $data['monitor']["data"][$x]["monitor_id"]; ?>"><?php echo $data['monitor']["data"][$x]["monitor_name"]; ?></option> 
															<?php } ?>
														</select>  
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">OS Name</label>
														<select name="OSName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["os_id"]; ?>"><?php echo $data['computer']["data"]["os_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['os']["data"]); $x++) { ?>  
															<option value="<?php echo $data['os']["data"][$x]["os_id"]; ?>"><?php echo $data['os']["data"][$x]["os_name"]; ?></option> 
															<?php } ?>
														</select>   
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Product Key OS</label>
														<input type="text" name="ProductKeyOS" class="form-control" id="exampleInputOSName" placeholder="Enter Product Key OS" value="<?php echo $data["computer"]["data"]["product_key_os"]; ?>">
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Office Name</label>
														<select name="OfficeName" class="form-control select2" style="width: 100%;"> 
															<option value="<?php echo $data['computer']["data"]["office_id"]; ?>"><?php echo $data['computer']["data"]["office_name"]; ?></option> 
															<?php for ($x = 0; $x < count($data['office']["data"]); $x++) { ?>  
															<option value="<?php echo $data['office']["data"][$x]["office_id"]; ?>"><?php echo $data['office']["data"][$x]["office_name"]; ?></option> 
															<?php } ?>
														</select> 
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Product Key Office</label>
														<input type="text" name="ProductKeyOffice" class="form-control" id="exampleInputOSName" placeholder="Enter Product Key Office" value="<?php echo $data["computer"]["data"]["product_key_office"]; ?>">
														</div>
														<div class="form-group">
														<label for="exampleInputEmail1">Remark</label>
														<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"><?php echo $data["computer"]["data"]["remark"];?></textarea> 
														</div> 
													</div>
													<!-- /.card-body --> 

													</div>
													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button>
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="computer()">
													</div>
												</form>

									</div>
									<!-- /.card -->
								</div>
							</div>
							<!-- /.row -->
						</div><!-- /.container-fluid -->
					</section>
						<?php }}}} ?>
						<!-- /.content --> 
					</div>
					<!-- /.content-wrapper -->

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html> 