<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="Content-Type:image/png; utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1"> 
  <title><?= $data['title']; ?></title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="icon" type="image/x-icon" href="<?= base_url; ?>/dist/logo.png">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/daterangepicker/daterangepicker.css">

  <link rel="stylesheet" href="<?= base_url; ?>/plugins/icheck-bootstrap/icheck-bootstrap.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/select2/css/select2.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/bootstrap4-duallistbox/bootstrap-duallistbox.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/bs-stepper/css/bs-stepper.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/dropzone/min/dropzone.min.css">
  
  <!-- Theme style -->
  <link rel="stylesheet" href="<?= base_url; ?>/dist/css/adminlte.css">
  <link rel="stylesheet" href="<?= base_url; ?>/dist/css/style.css">
  
  <!-- DataTables -->
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/datatables-responsive/css/responsive.bootstrap4.min.css">
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/datatables-buttons/css/buttons.bootstrap4.min.css">     
  <link rel="stylesheet" href="<?= base_url; ?>/plugins/datatables-colreorder/css/colReorder.bootstrap4.min">  
</head>
