<?php  
$token = $_SESSION['AccessToken'];
$UserID = $_SESSION['user_id'];
$EmployeeID = $_SESSION['employee_id'];
$data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $EmployeeID); 
?>

<!-- Main Sidebar Container -->
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<!-- Brand Logo -->
	<a href="<?= base_url; ?>/Home" class="brand-link"> 
		<span class="brand-text font-weight-light"><b>Deli Portal</b></span>
	</a>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar user panel (optional) -->
		<div class="user-panel mt-3 pb-3 mb-3 d-flex"> 
			<div class="info">
				<a href="<?= base_url; ?>/Home" class="d-block"><?php echo $data['employee']["data"]["first_name"] .' '. $data['employee']["data"]["last_name"] ?> </a>
			</div>
		</div> 
		<!-- SidebarSearch Form -->
		<div class="form-inline">
			<div class="input-group" data-widget="sidebar-search">
				<input class="form-control form-control-sidebar" type="search" placeholder="Search" aria-label="Search">
				<div class="input-group-append">
					<button class="btn btn-sidebar">
						<i class="fas fa-search fa-fw"></i>
					</button>
				</div>
			</div>
		</div> 

		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">  
			<?php  
			$decodeed = $this->model('formModel')->getDataFormJoinRole($token, $UserID, 0);
			// print_r($decodeed);
				if(empty($decodeed["data"])){
				}else{
					for($qc=0; $qc<count($decodeed["data"]); $qc++){  
						if($decodeed["data"][$qc]["form_type_code"] == "Head"){
							$FormParentID = $decodeed["data"][$qc]["form_id"];
						}?>
						<li class="nav-item">
							<a href="<?php echo base_url; ?>/<?php echo $decodeed["data"][$qc]["form_php"]; ?>" class="nav-link">
								<p>
									<?php echo $decodeed["data"][$qc]["form_code"]; ?>
									<i class="<?php echo $decodeed["data"][$qc]["class_tag"]; ?>"></i>
								</p>
							</a>
							<?php $dtSideBar = $this->model('formModel')->getDataFormJoinRole($token, $UserID, $FormParentID);
							if(empty($dtSideBar["data"])){
							}else{?>
								<ul class="nav nav-treeview"> 
									<?php for ($qb = 0; $qb < count($dtSideBar["data"]); $qb++) { 
										if($dtSideBar["data"][$qb]["form_type_code"] == "Child 1"){ 
											$FormChildID = $dtSideBar["data"][$qb]["form_id"];
											$classTag = $dtSideBar["data"][$qb]["class_tag"];
										}
										if($classTag == "hidden") {}else{?> 
										<li class="nav-item">
											<a href="<?php echo base_url; ?>/<?php echo $dtSideBar["data"][$qb]["form_php"]; ?>" class="nav-link">
												<i class="<?php echo $dtSideBar["data"][$qb]["class_tag"]; ?>"></i>
												<p>
													<?php echo $dtSideBar["data"][$qb]["form_code"]; ?>
												</p>
											</a>
											<?php $dtChildBar2 = $this->model('formModel')->getDataFormJoinRole($token, $UserID, $FormChildID);
											if(empty($dtChildBar2["data"])){
											}else{?>
												<ul class="nav nav-treeview child2"> 
													<?php for ($qa = 0; $qa < count($dtChildBar2["data"]); $qa++) {  ?> 
														<li class="nav-item">
															<a href="<?php echo base_url; ?>/<?php echo $dtChildBar2["data"][$qa]["form_php"]; ?>" class="nav-link">
																<i class="<?php echo $dtChildBar2["data"][$qa]["class_tag"]; ?>"></i>
																<p>
																	<?php echo $dtChildBar2["data"][$qa]["form_code"]; ?>
																</p>
															</a>
														</li>
													<?php } ?>
												</ul>
											<?php } ?>
										</li>
									<?php }} ?>
								</ul>
							<?php } ?>
						</li> 
					<?php }
				}
			?> 
		</ul>
	</nav>
	<!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->
</aside>


