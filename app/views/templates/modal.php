
<?php 
$token = $_SESSION['AccessToken'];
?>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">DELETE</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        ARE YOU SURE WANT TO DELETE THIS ROW?
      </div> 
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <a class="btn btn-danger btn-ok">Delete</a>
      </div> 
    </div>
  </div>
</div> 



<div class="modal fade bd-example-modal-xl" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Employee</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body table-responsive-xl">
          <?php $decoded = $this->model('employeeModel')->getDataEmployees($token);?>
        <table id="example1" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>  
              <th>NIK</th>
              <th>Firstname</th>
              <th>Lastname</th> 
              <th>Division</th> 
              <th>Department</th> 
              <th>Section</th> 
              <th>Position</th> 
              <th>Action</th> 
            </tr> 
          </thead>
          <tbody>
            <?php if(empty($decoded["data"])) { ?>
              <tr>
                <td>-</td>
                <td>-</td>
                <td>-</td> 
                <td>-</td>
                <td>-</td>  
                <td>-</td> 
                <td>-</td>
                <td>-</td> 
              </tr> 
            <?php }else{ for ($x = 0; $x < count($decoded["data"]); $x++) { ?>   
              <tr>    
                <td><?php echo $decoded["data"][$x]["nik"]; ?></td> 
                <td><?php echo $decoded["data"][$x]["first_name"]; ?></td> 
                <td><?php echo $decoded["data"][$x]["last_name"]; ?></td>  
                <td><?php echo $decoded["data"][$x]["division_name"]; ?></td>     
                <td><?php echo $decoded["data"][$x]["department_name"]; ?></td>     
                <td><?php echo $decoded["data"][$x]["section_name"]; ?></td>    
                <td><?php echo $decoded["data"][$x]["position_name"]; ?></td> 
                <td>   
                  <input type="button" value="Select" class="btn btn-primary" onclick="SetName(&quot;<?php echo $decoded["data"][$x]["employee_id"]; ?>&quot;,&quot;<?php echo $decoded["data"][$x]["nik"]; ?>&quot;, &quot;<?php echo $decoded["data"][$x]["first_name"]; ?>&quot;, &quot;<?php echo $decoded["data"][$x]["last_name"]; ?>&quot;, &quot;<?php echo $decoded["data"][$x]["division_name"]; ?>&quot;, &quot;<?php echo $decoded["data"][$x]["department_name"]; ?>&quot;, &quot;<?php echo $decoded["data"][$x]["section_name"]; ?>&quot;, &quot;<?php echo $decoded["data"][$x]["position_name"]; ?>&quot;);" />
                </td>     
              </tr>
              <?php }} ?>
            </tbody>
            <tfoot>
              <tr>   
                <th>NIK</th>
                <th>Firstname</th>
                <th>Lastname</th> 
                <th>Division</th> 
                <th>Department</th> 
                <th>Section</th> 
                <th>Position</th>  
                <th>Action</th> 
              </tr>
            </tfoot>
          </table> 
        </div> 
      </div>
    </div>
  </div>

  
<div class="modal fade bd-example-modal-xl" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Parent Licenses</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body table-responsive-xl"> 
        <?php $CompanyID = $data["CompanyID"];
        if(!empty($CompanyID)){
          $decoded =  $this->model('companylicenseModel')->getDataCompanyLicenseByCompanyId($token, $CompanyID);
          $decodedd = $this->model('companyModel')->getDataCompany($token, $CompanyID); ?>
        <table id="example2" class="table table-bordered table-striped" width="100%">
          <thead>
            <tr>  
              <th>Action</th> 
              <th>Company</th>
              <th>Business Unit</th>
              <th>License Type</th> 
              <th>License No</th> 
              <th>Issued Date</th> 
              <th>Expired Date</th>  
            </tr> 
          </thead>
          <tbody>
            <?php if(empty($decoded["data"])) { ?>
              <tr>
                <td>-</td>
                <td>-</td>
                <td>-</td> 
                <td>-</td>
                <td>-</td>  
                <td>-</td>  
                <td>-</td> 
              </tr> 
            <?php }else{ for ($x = 0; $x < count($decoded["data"]); $x++) { ?>   
              <tr>     
                <td>   
                  <input type="button" value="Select" class="btn btn-primary" onclick="SetName1(&quot;<?php echo $decoded["data"][$x]["company_license_id"]; ?>&quot;,&quot;<?php echo $decoded["data"][$x]["license_no"]; ?>&quot;);" />
                </td> 
                <td><?php echo $decodedd["data"]["company_name"]; ?></td> 
                <td><?php echo $decodedd["data"]["business_unit_name"]; ?></td> 
                <td><?php echo $decoded["data"][$x]["license_type_name"]; ?></td>  
                <td><?php echo $decoded["data"][$x]["license_no"]; ?></td>  
                <td><?php echo date("d-m-Y", $decoded["data"][$x]["issued_date"]); ?></td> 
                <td><?php echo date("d-m-Y", $decoded["data"][$x]["expired_date"]); ?></td>        
              </tr>
              <?php }} ?>
            </tbody>
            <tfoot>
              <tr>   
                <th>Action</th> 
                <th>Company</th>
                <th>Business Unit</th>
                <th>License Type</th> 
                <th>License No</th> 
                <th>Issued Date</th> 
                <th>Expired Date</th>  
              </tr>
            </tfoot>
          </table> 
       <?php } ?>
        </div> 
      </div>
    </div>
  </div>

  <div class="modal fade bd-example-modal-xl" id="myModal3" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-xl modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Close Permit</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body table-responsive-xl">   
        <form action="<?= base_url; ?>/CompanyLicense/updateCompanyLicense4/<?php echo $id; ?>" method="POST" name="form1"> 
        <input type="hidden" id="first" name="companylicenseID" readonly />
        <div class="form-group">
          <label for="exampleInputEmail1">Remark</label>
          <textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Remark"></textarea>  
        </div> 
        
        <button type="submit" name="submit" class="btn btn-danger">Confirm</button>
        </div> 
      </form>
      </div>
    </div>
  </div>

  <script type="text/javascript">
    function SetName(EmpID, NIK, Firstname, Lastname, Division, Department, Section, Position) { 
      var EmpNIK = NIK;
      $('#myModal1').modal('hide'); 
      $("#EmpID").val(EmpID);
      $("#mydata").val(EmpNIK);
      $("#firstname").val(Firstname);
      $("#Lastname").val(Lastname);
      $("#Division").val(Division);
      $("#Department").val(Department);
      $("#Section").val(Section);
      $("#Position").val(Position);
    }
  </script>

<script type="text/javascript">
    function SetName1(ParentID, LicenseNo) {  
      $('#myModal2').modal('hide'); 
      $("#mydata1").val(ParentID);  
      $("#LicenseNotemp").val(LicenseNo); 
    }
  </script>