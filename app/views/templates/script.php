<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="<?= base_url; ?>/plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="<?= base_url; ?>/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<script src="<?= base_url; ?>/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="<?= base_url; ?>/plugins/daterangepicker/daterangepicker.js"></script>
<script src="<?= base_url; ?>/plugins/select2/js/select2.full.min.js"></script>
<script src="<?= base_url; ?>/plugins/bootstrap4-duallistbox/jquery.bootstrap-duallistbox.min.js"></script>
<script src="<?= base_url; ?>/plugins/moment/moment.min.js"></script>
<script src="<?= base_url; ?>/plugins/inputmask/jquery.inputmask.min.js"></script>
<script src="<?= base_url; ?>/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<script src="<?= base_url; ?>/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<script src="<?= base_url; ?>/plugins/bootstrap-switch/js/bootstrap-switch.min.js"></script>
<script src="<?= base_url; ?>/plugins/bs-stepper/js/bs-stepper.min.js"></script>
<script src="<?= base_url; ?>/plugins/dropzone/min/dropzone.min.js"></script>

<!-- overlayScrollbars -->
<script src="<?= base_url; ?>/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?= base_url; ?>/dist/js/adminlte.js"></script>

<!-- DataTables  & Plugins -->
<script src="<?= base_url; ?>/plugins/datatables/jquery.dataTables.min.js"></script>
<script src="<?= base_url; ?>/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url; ?>/plugins/datatables-responsive/js/dataTables.responsive.min.js"></script>
<script src="<?= base_url; ?>/plugins/datatables-responsive/js/responsive.bootstrap4.min.js"></script>
<script src="<?= base_url; ?>/plugins/datatables-buttons/js/dataTables.buttons.min.js"></script>
<script src="<?= base_url; ?>/plugins/datatables-buttons/js/buttons.bootstrap4.min.js"></script>
<script src="<?= base_url; ?>/plugins/jszip/jszip.min.js"></script>
<script src="<?= base_url; ?>/plugins/pdfmake/pdfmake.min.js"></script>
<script src="<?= base_url; ?>/plugins/pdfmake/vfs_fonts.js"></script>
<script src="<?= base_url; ?>/plugins/datatables-buttons/js/buttons.html5.min.js"></script>
<script src="<?= base_url; ?>/plugins/datatables-buttons/js/buttons.print.min.js"></script>
<script src="<?= base_url; ?>/plugins/datatables-buttons/js/buttons.colVis.min.js"></script>
<script src="<?= base_url; ?>/plugins/datatables-colreorder/js/dataTables.colReorder.min.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="<?= base_url; ?>/plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="<?= base_url; ?>/plugins/raphael/raphael.min.js"></script>
<script src="<?= base_url; ?>/plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="<?= base_url; ?>/plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="<?= base_url; ?>/plugins/chart.js/Chart.min.js"></script> 

<!-- AdminLTE for demo purposes -->
<script src="<?= base_url; ?>/dist/js/demo.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?= base_url; ?>/dist/js/pages/dashboard2.js"></script>
<script src="<?= base_url; ?>/dist/js/footer.js"></script>
<!-- Page specific script -->

<script type="text/javascript">
  $('#myModal').on('show.bs.modal', function(e) {
    $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
  });
</script>

<script>
  $(function () {
    bsCustomFileInput.init();
  });
</script>

<script>
  $('#myModal1').on('shown.bs.modal', function () {
   var table = $('#example1').DataTable();
   table.columns.adjust();
 });
</script>

<script>
  $('#myModal2').on('shown.bs.modal', function () { 
   var table = $('#example2').DataTable();
   table.columns.adjust();
 });
</script> 

<script>
  $("a[data-toggle=\"tab\"]").on("shown.bs.tab", function (e) {
    $($.fn.dataTable.tables(true)).DataTable().columns.adjust();
  });
</script> 

<script>
$(function () {
  $('#myModal3').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget); // Button that triggered the modal
    var companylicenseId = button.data('companylicenseid'); // Extract info from data-* attributes 
    var modal = $(this);
    modal.find('#first').val(companylicenseId); 
  });
});
</script>

<script>   
  $(document).ready(function() { 

      var table = $("#divisionTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/Division/ajaxGetDataDivision",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "division_name" },
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#divisionTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#departmentTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/Department/ajaxGetDataDepartment",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "division_name" },
            { "data": "department_name" },
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#departmentTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#sectionTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/Section/ajaxGetDataSection",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "division_name" },
            { "data": "department_name" },
            { "data": "section_name" },
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#sectionTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#positionTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/Position/ajaxGetDataPosition",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "position_name" },
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#positionTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#locationTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/Location/ajaxGetDataLocation",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "location_name" },
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#locationTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#employeeTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/Employee/ajaxGetDataEmployee",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "nik" },
            { "data": "firstname" },
            { "data": "lastname" },
            { "data": "division_name" },
            { "data": "department_name" },
            { "data": "section_name" },
            { "data": "position_name" }, 
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#employeeTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#userTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/User/ajaxGetDataUser",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "username" },
            { "data": "nik" },
            { "data": "firstname" },
            { "data": "lastname" },
            { "data": "email" }, 
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#userTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#formTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/Form/ajaxGetDataForm",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "form_code" },
            { "data": "form_description" },
            { "data": "form_type_code" },
            { "data": "form_parent_id" },
            { "data": "sequence_no" }, 
            { "data": "class_tag" }, 
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#formTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#formTypeTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/FormType/ajaxGetDataFormType",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "form_type_code" },
            { "data": "form_type_description" }, 
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#formTypeTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#roleTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/Role/ajaxGetDataRole",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "role_code" },
            { "data": "role_description" }, 
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#roleTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>   
  $(document).ready(function() { 

      var table = $("#userRoleTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/UserRole/ajaxGetDataUserRole",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "firstname" }, 
            { "data": "lastname" }, 
            { "data": "role_code" },
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#userRoleTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 


<script>   
  $(document).ready(function() { 

      var table = $("#roleFormTable").DataTable({ 
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
          "processing": true,
          "serverSide": true,
          "dom": "Blfrtip",
          "buttons": [ 
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],  
          "ajax":{
            "url":  "<?= base_url; ?>/RoleForm/ajaxGetDataRoleForm",
            "dataType": "json",
            "type": "POST"
          },
          "columns": [
            { "data": "role_code" }, 
            { "data": "form_code" }, 
            { "data": "create_flag" },
            { "data": "read_flag" },
            { "data": "update_flag" },
            { "data": "delete_flag" }, 
            { "data": "remark",
              "render": function(data, type, row){
            	  return data.split("\n").join("<br/>");
              }
            },
            { "data": "aksi" }
          ], 
         
          }).buttons().container().appendTo('#roleFormTable_wrapper .col-md-6:eq(0)');
    } ); 
</script> 

<script>  
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#example1 tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "buttons": [ 
      {
        extend: 'copy',
        exportOptions: {
          columns: ':visible' 
        }
      }, 
      {
        extend: 'csv',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        exportOptions: {
          columns: ':visible'
        }
      }, "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
  } ); 
</script>   

<script>  
  $(document).ready(function() {
        // Setup - add a text input to each footer cell 
        $("#example2 tfoot th").each( function () {
          var title = $(this).text();
          $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        });

        // DataTable
        var table = $("#example2").DataTable({
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true, "stateSave":  true,
          "buttons": [
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],
          initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                  var that = this;

                  $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                      that
                      .search( this.value )
                      .draw();
                    }
                  } );
                } );
              }
            }).buttons().container().appendTo('#example2_wrapper .col-md-6:eq(0)');
      } ); 
    </script>  


    <script>  
      $(document).ready(function() {
        // Setup - add a text input to each footer cell 
        $("#example3 tfoot th").each( function () {
          var title = $(this).text();
          $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        });

        // DataTable
        var table = $("#example3").DataTable({
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true, "stateSave":  true,
          "buttons": [
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],
          initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                  var that = this;

                  $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                      that
                      .search( this.value )
                      .draw();
                    }
                  } );
                } );
              }
            }).buttons().container().appendTo('#example3_wrapper .col-md-6:eq(0)');
      } ); 
    </script>  

    <script>
      $(document).ready(function() {
        // Setup - add a text input to each footer cell 
        $("#example tfoot th").each( function () {
          var title = $(this).text();
          $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
        });

        // Wrap the colspan'ing header cells with a span so they can be positioned
        // absolutely - filling the available space, and no more.
        $('#example thead th[colspan]').wrapInner( '<span/>' ).append( '&nbsp;' );     

        // DataTable
        var table = $("#example").DataTable({
          "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true, "stateSave":  true,
          "buttons": [
          {
            extend: 'copy',
            exportOptions: {
              columns: ':visible' 
            }
          }, 
          {
            extend: 'csv',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'excel',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'pdf',
            exportOptions: {
              columns: ':visible'
            }
          },
          {
            extend: 'print',
            exportOptions: {
              columns: ':visible'
            }
          }, "colvis"],
          initComplete: function () {
                // Apply the search
                this.api().columns().every( function () {
                  var that = this;

                  $( 'input', this.footer() ).on( 'keyup change clear', function () {
                    if ( that.search() !== this.value ) {
                      that
                      .search( this.value )
                      .draw();
                    }
                  } );
                } );
              }
            }).buttons().container().appendTo('#example_wrapper .col-md-6:eq(0)');
      } ); 
    </script>
    

<script> 
  // ===================================================================================================== HOME COMPANY APPROVE =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#AppCompany tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#AppCompany").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [ 
      { "visible": false, "targets": 3 },
      { "visible": false, "targets": 4 },
      { "visible": false, "targets": 5 },
      { "visible": false, "targets": 6 },
      { "visible": false, "targets": 9 },
      { "visible": false, "targets": 11 } 
      ],
      "buttons": [ 
      {
        extend: 'copy',
        exportOptions: {
          columns: ':visible' 
        }
      }, 
      {
        extend: 'csv',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        exportOptions: {
          columns: ':visible'
        }
      }, "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#AppCompany_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== HOME COMPANY APPROVE =====================================================================================================
</script> 

<script> 
  // ===================================================================================================== HOME COMPANY LICENSE APPROVE =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#Approvelicense tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#Approvelicense").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [ 
      { "visible": false, "targets": 9 },
      { "visible": false, "targets": 11 },
      { "visible": false, "targets": 13 } 
      ],
      "buttons": [ 
      {
        extend: 'copy',
        exportOptions: {
          columns: ':visible' 
        }
      }, 
      {
        extend: 'csv',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        exportOptions: {
          columns: ':visible'
        }
      }, "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#Approvelicense_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== HOME COMPANY LICENSE APPROVE =====================================================================================================
</script> 

<script> 
  // ===================================================================================================== HOME COMPANY LICENSE CONCERN BOD =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#ConcernlicenseBOD tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#ConcernlicenseBOD").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [ 
      { "visible": false, "targets": 9 },
      { "visible": false, "targets": 11 },
      { "visible": false, "targets": 12 },
      { "visible": false, "targets": 13 },
      { "visible": false, "targets": 14 } 
      ],
      "buttons": [  "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#ConcernlicenseBOD_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== HOME COMPANY LICENSE CONCERN BOD =====================================================================================================
</script> 

<script> 
  // ===================================================================================================== HOME COMPANY LICENSE CONCERN =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#Concernlicense tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#Concernlicense").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [ 
      { "visible": false, "targets": 9 },
      { "visible": false, "targets": 11 },
      { "visible": false, "targets": 12 },
      { "visible": false, "targets": 13 },
      { "visible": false, "targets": 14 } 
      ],
      "buttons": [ 
      {
        extend: 'copy',
        exportOptions: {
          columns: ':visible' 
        }
      }, 
      {
        extend: 'csv',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        exportOptions: {
          columns: ':visible'
        }
      }, "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#Concernlicense_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== HOME COMPANY LICENSE CONCERN =====================================================================================================
</script> 

 <script> 
  // ===================================================================================================== COMPANY =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#company tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#company").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [
      { "visible": false, "targets": 3 },
      { "visible": false, "targets": 4 },
      { "visible": false, "targets": 5 },
      { "visible": false, "targets": 6 },
      { "visible": false, "targets": 9 },
      { "visible": false, "targets": 11 } 
      ],
      "buttons": [ 
      {
        extend: 'copy',
        exportOptions: {
          columns: ':visible' 
        }
      }, 
      {
        extend: 'csv',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        exportOptions: {
          columns: ':visible'
        }
      }, "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#company_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== COMPANY =====================================================================================================
</script>   

<script> 
  // ===================================================================================================== LICENSE BOD =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#licenseBOD tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#licenseBOD").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [
      { "visible": false, "targets": 8 },
      { "visible": false, "targets": 9 },
      { "visible": false, "targets": 14 },
      { "visible": false, "targets": 15 },
      { "visible": false, "targets": 16 },
      { "visible": false, "targets": 18 },
      { "visible": false, "targets": 20 }
      ],
      "buttons": [ "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#licenseBOD_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== LICENSE BOD =====================================================================================================
</script>  

<script> 
  // ===================================================================================================== LICENSE =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#license tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#license").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [
      { "visible": false, "targets": 8 },
      { "visible": false, "targets": 9 },
      { "visible": false, "targets": 14 },
      { "visible": false, "targets": 15 },
      { "visible": false, "targets": 16 },
      { "visible": false, "targets": 18 },
      { "visible": false, "targets": 20 }
      ],
      "buttons": [ 
      {
        extend: 'copy',
        exportOptions: {
          columns: ':visible' 
        }
      }, 
      {
        extend: 'csv',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        exportOptions: {
          columns: ':visible'
        }
      }, "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#license_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== LICENSE =====================================================================================================
</script>  

<script> 
  // ===================================================================================================== SHAREHOLDER BOD =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#shareholderBOD tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#shareholderBOD").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [
      { "visible": false, "targets": 4 },
      { "visible": false, "targets": 6 },
      { "visible": false, "targets": 8 }
      ],
      "buttons": [ "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#shareholderBOD_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== SHAREHOLDER BOD =====================================================================================================
</script> 

<script> 
  // ===================================================================================================== SHAREHOLDER =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#shareholder tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#shareholder").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [
      { "visible": false, "targets": 4 },
      { "visible": false, "targets": 6 },
      { "visible": false, "targets": 8 }
      ],
      "buttons": [ 
      {
        extend: 'copy',
        exportOptions: {
          columns: ':visible' 
        }
      }, 
      {
        extend: 'csv',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        exportOptions: {
          columns: ':visible'
        }
      }, "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#shareholder_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== SHAREHOLDER =====================================================================================================
</script> 

<script> 
  // ===================================================================================================== MANAGEMENT BOD =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#managementBOD tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#managementBOD").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [
      { "visible": false, "targets": 4 },
      { "visible": false, "targets": 6 } 
      ],
      "buttons": [ "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#managementBOD_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== MANAGEMENT BOD =====================================================================================================
</script> 

<script> 
  // ===================================================================================================== MANAGEMENT =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#management tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#management").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [
      { "visible": false, "targets": 4 },
      { "visible": false, "targets": 6 } 
      ],
      "buttons": [ 
      {
        extend: 'copy',
        exportOptions: {
          columns: ':visible' 
        }
      }, 
      {
        extend: 'csv',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        exportOptions: {
          columns: ':visible'
        }
      }, "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#management_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== MANAGEMENT =====================================================================================================
</script> 

<script> 
  // ===================================================================================================== COMPANY LICENSE =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#Comlicense tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#Comlicense").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [
      { "visible": false, "targets": 3 },
      { "visible": false, "targets": 4 },
      { "visible": false, "targets": 9 },
      { "visible": false, "targets": 10 },
      { "visible": false, "targets": 11 },
      { "visible": false, "targets": 12 },
      { "visible": false, "targets": 13 },
      { "visible": false, "targets": 15 },
      { "visible": false, "targets": 17 } 
      ],
      "buttons": [ 
      {
        extend: 'copy',
        exportOptions: {
          columns: ':visible' 
        }
      }, 
      {
        extend: 'csv',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'excel',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'pdf',
        exportOptions: {
          columns: ':visible'
        }
      },
      {
        extend: 'print',
        exportOptions: {
          columns: ':visible'
        }
      }, "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#Comlicense_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== COMPANY LICENSE =====================================================================================================
</script> 
<script> 
  // ===================================================================================================== COMPANY LICENSE BOD =====================================================================================================
  $(document).ready(function() {
    // Setup - add a text input to each footer cell 
    $("#ComlicenseBOD tfoot th").each( function () {
      var title = $(this).text();
      $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    });

    // DataTable
    var table = $("#ComlicenseBOD").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false, "colReorder": true,
      "columnDefs": [
      { "visible": false, "targets": 6 },
      { "visible": false, "targets": 7 },  
      { "visible": false, "targets": 12 },
      { "visible": false, "targets": 13 },
      { "visible": false, "targets": 15 },
      { "visible": false, "targets": 17 } 
      ],
      "buttons": [ "colvis"],  
      initComplete: function () {
            // Apply the search
            this.api().columns().every( function () {
              var that = this;

              $( 'input', this.footer() ).on( 'keyup change clear', function () {
                if ( that.search() !== this.value ) {
                  that
                  .search( this.value )
                  .draw();
                }
              } );
            } );
          }
        }).buttons().container().appendTo('#ComlicenseBOD_wrapper .col-md-6:eq(0)');
  } ); 
  // ===================================================================================================== COMPANY LICENSE BOD=====================================================================================================
</script> 

    <script>
      $(function(){
        var url = window.location; 
  // if(window.location.hash === "#TradingRiskManagement"){
  //  var match = window.location.href.match(/^[^#]+#([^?]*)\??(.*)/);
  //  var url = match[0].split('#')[0]; 
  // } 
  // for sidebar menu entirely but not cover treeview
  $('ul.nav-sidebar a').filter(function() {
    return this.href != url;
  }).removeClass('active');

  // for sidebar menu entirely but not cover treeview
  $('ul.nav-sidebar a').filter(function() {
    return this.href == url;
  }).addClass('active');

  // for treeview
  $('ul.nav-treeview a').filter(function() {
    return this.href == url;
  }).parentsUntil(".nav-sidebar > .nav-treeview").addClass('menu-open').prev('a').addClass('active');
})
</script> 

<script>
  $("#SV").click(function(){ 
    $("select > option").prop("selected","selected"); 
  });
</script> 

<script>
  var getUrlParameter = function getUrlParameter(sParam) {
    var sPageURL = window.location.search.substring(1),
    sURLVariables = sPageURL.split('&'),
    sParameterName,
    i;

    for (i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');

      if (sParameterName[0] === sParam) {
        return typeof sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
      }
    }
    return false;
  };
</script> 

<script>
  function isInt(value) {
    return !isNaN(value) && 
    parseInt(Number(value)) == value && 
    !isNaN(parseInt(value, 10));
  }
</script> 
</script>

<script type="text/javascript">
  $(document).ready(function(){ 
    $("#FormTypeID").on("change",function(){
      var FormType = $(this).val(); 
      $.ajax({
        url :"<?= base_url; ?>/Form/ajaxGetParent",
        type:"POST",
        cache:false,
        data: 'FormType=' + FormType,
        success:function(response){  
          $('#FormParentID').html(response); 
        }
      });
    }); 
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){  
    var url = window.location.href; 
    var id = url.substring(url.lastIndexOf('/')+1); 
    if(isInt(id)==false){
    }
    else{  
      $.ajax({
        url :"<?= base_url; ?>/Form/ajaxGetParentEdt",
        type:"POST",
        cache:false,
        data: 'id=' + id,
        success:function(response){    
          $('#FormParentID').html(response);  
        }
      });
    }
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){ 
    $("#divisionCB").on("change",function(){
      var DivisionID = $(this).val();  
      $.ajax({
        url :"<?= base_url; ?>/Section/ajaxGetDepartment",
        type:"POST",
        cache:false,
        data: 'DivisionID=' + DivisionID,
        success:function(response){  
          $('#DepartmentCB').html(response);  
        }
      });
    }); 
  }); 
</script>

<script type="text/javascript">
  $(document).ready(function(){  
    var url = window.location.href; 
    var id = url.substring(url.lastIndexOf('/')+1); 
    if(isInt(id)==false){
    }
    else{  
      $.ajax({
        url :"<?= base_url; ?>/Section/ajaxGetDepartmentEdt",
        type:"POST",
        cache:false,
        data: "id=" + id, 
        success:function(response){    
          $('#DepartmentCBEdt').html(response);    
        }
      });
    }
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){ 
    $("#divisionCBEdt").on("change",function(){
      var DivisionID = $(this).val(); 
      $.ajax({
        url :"<?= base_url; ?>/Section/ajaxGetDepartment",
        type:"POST",
        cache:false,
        data: 'DivisionID=' + DivisionID,
        success:function(response){  
          $('#DepartmentCBEdt').html(response); 
        }
      });
    }); 
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){  
    var url = window.location.href; 
    var id = url.substring(url.lastIndexOf('/')+1);   
    if(isInt(id)==false){
    }
    else{  
      $.ajax({
        url :"<?= base_url; ?>/Employee/ajaxGetDepartmentEdtEmployee",
        type:"POST",
        cache:false,
        data: 'id=' + id,
        success:function(response){    
          $('#DepartmentCBEdt3').html(response); 
        }
      });
    }
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){ 
    $("#divisionCBEdt3").on("change",function(){
      var DivisionID = $(this).val(); 
      $.ajax({
        url :"<?= base_url; ?>/Section/ajaxGetDepartment",
        type:"POST",
        cache:false,
        data: 'DivisionID=' + DivisionID,
        success:function(response){  
          $('#DepartmentCBEdt3').html(response); 
        }
      });
    }); 
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){ 
    $("#DepartmentCB").on("change",function(){
      var DepartmentID = $(this).val(); 
      $.ajax({
        url :"<?= base_url; ?>/Employee/ajaxGetSection",
        type:"POST",
        cache:false,
        data: 'DepartmentID=' + DepartmentID,
        success:function(response){  
          $('#SectionCB').html(response); 
        }
      });
    }); 
  });
</script>

<script type="text/javascript">
  $(document).ready(function(){  
    var url = window.location.href; 
    var id = url.substring(url.lastIndexOf('/')+1); 
    if(isInt(id)==false){
    }
    else{  
      $.ajax({
        url :"<?= base_url; ?>/Employee/ajaxGetSectionEdt",
        type:"POST",
        cache:false,
        data: 'id=' + id,
        success:function(response){    
          $('#SectionCBEdt').html(response); 
        }
      });
    }
  });
</script>


<script type="text/javascript">
  $(document).ready(function(){ 
    $("#DepartmentCBEdt3").on("change",function(){
      var DepartmentID = $(this).val(); 
      $.ajax({
        url :"<?= base_url; ?>/Employee/ajaxGetSection",
        type:"POST",
        cache:false,
        data: 'DepartmentID=' + DepartmentID,
        success:function(response){  
          $('#SectionCBEdt').html(response);   
        }
      });
    }); 
  });
</script> 

<script>
  function setCookie(cname,cvalue,exdays) {
    const d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    let expires = "expires=" + d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
  }

  const  d = new Date();
  let  diff = d.getTimezoneOffset(); 
  diff = diff == 0 ? 0 : -diff;
  let expires = "expires="+ d.toUTCString(); 
  setCookie("offset", diff, expires); 
  console.log(diff);
</script> 

<script>
  function required()
  {  
      //================================= DIVISION ================================= 
      var divName = document.forms["form1"]["DivisionName"].value;
      if (divName == "")
      {
        alert("Please Input Division Name");
        return false;
      }else{ 
        return true;  
      }
    }  
      //================================= DIVISION ================================= 
    </script>

    <script>
      function requiredDept()
      {
      //================================= DEPARTMENT ================================= 
      var divID = document.forms["form1"]["DivisionID"].value;
      if (divID == "")
      {
        alert("Please Input Division Name");
        return false;
      }else{
        var DeptName = document.forms["form1"]["DepartmentName"].value;
        if (DeptName == "")
        {
          alert("Please Input Department Name");
          return false;
        }else{
          return true; 
        } 
      } 
    }
      //================================= DEPARTMENT ================================= 
    </script>

    <script>
      function requiredSec()
      { 
      //================================= SECTION ================================= 
      var divIDSection = document.forms["form1"]["DivisionID"].value;
      if (divIDSection == "")
      {
        alert("Please Input Division Name");
        return false;
      }else{
        var DeptID = document.forms["form1"]["DepartmentId"].value;
        if (DeptID == "")
        {
          alert("Please Input Department Name");
          return false;
        }else{
          var SecName = document.forms["form1"]["SectionName"].value;
          if (SecName == "")
          {
            alert("Please Input Section Name");
            return false;
          }else{
            return true; 
          } 
        } 
      }  
    }
    //================================= SECTION ================================= 
  </script>

  <script>
    function requiredPos()
    { 
      //================================= POSITION ================================= 
      var PosName = document.forms["form1"]["PositionName"].value;
      if (PosName == "")
      {
        alert("Please Input Position Name");
        return false;
      }else{ 
        return true;  
      }
    }  
    //================================= POSITION ================================= 
  </script>

  <script>
    function requiredLoc()
    { 
      //================================= LOCATION ================================= 
      var LocName = document.forms["form1"]["LocationName"].value;
      if (LocName == "")
      {
        alert("Please Input Location Name");
        return false;
      }else{ 
        return true;  
      }
    }  
    //================================= LOCATION ================================= 
  </script>

  <script>
    function requiredEmp()
    { 
      //================================= EMPLOYEE ================================= 
      var NIK = document.forms["form1"]["NIK"].value;
      if (NIK == "")
      {
        alert("Please Input NIK");
        return false;
      }else{
        var FName = document.forms["form1"]["Firstname"].value;
        if (FName == "")
        {
          alert("Please Input Firstname");
          return false;
        }else{
          var Email = document.forms["form1"]["Email"].value;
          if (Email == "")
          {
            alert("Please Input Email");
            return false;
          }else{
            return true; 
          } 
        } 
      }  
    }
    //================================= EMPLOYEE ================================= 
  </script>

  <script>
    function requiredUser()
    { 
      //================================= USER ================================= 
      var UName = document.forms["form1"]["Username"].value;
      if (UName == "")
      {
        alert("Please Input Username");
        return false;
      }else{ 
        var Email = document.forms["form1"]["Email"].value;
        if (Email == "")
        {
          alert("Please Input Email");
          return false;
        }else{
          return true; 
        }  
      }  
    }
    //================================= USER ================================= 
  </script>

  <script>
    function requiredChangePass()
    { 
      //================================= CHANGE PASSWORD ================================= 
      var NPass = document.forms["form1"]["Password"].value;
      if (NPass == "")
      {
        alert("Please Input New Password");
        return false;
      }else{
        var ConPass = document.forms["form1"]["ConPassword"].value;
        if (ConPass == "")
        {
          alert("Please Input Confirm Password");
          return false;
        }else{
          return true; 
        } 
      }  
    }
    //================================= CHANGE PASSWORD ================================= 
  </script>

  <script>
    function requiredForm()
    { 
      //================================= FORM ================================= 
      var FormCode = document.forms["form1"]["FormCode"].value;
      if (FormCode == "")
      {
        alert("Please Input Form Code");
        return false;
      }else{
        var FDes = document.forms["form1"]["FormDescription"].value;
        if (FDes == "")
        {
          alert("Please Input Form Description");
          return false;
        }else{
          var FormType = document.forms["form1"]["FormTypeID"].value;
          if (FormType == "")
          {
            alert("Please Input Form Type");
            return false;
          }else{
            var SeqNo = document.forms["form1"]["SequenceNo"].value;
            if (SeqNo == "")
            {
              alert("Please Input Sequence Number");
              return false;
            }else{
              return true; 
            }
          } 
        } 
      }  
    }
    //================================= FORM ================================= 
  </script>

  <script>
    function requiredFormType()
    { 
      //================================= FORM TYPE ================================= 
      var FormTypeCode = document.forms["form1"]["FormTypeCode"].value;
      if (FormTypeCode == "")
      {
        alert("Please Input Form Type Code");
        return false;
      }else{
        var FormTypeDes = document.forms["form1"]["FormTypeDescription"].value;
        if (FormTypeDes == "")
        {
          alert("Please Input Form Type Description");
          return false;
        }else{ 
          return true;  
        }  
      }
    }
    //================================= FORM TYPE ================================= 
  </script>

  <script>
    function requiredRole()
    { 
      //================================= ROLE ================================= 
      var RoleCode = document.forms["form1"]["RoleCode"].value;
      if (RoleCode == "")
      {
        alert("Please Input Role Code");
        return false;
      }else{
        var RoleDes = document.forms["form1"]["RoleDescription"].value;
        if (RoleDes == "")
        {
          alert("Please Input Role Description");
          return false;
        }else{ 
          return true;  
        }  
      }
    }
    //================================= ROLE ================================= 
  </script>

  <script>
    function requiredUserRole()
    { 
      //================================= USER ROLE ================================= 
      var UsrID = document.forms["form1"]["UsrID"].value;
      if (UsrID == "")
      {
        alert("Please Input Username");
        return false;
      }else{
        var RoleID = document.forms["form1"]["RoleID"].value;
        if (RoleID == "")
        {
          alert("Please Input Role");
          return false;
        }else{ 
          return true;  
        }  
      }
    }
    //================================= USER ROLE ================================= 
  </script>

  <script>
    function requiredRoleForm()
    { 
      //================================= ROLE FORM ================================= 
      var RoleID = document.forms["form1"]["RoleID"].value;
      if (RoleID == "")
      {
        alert("Please Input Role");
        return false;
      }else{
        var FormId = document.forms["form1"]["FormId"].value;
        if (FormId == "")
        {
          alert("Please Input Form");
          return false;
        }else{ 
          return true;  
        }  
      }
    }
    //================================= ROLE FORM ================================= 
  </script>

  <script>
    function requiredBU()
    {  
      //================================= BUSINESS UNIT ================================= 
      var BUName = document.forms["form1"]["BusinessUnitName"].value;
      if (BUName == "")
      {
        alert("Please Input Business Unit Name");
        return false;
      }else{ 
        return true;  
      }
    }  
      //================================= BUSINESS UNIT ================================= 
    </script> 

    <script>
      function requiredCMT()
      {  
        //================================= COMPANY MANAGEMENT TYPE ================================= 
        var divName = document.forms["form1"]["CompanyManagementTypeName"].value;
        if (divName == "")
        {
          alert("Please Input Company Management Type Name");
          return false;
        }else{ 
          return true;  
        }
      }  
      //================================= COMPANY MANAGEMENT TYPE ================================= 
    </script>

    <script>
      function requiredLT()
      {  
      //================================= LICENSE TYPE ================================= 
      var LTName = document.forms["form1"]["LicenseTypeName"].value;
      if (LTName == "")
      {
        alert("Please Input License Type Name");
        return false;
      }else{ 
        return true;  
      }
    }  
      //================================= LICENSE TYPE ================================= 
    </script>

    <script>
      function requiredEQT()
      {  
      //================================= EMAIL QUEUE TYPE ================================= 
      var EQTName = document.forms["form1"]["EmailQueueTypeName"].value;
      if (EQTName == "")
      {
        alert("Please Input Email Queue Type Name");
        return false;
      }else{ 
        var TReference = document.forms["form1"]["TableReference"].value;
        if (TReference == "")
        {
          alert("Please Input Table Reference");
          return false;
        }else{ 
          var FReference = document.forms["form1"]["FieldReference"].value;
          if (FReference == "")
          {
            alert("Please Input Field Reference");
            return false;
          }else{ 
            var RName = document.forms["form1"]["RecipientName"].value;
            if (RName == "")
            {
              alert("Please Input Recipient Name");
              return false;
            }else{ 
              var ERecipient = document.forms["form1"]["EmailRecipient"].value;
              if (ERecipient == "")
              {
                alert("Please Input Email Recipient");
                return false;
              }else{  
                var ESubject = document.forms["form1"]["EmailSubject"].value;
                if (ESubject == "")
                {
                  alert("Please Input Email Subject");
                  return false;
                }else{ 
                  return true;  
                }   
              } 
            } 
          }  
        }   
      }
    }  
      //================================= EMAIL QUEUE TYPE ================================= 
    </script>

    <script>
      function requiredCompany()
      {  
      //================================= COMPANY ================================= 
      var CName = document.forms["form1"]["CompanyName"].value;
      if (CName == "")
      {
        alert("Please Input Company Name");
        return false;
      }else{ 
        var BUID = document.forms["form1"]["BusinessUnitID"].value;
        if (BUID == "")
        {
          alert("Please Input Business Unit Name");
          return false;
        }else{  
          return true; 
        }
      }
    }  
      //================================= COMPANY ================================= 
    </script>

    <script>
      function requiredLC()
      {  
      //================================= LICENSE ================================= 
      var LID = document.forms["form1"]["LicenseTypeID"].value;
      if (LID == 0)
      {
        alert("Please Input License Type");
        return false;
      }else{ 
        var LNo = document.forms["form1"]["LicenseNo"].value;
        if (LNo == "")
        {
          alert("Please Input License Number");
          return false;
        }else{  
          var IssAt = document.forms["form1"]["IssueAt"].value;
          if (IssAt == "")
          {
            alert("Please Input Issue Date");
            return false;
          }else{  
            var Stat = document.forms["form1"]["Status"].value;
            if (Stat == "")
            {
              alert("Please Input Status");
              return false;
            }else{  
              return true; 
            }
          }
        }
      }
    }  
      //================================= LICENSE ================================= 
    </script>

    <script>
      function requiredLCR()
      {  
      //================================= LICENSE RENEWAL =================================  
      var Stat = document.forms["form1"]["Status"].value;
      if (Stat == "")
      {
        alert("Please Input Status");
        return false;
      }else{  
        return true; 
      }
    }  
      //================================= LICENSE RENEWAL ================================= 
    </script>

    <script>
      function division() {
        window.location = "<?= base_url; ?>/Division";
      }
    </script>  

    <script>  
      function department() {
        window.location = "<?= base_url; ?>/Department";
      }
    </script>  

    <script> 
      function section() {
        window.location = "<?= base_url; ?>/Section";
      }
    </script> 

    <script>  
      function position() {
        window.location = "<?= base_url; ?>/Position";
      }
    </script>  

    <script> 
      function loc() {
        window.location = "<?= base_url; ?>/Location";
      }
    </script>

    <script>   
      function employee() {
        window.location = "<?= base_url; ?>/Employee";
      }
    </script> 

    <script>  
      function user() {
        window.location = "<?= base_url; ?>/User";
      }
    </script> 

    <script>  
      function ResetPassword(id) { 
        window.location = "<?= base_url; ?>/User/resetPassword/"+id; 
      }
    </script>  

    <script> 
      function forms() {
        window.location = "<?= base_url; ?>/Form";
      } 
    </script> 

    <script>  
      function formType() {
        window.location = "<?= base_url; ?>/FormType";
      }
    </script>  

    <script> 
      function role() {
        window.location = "<?= base_url; ?>/Role";
      }
    </script>  

    <script> 
      function userRole() {
        window.location = "<?= base_url; ?>/UserRole";
      }
    </script>  

    <script> 
      function roleForm() {
        window.location = "<?= base_url; ?>/RoleForm";
      } 
    </script>  

    <script> 
      function businessUnit() {
        window.location = "<?= base_url; ?>/BusinessUnit";
      } 
    </script>
    
    <script> 
      function companymanagementtype() {
        window.location = "<?= base_url; ?>/CompanyManagementType";
      } 
    </script> 

    <script> 
      function licenseType() {
        window.location = "<?= base_url; ?>/LicenseType";
      } 
    </script>  

    <script> 
      function emailQueueType() {
        window.location = "<?= base_url; ?>/EmailQueueType";
      } 
    </script> 
    
    <script> 
      function approve(id) {
        window.location = "<?= base_url; ?>/Company/updateCompanyApprove/"+id;
      } 
    </script> 

    <script> 
      function deactivate(id) {
        window.location = "<?= base_url; ?>/Company/updateCompanyDeactive/"+id;
      } 
    </script> 

    <script> 
      function approveLicense(id, companyid) {
        window.location = "<?= base_url; ?>/CompanyLicense/updateCompanyLicenseApprove/"+id;
      } 
    </script> 

    <script> 
      function deactivateLicense(id, companyid) {
        window.location = "<?= base_url; ?>/CompanyLicense/updateCompanyLicenseDeactive/"+id;
      } 
    </script> 


    <script> 
      function company() {
        window.location = "<?= base_url; ?>/Company";
      } 
    </script> 

    <script> 
      function license() {
        window.location = "<?= base_url; ?>/CompanyLicense";
      } 
    </script> 

    <script>
      function processor() {
        window.location = "<?= base_url; ?>/Processor";
      }
    </script> 

    <script>
      function motherboard() {
        window.location = "<?= base_url; ?>/Motherboard";
      }
    </script> 

    <script>
      function ram() {
        window.location = "<?= base_url; ?>/RAM";
      }
    </script> 

    <script>
      function hdd() {
        window.location = "<?= base_url; ?>/HDD";
      }
    </script> 

    <script>
      function casing() {
        window.location = "<?= base_url; ?>/Casing";
      }
    </script> 

    <script>
      function keyboard() {
        window.location = "<?= base_url; ?>/Keyboard";
      }
    </script> 

    <script>
      function mouse() {
        window.location = "<?= base_url; ?>/Mouse";
      }
    </script> 

    <script>
      function monitor() {
        window.location = "<?= base_url; ?>/Monitor";
      }
    </script> 

    <script>
      function os() {
        window.location = "<?= base_url; ?>/OS";
      }
    </script> 

    <script>
      function office() {
        window.location = "<?= base_url; ?>/Office";
      }
    </script> 

    <script>
      function computer() {
        window.location = "<?= base_url; ?>/UserComputer";
      }
    </script> 

    <script>
      function laptop() {
        window.location = "<?= base_url; ?>/UserLaptop";
      }
    </script> 

    <script>
      function servicelaptop() {
        window.location = "<?= base_url; ?>/ServiceLaptop";
      }
    </script> 

    <script>
      function ink() {
        window.location = "<?= base_url; ?>/Ink";
      }
    </script> 

     <script>
      function userPrinter() {
        window.location = "<?= base_url; ?>/UserPrinter";
      }
    </script> 

    <script>
      function ups() {
        window.location = "<?= base_url; ?>/UserUPS";
      }
    </script> 

    <script>
      function antivirus() {
        window.location = "<?= base_url; ?>/UserAntivirus";
      }
    </script> 
    
    <script>
      function harddisk() {
        window.location = "<?= base_url; ?>/UserHarddisk";
      }
    </script> 

    <script>
      function subscription() {
        window.location = "<?= base_url; ?>/DataSubscriptions";
      }
    </script> 

    <script>
      $(document).ready(function(){ 
        initial.onchange = evt => {
          const [file] = initial.files
          if (file) {
            Initimg.src = URL.createObjectURL(file)
          }
        }
      });  
    </script>

    <script>
      $(document).ready(function(){ 
        signature.onchange = evt => {
          const [file] = signature.files
          if (file) {
            Signimg.src = URL.createObjectURL(file)
          }
        }
      });  
    </script>

    <script>
      $(function () {  
    //Date picker
    $('#reservationdate').datetimepicker({
      format: 'DD-MM-YYYY'
    });
  }) 
</script>

<script>
  $(function () {  
    //Date picker
    $('#reservationdate1').datetimepicker({
      format: 'DD-MM-YYYY'
    });
  }) 
</script>

<script>
  $(function () {  
    //Date picker
    $('#reservationdate2').datetimepicker({
      format: 'DD-MM-YYYY'
    });
  }) 
</script>

<script>
  $(function () {  
    //Date picker
    $('#reservationdate3').datetimepicker({
      format: 'DD-MM-YYYY'
    });
  }) 
</script>
