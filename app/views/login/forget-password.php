<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>

<head> 
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.1/css/all.css" integrity="sha384-gfdkjb5BdAXd+lj+gudLWI+BXq4IuLW5IT+brZEZsLFm++aCMlF1V92rMkPaX4PP" crossorigin="anonymous">
	<link rel="stylesheet" href="<?= base_url; ?>/dist/css/login.css">
</head>
<!--Coded with love by Mutiullah Samim-->
<body>
	<div class="container h-100">
		<div class="d-flex justify-content-center h-100">
			<div class="user_card">
				<div class="d-flex justify-content-center">
					<div class="brand_logo_container">
						<img src="<?= base_url; ?>/dist/logo.png" class="brand_logo" alt="Logo">
					</div>
				</div>
				<div class="d-flex justify-content-center form_container">
					<form name="form1" action="<?= base_url; ?>/Login/prosesForgetPassword" method="POST">
						<div class="input-group mb-3">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-user"></i></span>
							</div>
							<input type="text" name="Name" class="form-control input_user" placeholder="Username"> 
						</div>
						<div class="input-group mb-3">
							<div class="input-group-append">
								<span class="input-group-text"><i class="fas fa-solid fa-at"></i></span>
							</div> 
							<input type="text" name="Email" class="form-control input_user" placeholder="Email"> 
						</div> 
						<div class="d-flex justify-content-center mt-3 login_container">
							<button type="submit" name="submit" class="btn login_btn">Submit</button>
						</div>
					</form>
				</div>  
				<div class="row">
				<div class="col-sm-12">
				<?php
					Flasher::Message();
				?>
				</div>
			</div>
				<span class="forget" onclick="window.location.href='<?= base_url; ?>/Login';"><div class="forget-password"> 
					<b>Sign In</b>
				</div></span> 
			</div>
		</div>
	</div>
</body>
</html> 

<script>
	function requiredFPass()
	{
		var empt = document.forms["form1"]["Name"].value;
		if (empt == "")
		{
			alert("Please Fill The Name");
			return false;
		}else{
			var Email = document.forms["form1"]["Email"].value;
			if (Email == "")
			{
				alert("Please Input Email");
				return false;
			}else{
				return true; 
			} 
		}  
	}
</script>
