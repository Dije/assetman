<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
				if($data['Access']["data"][$i]["form_php"] == $data['PHP']){ ?> 
				<!-- /.content -->  
				<section class="content">
					<div class="row">
						<div class="col-sm-12">
							<?php
							Flasher::Message();
							?>
						</div>
					</div>
					<div class="container-fluid">
						<div class="row">
							<!-- left column -->
							<div class="col-md-12">
								<div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title"><?php echo $data["company"]["data"]["company_name"]; ?></h3>
                                    </div>
									<div class="card-header p-2"> 
										<ul class="nav nav-pills">
											<li class="nav-item"><a class="nav-link active" href="#activity" data-toggle="tab">Shareholder</a></li>
											<li class="nav-item"><a class="nav-link" href="#timeline" data-toggle="tab">Management</a></li>
											<li class="nav-item"><a class="nav-link" href="#settings" data-toggle="tab">License</a></li>
										</ul>
									</div>
									<div class="card-body">
										<div class="tab-content">
											<div class="tab-pane active" id="activity">
												<?php if(empty($data['Access'])){}
												else{ for($a=0; $a< count($data['Access']["data"]); $a++){ 
													if($data['Access']["data"][$a]["form_php"] == "CompanyShareholder"){ ?>
														<!-- Main content --> 
														<div class="container-fluid">
															<div class="row">
																<div class="col-12">  
																	<div class="card">
																		<!-- =============================== add ================================ --> 
																		<?php if($data['Access']["data"][$a]["create_flag"] == true ) { ?> 
																			<div class="card-header"> 
																					<a href="<?= base_url; ?>/CompanyShareholder/add/<?php echo $data["company"]["data"]["company_id"]; ?>" class="btn btn-primary">Add</a>  
																			</div>  
																		<?php } ?>
																		<!-- =============================== add ================================ --> 
																		<?php if($data['Access']["data"][$a]["read_flag"] == true ) { ?> 
																			<div class="card-body"> 
																				<table id="shareholderBOD" class="table table-bordered table-striped">
																					<thead>
																						<tr> 
																							<th>Shareholder Name</th>
																							<th>Number Of Share</th>
																							<th>Percentage Of Share</th>
																							<th>Share Amount</th>   
																							<th>Remark</th> 
																							<th>Created By</th> 
																							<th>Created Date</th> 
																							<th>Updated By</th> 
																							<th>Updated Date</th> 
																							<?php if($data['Access']["data"][$a]["update_flag"] == true OR $data['Access']["data"][$a]["delete_flag"] == true) { ?>  
																								<th>Action</th>  
																							<?php } ?> 
																						</tr>
																					</thead>
																					<tbody>
																						<?php if(empty($data["companyShareholder"]["data"])) { ?>
																							<tr>
																								<td>-</td>
																								<td>-</td>
																								<td>-</td>
																								<td>-</td>
																								<td>-</td>   
																								<td>-</td>
																								<td>-</td>   
																								<td>-</td>   
																								<td>-</td>
																								<?php if($data['Access']["data"][$a]["update_flag"] == true OR $data['Access']["data"][$a]["delete_flag"] == true) { ?>  
																									<td>-</td> 
																								<?php } ?> 
																							</tr> 
																						<?php }else{  for ($z = 0; $z < count($data["companyShareholder"]["data"]); $z++) { ?>   
																							<tr>
																								<td><?php echo $data["companyShareholder"]["data"][$z]["shareholder_name"]; ?></td> 
																								<td><?php echo number_format($data["companyShareholder"]["data"][$z]["number_of_share"], 0, ',', '.'); ?> Lembar</td>
																								<td><?php echo $data["companyShareholder"]["data"][$z]["percentage_of_share"]; ?> %</td> 
																								<td><?php echo number_format($data["companyShareholder"]["data"][$z]["share_amount"], 0, ',', '.'); ?></td> 
																								<td><?php echo $data["companyShareholder"]["data"][$z]["remark"]; ?></td>  
																								<?php if($data["companyShareholder"]["data"][$z]["created_user_id"] == 0) { ?>
																									<td> </td>
																								<?php }else{ $UserCreate = $this->model('userModel')->getDataUser($token, $data["companyShareholder"]["data"][$z]["created_user_id"])?>
																									<td><?php echo $UserCreate["data"]["username"];  ?></td>
																								<?php }?>
																								<?php if($data["companyShareholder"]["data"][$z]["created_at"] == 0) { ?>
																									<td> </td>
																								<?php }else{ ?>
																									<td><?php echo date("d-m-Y", $data["companyShareholder"]["data"][$z]["created_at"]); ?></td> 
																								<?php }?>
																								<?php if($data["companyShareholder"]["data"][$z]["updated_user_id"] == 0) { ?>
																									<td> </td>
																								<?php }else{ $UserUpdt = $this->model('userModel')->getDataUser($token, $data["companyShareholder"]["data"][$z]["updated_user_id"])?>
																									<td><?php echo $UserUpdt["data"]["username"];  ?></td>
																								<?php }?>
																								<?php if($data["companyShareholder"]["data"][$z]["updated_at"] == 0) { ?>
																									<td> </td>
																								<?php }else{ ?>
																									<td><?php echo date("d-m-Y", $data["companyShareholder"]["data"][$z]["updated_at"]); ?></td> 
																								<?php }?>
																								<?php if($data['Access']["data"][$a]["update_flag"] == true OR $data['Access']["data"][$a]["delete_flag"] == true) { ?>  
																									<td>  
																										<?php if($data['Access']["data"][$a]["update_flag"] == true) { ?>
																											<a href="<?= base_url; ?>/CompanyShareholder/edit/<?php echo $data["companyShareholder"]["data"][$z]["company_shareholder_id"]; ?>" name="edit" class="btn btn-primary">Edit</a>   
																										<?php } ?>
																										<?php if($data['Access']["data"][$a]["delete_flag"] == true) { ?>
																											<a href="#myModal" class="btn btn-danger" data-href="<?= base_url; ?>/CompanyShareholder/deleteCompanyShareholder/<?php echo $data["companyShareholder"]["data"][$z]["company_shareholder_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a>  
																										<?php } ?> 
																									</td>    
																								<?php } ?> 
																							</tr>
																						<?php }} ?>
																					</tbody>
																					<tfoot>
																						<tr>  
																							<th>Shareholder Name</th>
																							<th>Number Of Share</th>
																							<th>Percentage Of Share</th>
																							<th>Share Amount</th>   
																							<th>Remark</th> 
																							<th>Created By</th> 
																							<th>Created Date</th> 
																							<th>Updated By</th> 
																							<th>Updated Date</th> 
																							<?php if($data['Access']["data"][$a]["update_flag"] == true OR $data['Access']["data"][$a]["delete_flag"] == true) { ?>  
																								<th>Action</th>  
																							<?php } ?> 
																						</tr>
																					</tfoot>
																				</table>  
																			<?php } ?>
																		</div>
																		<!-- /.card-body -->
																	</div>
																	<!-- /.card -->
																</div>
																<!-- /.col -->
															</div>
															<!-- /.row -->
														</div>
														<!-- /.container-fluid -->
													<?php }}} ?>
												</div> 

												<div class="tab-pane" id="timeline"> 
													<?php if(empty($data['Access'])){}
													else{ for($aa=0; $aa< count($data['Access']["data"]); $aa++){ 
														if($data['Access']["data"][$aa]["form_php"] == "CompanyManagement"){ ?>
															<!-- Main content --> 
															<div class="container-fluid">
																<div class="row">
																	<div class="col-12">  
																		<div class="card">
																			<!-- =============================== add ================================ --> 
																			<?php if($data['Access']["data"][$aa]["create_flag"] == true ) { ?> 
																				<div class="card-header"> 
																						<a href="<?= base_url; ?>/CompanyManagement/add/<?php echo $data["company"]["data"]["company_id"]; ?>" class="btn btn-primary">Add</a>  
																				</div>  
																			<?php } ?>
																			<!-- =============================== add ================================ --> 
																			<?php if($data['Access']["data"][$aa]["read_flag"] == true ) { ?>  
																				<div class="card-body"> 
																					<table id="managementBOD" class="table table-bordered table-striped" width="100%"> 
																						<thead>
																							<tr>
																								<th>Management Type</th>
																								<th>Name</th>
																								<th>Remark</th> 
																								<th>Created By</th> 
																								<th>Created Date</th> 
																								<th>Updated By</th>
																								<th>Updated Date</th> 
																								<?php if($data['Access']["data"][$aa]["update_flag"] == true OR $data['Access']["data"][$aa]["delete_flag"] == true) { ?>   
																									<th>Action</th>   
																								<?php } ?>
																							</tr>
																						</thead>
																						<tbody>
																							<?php if(empty($data["companyManagement"]["data"])) { ?>
																								<tr>
																									<td>-</td>
																									<td>-</td>
																									<td>-</td>
																									<td>-</td>  
																									<td>-</td>
																									<td>-</td>  
																									<td>-</td>
																									<?php if($data['Access']["data"][$aa]["update_flag"] == true OR $data['Access']["data"][$aa]["delete_flag"] == true) { ?>  
																										<td>-</td>  
																									<?php } ?>
																								</tr> 
																							<?php }else{  for ($a = 0; $a< count($data["companyManagement"]["data"]); $a++) { ?>   
																								<tr>
																									<td><?php echo $data["companyManagement"]["data"][$a]["company_management_type_name"]; ?></td>  
																									<td><?php echo $data["companyManagement"]["data"][$a]["management_name"]; ?></td> 
																									<td><?php echo $data["companyManagement"]["data"][$a]["remark"]; ?></td> 
																									<?php if($data["companyManagement"]["data"][$a]["created_user_id"] == 0) { ?>
																										<td> </td>
																									<?php }else{ $UserCreate = $this->model('userModel')->getDataUser($token, $data["companyManagement"]["data"][$a]["created_user_id"])?>
																										<td><?php echo $UserCreate["data"]["username"];  ?></td>
																									<?php }?>
																									<?php if($data["companyManagement"]["data"][$a]["created_at"] == 0) { ?>
																										<td> </td>
																									<?php }else{ ?>
																										<td><?php echo date("d-m-Y", $data["companyManagement"]["data"][$a]["created_at"]); ?></td> 
																									<?php }?>
																									<?php if($data["companyManagement"]["data"][$a]["updated_user_id"] == 0) { ?>
																										<td> </td>
																									<?php }else{ $UserUpdt = $this->model('userModel')->getDataUser($token, $data["companyManagement"]["data"][$a]["updated_user_id"])?>
																										<td><?php echo $UserUpdt["data"]["username"];  ?></td>
																									<?php }?>
																									<?php if($data["companyManagement"]["data"][$a]["updated_at"] == 0) { ?>
																										<td> </td>
																									<?php }else{ ?>
																										<td><?php echo date("d-m-Y", $data["companyManagement"]["data"][$a]["updated_at"]); ?></td> 
																									<?php }?>
																									<?php if($data['Access']["data"][$aa]["update_flag"] == true OR $data['Access']["data"][$aa]["delete_flag"] == true) { ?>  
																										<td>  
																											<?php if($data['Access']["data"][$aa]["update_flag"] == true) { ?>
																												<a href="<?= base_url; ?>/CompanyManagement/edit/<?php echo $data["companyManagement"]["data"][$a]["company_management_id"]; ?>" name="edit" class="btn btn-primary">Edit</a>   
																											<?php } ?>
																											<?php if($data['Access']["data"][$aa]["delete_flag"] == true) { ?>
																												<a href="#myModal" class="btn btn-danger" data-href="<?= base_url; ?>/CompanyManagement/deletecompanyManagement/<?php echo $data["companyManagement"]["data"][$a]["company_management_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a>   
																											<?php } ?>
																										</td>     
																									<?php } ?>
																								</tr>
																							<?php }} ?>
																						</tbody>
																						<tfoot>
																							<tr>
																								<th>Management Type</th>
																								<th>Name</th>
																								<th>Remark</th> 
																								<th>Created By</th> 
																								<th>Created Date</th> 
																								<th>Updated By</th>
																								<th>Updated Date</th> 
																								<?php if($data['Access']["data"][$aa]["update_flag"] == true OR $data['Access']["data"][$aa]["delete_flag"] == true) { ?>   
																									<th>Action</th>   
																								<?php } ?>
																							</tr>
																						</tfoot>
																					</table>  
																				<?php } ?>
																			</div>
																			<!-- /.card-body -->
																		</div>
																		<!-- /.card -->
																	</div>
																	<!-- /.col -->
																</div>
																<!-- /.row -->
															</div>
															<!-- /.container-fluid -->
														<?php }}} ?>
													</div>

													<div class="tab-pane" id="settings"> 
														<?php if(empty($data['Access'])){}
														else{ for($aaa=0; $aaa< count($data['Access']["data"]); $aaa++){ 
															if($data['Access']["data"][$aaa]["form_php"] == "CompanyLicense"){ ?>
																<!-- Main content --> 
																<div class="container-fluid">
																	<div class="row">
																		<div class="col-12">  
																			<div class="card">
																				<!-- =============================== add ================================ --> 
																				<?php if($data['Access']["data"][$aaa]["create_flag"] == true ) { ?> 
																					<div class="card-header"> 
																							<a href="<?= base_url; ?>/CompanyLicense/add/<?php echo $data["company"]["data"]["company_id"]; ?>" class="btn btn-primary">Add</a>   
																					</div>  
																				<?php } ?>
																				<!-- =============================== add ================================ --> 
																				<?php if($data['Access']["data"][$aaa]["read_flag"] == true ) { ?>  
																					<div class="card-body"> 
																						<table id="ComlicenseBOD" class="table table-bordered table-striped">
																							<thead>
																								<tr>
																									<th>License Type</th> 
																									<th>Status</th> 
																									<th>Severity</th> 
																									<th>Renewal Status</th> 
																									<th>License No</th> 
																									<th>License Parent No</th>
																									<th>Renewable</th> 
																									<th>Issued By</th> 
																									<th>Issued Date</th> 
																									<th>Expired Date</th> 
																									<th>Earliest Renewal Date</th> 
																									<th>Last Renewal Date</th>  
																									<th>Approved By</th> 
																									<th>Approved Date</th> 
																									<th>Remark</th> 
																									<th>Created By</th> 
																									<th>Created Date</th> 
																									<th>Updated By</th> 
																									<th>Updated Date</th> 
																									<?php if($data['Access']["data"][$aaa]["update_flag"] == true OR $data['Access']["data"][$aaa]["delete_flag"] == true) { ?>
																										<th>Action</th>  
																									<?php } ?> 
																								</tr>
																							</thead>
																							<tbody>
																								<?php if(empty($data["companyLicense"]["data"])) { ?>
																									<tr>
																										<td>-</td>
																										<td>-</td>
																										<td>-</td>
																										<td>-</td>
																										<td>-</td> 
																										<td>-</td> 
																										<td>-</td>
																										<td>-</td>
																										<td>-</td>   
																										<td>-</td>
																										<td>-</td>  
																										<td>-</td>
																										<td>-</td>
																										<td>-</td>
																										<td>-</td> 
																										<td>-</td>
																										<td>-</td>
																										<td>-</td>   
																										<td>-</td>
																										<td>-</td>   
																									</tr> 
																								<?php }else{  for ($a = 0; $a< count($data["companyLicense"]["data"]); $a++) { ?>   
																									<tr>
																										<td><?php echo $data["companyLicense"]["data"][$a]["license_type_name"]; ?></td>  
																										<?php if($data["companyLicense"]["data"][$a]["status"] == 1){ ?> 
																											<td>Draft</td>    
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 2){ ?>  
																											<td>Need Approval</td>        
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] < $data["companyLicense"]["data"][$a]["expired_date"]){ ?> 
																											<td style="background-color:#FFFF00;">Open to Renew</td> 
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["expired_date"] <= time()){ ?> 
																											<td style="background-color:#FF0000;">Expired</td>   
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3){ ?> 
																											<td style="background-color:#00FF00;">Active</td>     
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 4){ ?>
																											<td>Not Active</td>      
																										<?php } ?>
																										<?php if($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] < $data["companyLicense"]["data"][$a]["last_renewal_date"]){ ?>
																											<td>LOW</td> 
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["last_renewal_date"] <= time() && $data["companyLicense"]["data"][$a]["last_renewal_date"] < $data["companyLicense"]["data"][$a]["expired_date"]){ ?>
																											<td>HIGH</td>   
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["expired_date"] < time()){ ?> 
																											<td>CRITICAL</td>        
																										<?php }else{?>
																											<td> </td>
																										<?php } ?>
																										<?php if($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$a]["renewal_status"] == 1){ ?>  
																											<td>Open</td>   
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$a]["renewal_status"] == 2){ ?>  
																											<td>Follow Up</td>   
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$a]["renewal_status"] == 3){ ?>  
																											<td>Renew</td>  
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$a]["renewal_status"] == 4){ ?> 
																											<td>Close Permit</td>  
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$a]["renewal_status"] == 5){ ?> 
																											<td>Renew Approved</td>  
																										<?php }elseif($data["companyLicense"]["data"][$a]["status"] == 3 && $data["companyLicense"]["data"][$a]["earliest_renewal_date"] <= time() && $data["companyLicense"]["data"][$a]["renewal_status"] == 6){ ?> 
																											<td>Close Permit Approved</td>  
																										<?php }else{ ?>
																											<td> </td>
																										<?php } ?>
																										<td><?php echo $data["companyLicense"]["data"][$a]["license_no"]; ?></td>  
																										<?php $Pid = $data["companyLicense"]["data"][$a]["parent_license_id"];
																										$data["ParentLicense"] = $this->model('companylicenseModel')->getDataCompanyLicense($token, $Pid); 
																										if($data["ParentLicense"]["status"] == false) {?>
																										<td> </td>
																										<?php }else{?>
																										<td><?php echo $data["ParentLicense"]["data"]["license_no"]; ?></td>    
																										<?php } ?>  
																										<?php if($data["companyLicense"]["data"][$a]["renewable"] == 1){ ?> 
																											<td>True</td>    
																										<?php }else{ ?>  
																											<td>False</td> 
																										<?php } ?>
																										<td><?php echo $data["companyLicense"]["data"][$a]["issued_by"]; ?></td>  
																										<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$a]["issued_date"]); ?></td> 
																										<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$a]["expired_date"]); ?></td>  
																										<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$a]["earliest_renewal_date"]); ?></td> 
																										<td><?php echo date("d-m-Y", $data['companyLicense']["data"][$a]["last_renewal_date"]); ?></td>  
																										<?php if($data["companyLicense"]["data"][$a]["approved_user_id"] == 0) { ?>
																											<td> </td>
																										<?php }else{ $UserApp = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"][$a]["approved_user_id"])?>
																											<td><?php echo $UserApp["data"]["username"];  ?></td>
																										<?php }?>
																										<?php if($data["companyLicense"]["data"][$a]["approved_date"] == 0) { ?>
																											<td> </td>
																										<?php }else{ ?>
																											<td><?php echo date("d-m-Y", $data["companyLicense"]["data"][$a]["approved_date"]); ?></td> 
																										<?php }?>
																										<td><?php echo $data["companyLicense"]["data"][$a]["remark"]; ?></td>  
																										<?php if($data["companyLicense"]["data"][$a]["created_user_id"] == 0) { ?>
																											<td> </td>
																										<?php }else{ $UserCreate = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"][$a]["created_user_id"])?>
																											<td><?php echo $UserCreate["data"]["username"];  ?></td>
																										<?php }?>
																										<?php if($data["companyLicense"]["data"][$a]["created_at"] == 0) { ?>
																											<td> </td>
																										<?php }else{ ?>
																											<td><?php echo date("d-m-Y", $data["companyLicense"]["data"][$a]["created_at"]); ?></td> 
																										<?php }?>
																										<?php if($data["companyLicense"]["data"][$a]["updated_user_id"] == 0) { ?>
																											<td> </td>
																										<?php }else{ $UserUpdt = $this->model('userModel')->getDataUser($token, $data["companyLicense"]["data"][$a]["updated_user_id"])?>
																											<td><?php echo $UserUpdt["data"]["username"];  ?></td>
																										<?php }?>
																										<?php if($data["companyLicense"]["data"][$a]["updated_at"] == 0) { ?>
																											<td> </td>
																										<?php }else{ ?>
																											<td><?php echo date("d-m-Y", $data["companyLicense"]["data"][$a]["updated_at"]); ?></td> 
																										<?php }?>
																										<?php if($data['Access']["data"][$aaa]["update_flag"] == true OR $data['Access']["data"][$aaa]["delete_flag"] == true) { ?>  
																											<td>  
																												<?php if($data['Access']["data"][$aaa]["update_flag"] == true) { ?>
																													<a href="<?= base_url; ?>/CompanyLicense/edit/<?php echo $data["companyLicense"]["data"][$a]["company_license_id"]; ?>" name="edit" class="btn btn-primary">Edit</a>   
																												<?php } ?>
																												<?php if($data['Access']["data"][$aaa]["delete_flag"] == true) { ?>
																													<a href="#myModal" class="btn btn-danger" data-href="<?= base_url; ?>/CompanyLicense/deleteCompanyLicense/<?php echo $data["companyLicense"]["data"][$a]["company_license_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a>  
																													
																												<?php } ?>
																											</td>     
																										<?php } ?>
																									</tr>
																								<?php }} ?>
																							</tbody>
																							<tfoot>
																								<tr>
																									<th>License Type</th> 
																									<th>Status</th> 
																									<th>Severity</th> 
																									<th>Renewal Status</th> 
																									<th>License No</th> 
																									<th>License Parent No</th>
																									<th>Renewable</th> 
																									<th>Issued By</th> 
																									<th>Issued Date</th> 
																									<th>Expired Date</th> 
																									<th>Earliest Renewal Date</th> 
																									<th>Last Renewal Date</th>  
																									<th>Approved By</th> 
																									<th>Approved Date</th> 
																									<th>Remark</th> 
																									<th>Created By</th> 
																									<th>Created Date</th> 
																									<th>Updated By</th> 
																									<th>Updated Date</th> 
																									<?php if($data['Access']["data"][$aaa]["update_flag"] == true OR $data['Access']["data"][$aaa]["delete_flag"] == true) { ?>
																										<th>Action</th>  
																									<?php } ?> 
																								</tr>
																							</tfoot>
																						</table>  
																					<?php } ?>
																				</div>
																				<!-- /.card-body -->
																			</div>
																			<!-- /.card -->
																		</div>
																		<!-- /.col -->
																	</div>
																	<!-- /.row -->
																</div>
																<!-- /.container-fluid -->
															<?php }}} ?>

														</div>

													</div>
												</div>

											</div>
										</div>
									</div>
								</section>

								<!-- /.content -->   
							<?php }}} ?>
							<!-- /.content --> 
						</div>
						<!-- /.content-wrapper -->

						<!-- Control Sidebar -->
						<aside class="control-sidebar control-sidebar-dark">
							<!-- Control sidebar content goes here -->
						</aside>
						<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
					</body>
					</html>
