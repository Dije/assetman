<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>

			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
						if($data['Access']["data"][$i]["read_flag"] == true ) { ?> 
							<!-- Main content -->
							<section class="content">
							<div class="row">
								<div class="col-sm-12">
								<?php
									Flasher::Message();
								?>
								</div>
							</div>
								<div class="container-fluid">
									<div class="row">
										<div class="col-12">  
											<div class="card">
												<div class="card-header">
													<!-- =============================== CREATE ================================ --> 
													<?php if($data['Access']["data"][$i]["create_flag"] == true ) { ?> 
														<a href="<?= base_url; ?>/Company/add" class="btn btn-primary">Add</a> 
													<?php } ?>
													<!-- =============================== CREATE ================================ --> 
												</div>
												<!-- /.card-header -->
												<div class="card-body"> 
													<table id="company" class="table table-bordered table-striped">
														<thead>
															<tr> 
																<th>Company</th>
																<th>Business Unit</th>
																<th>Address</th> 
																<th>Legal & License File URL</th> 
																<th>Status</th> 
																<th>Approved By</th> 
																<th>Approved Date</th> 
																<th>Remark</th> 
																<th>Created By</th> 
																<th>Created Date</th> 
																<th>Updated By</th> 
																<th>Updated Date</th> 
																<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																	<th>Action</th>  
																<?php } ?>
																<?php if($data["userRole"]["data"]["role_code"] == "BOD") { ?> 
																	<th>Action</th>  
																<?php } ?>
															</tr>
														</thead>
														<tbody>
															<?php if(empty($data["company"]["data"])) { ?>
																<tr> 
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>  
																	<td>-</td>
																	<td>-</td> 
																	<td>-</td>
																	<td>-</td>
																	<td>-</td>  
																	<td>-</td>
																	<td>-</td> 
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																		<th>-</th>  
																	<?php } ?>
																	<?php if($data["userRole"]["data"]["role_code"] == "BOD") { ?> 
																		<th>-</th>  
																	<?php } ?>
																</tr> 
															<?php }else{  for ($x = 0; $x < count($data["company"]["data"]); $x++) { ?>   
																<tr>
																	<td><?php echo $data["company"]["data"][$x]["company_name"]; ?></td> 
																	<td><?php echo $data["company"]["data"][$x]["business_unit_name"]; ?></td>
																	<td><?php echo $data["company"]["data"][$x]["address"]; ?></td>  
																	<td><?php echo $data["company"]["data"][$x]["legal_license_file_url"]; ?></td>  
																	<?php if($data["company"]["data"][$x]["status"] == 1){ ?> 
																		<td>Draft</td> 
																	<?php }elseif($data["company"]["data"][$x]["status"] == 2){ ?> 
																		<td>Need Approval</td>  
																	<?php }elseif($data["company"]["data"][$x]["status"] == 3){ ?> 
																		<td>Active</td> 
																	<?php }elseif($data["company"]["data"][$x]["status"] == 4){ ?>
																		<td>NotActive</td> 
																	<?php } ?>
																	<?php if($data["company"]["data"][$x]["approved_user_id"] == 0) { ?>
																		<td> </td>
																	<?php }else{ $UserApp = $this->model('userModel')->getDataUser($token, $data["company"]["data"][$x]["approved_user_id"])?>
																		<td><?php echo $UserApp["data"]["username"];  ?></td>
																	<?php }?>
																	<?php if($data["company"]["data"][$x]["approved_date"] == 0) { ?>
																		<td> </td>
																	<?php }else{ ?>
																		<td><?php echo date("d-m-Y", $data["company"]["data"][$x]["approved_date"]); ?></td> 
																	<?php }?>
																	<td style="white-space: pre-line;"><?php echo $data["company"]["data"][$x]["remark"]; ?></td>  
																	<?php if($data["company"]["data"][$x]["created_user_id"] == 0) { ?>
																		<td> </td>
																	<?php }else{ $UserCreate = $this->model('userModel')->getDataUser($token, $data["company"]["data"][$x]["created_user_id"])?>
																		<td><?php echo $UserCreate["data"]["username"];  ?></td>
																	<?php }?>
																	<?php if($data["company"]["data"][$x]["created_at"] == 0) { ?>
																		<td> </td>
																	<?php }else{ ?>
																		<td><?php echo date("d-m-Y", $data["company"]["data"][$x]["created_at"]); ?></td> 
																	<?php }?>
																	<?php if($data["company"]["data"][$x]["updated_user_id"] == 0) { ?>
																		<td> </td>
																	<?php }else{ $UserUpdt = $this->model('userModel')->getDataUser($token, $data["company"]["data"][$x]["updated_user_id"])?>
																		<td><?php echo $UserUpdt["data"]["username"];  ?></td>
																	<?php }?>
																	<?php if($data["company"]["data"][$x]["updated_at"] == 0) { ?>
																		<td> </td>
																	<?php }else{ ?>
																		<td><?php echo date("d-m-Y", $data["company"]["data"][$x]["updated_at"]); ?></td> 
																	<?php }?>
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>  
																		<td> 
																			<?php if($data['Access']["data"][$i]["update_flag"] == true) { ?>
																				<a href="<?= base_url; ?>/Company/edit/<?php echo $data["company"]["data"][$x]["company_id"]; ?>" name="edit" class="btn btn-primary">Edit</a> 
																			<?php } ?>
																			<?php if($data['Access']["data"][$i]["delete_flag"] == true) { ?>
																				<a href="#myModal" class="btn btn-danger" data-href="<?= base_url; ?>/Company/deleteCompany/<?php echo $data["company"]["data"][$x]["company_id"]; ?>" data-toggle="modal" data-target="#myModal">Delete</a> 
																			<?php } ?>
																		</td>    
																	<?php } ?>
																	<?php if($data["userRole"]["data"]["role_code"] == "BOD") { ?>
																		<td> 
																			<a href="<?= base_url; ?>/Company/detail/<?php echo $data["company"]["data"][$x]["company_id"]; ?>" name="detail" class="btn btn-primary">Detail</a> 
																		</td> 
																	<?php } ?>
																</tr>
																<?php }} ?>
															</tbody>
															<tfoot>
																<tr> 
																	<th>Company</th>
																	<th>Business Unit</th>
																	<th>Address</th> 
																	<th>Legal & License File URL</th> 
																	<th>Status</th> 
																	<th>Approved By</th> 
																	<th>Approved Date</th> 
																	<th>Remark</th> 
																	<th>Created By</th> 
																	<th>Created Date</th> 
																	<th>Updated By</th> 
																	<th>Updated Date</th> 
																	<?php if($data['Access']["data"][$i]["update_flag"] == true OR $data['Access']["data"][$i]["delete_flag"] == true) { ?>
																		<th>Action</th>  
																	<?php } ?>
																	<?php if($data["userRole"]["data"]["role_code"] == "BOD") { ?> 
																		<th>Action</th>  
																	<?php } ?>
																</tr>
															</tfoot>
														</table> 
													</div>
													<!-- /.card-body -->
												</div>
												<!-- /.card -->
											</div>
											<!-- /.col -->
										</div>
										<!-- /.row -->
									</div>
									<!-- /.container-fluid -->
								</section>
								<!-- /.content -->
							<?php }}}}?>
							<!-- /.content --> 
						</div>
						<!-- /.content-wrapper -->

						<!-- Control Sidebar -->
						<aside class="control-sidebar control-sidebar-dark">
							<!-- Control sidebar content goes here -->
						</aside>
						<!-- /.control-sidebar --> 
						<?php 
						require_once '../app/views/templates/modal.php';
						?>   
				</body>
				</html>
