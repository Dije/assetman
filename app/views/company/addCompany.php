<body class="hold-transition sidebar-mini">
	<div class="wrapper">
		<?php 
		require_once '../app/views/templates/navbar.php';
		?>

		<?php 
		require_once '../app/views/templates/sidebar.php';
		?>   

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<?php 
			require_once '../app/views/templates/breadcrumb.php';
			?>
			
			<!-- Main content -->
			<?php if(empty($data['Access'])){}
			else{ for($i=0; $i< count($data['Access']["data"]); $i++){ 
					if($data['Access']["data"][$i]["form_php"] == $data['PHP']){  
							//================================= CREATE ================================= 
						if($data['Access']["data"][$i]["create_flag"] == true ) { ?>
							<section class="content">
								<div class="container-fluid">
									<div class="row">
										<!-- left column -->
										<div class="col-md-12">
											<!-- general form elements -->
											<div class="card card-primary">
												<div class="card-header">
													<h3 class="card-title">Create New Data</h3>
												</div>
												<!-- /.card-header -->
												<!-- form start -->
												<form action="<?= base_url; ?>/Company/addCompany" method="POST" name="form1" >
												<div class="row">
														<div class="card-body col-md-6"> 
														<div class="form-group">
															<label for="exampleInputEmail1">Company*</label>
															<input type="text" name="CompanyName" class="form-control" id="exampleInputOSName" placeholder="Enter Company Name">
														</div>     
														<div class="form-group">
															<label for="exampleInputEmail1">Business Type*</label> 
															<select name="BusinessUnitID" class="form-control select2" style="width: 100%;"> 
																<?php for ($x = 0; $x < count($data["businessUnit"]["data"]); $x++) { ?>  
																	<option value="<?php echo $data["businessUnit"]["data"][$x]["business_unit_id"]; ?>"><?php echo $data["businessUnit"]["data"][$x]["business_unit_name"]; ?></option> 
																<?php } ?>
															</select>  
														</div>      
														<div class="form-group">
															<label for="exampleInputEmail1">Address</label>
															<textarea id="adress" name="Address" class="form-control" rows="4" cols="50" placeholder="Enter Address"></textarea> 
														</div>    
														<div class="form-group">
							         						<label for="exampleInputEmail1">Legal & License File Url</label>
															<input type="text" name="LegalLicenseFileURL" class="form-control" id="exampleInputOSName" placeholder="Enter Legal & License File Url">  
														</div>     
														<div class="form-group">
															<label for="exampleInputEmail1">Status</label>
															<select name="Status" class="form-control select2" style="width: 100%;">  
																<option value="1">Draft</option> 
																<option value="2">Need Approval</option> 
																<option value="3">Active</option> 
																<option value="4">Not Active</option> 
															</select>   
														</div>                             
													</div>
													<div class="card-body col-md-6">     
														<div class="form-group">
															<label for="exampleInputEmail1">Approved By</label>
															<input type="text" name="ApprovedBy" class="form-control" id="exampleInputOSName" placeholder="" disabled>
														</div>    
														<div class="form-group">
															<label for="exampleInputEmail1">Approved Date</label>
															<input type="text" name="ApprovedDate" class="form-control" id="exampleInputOSName" placeholder="" disabled>
														</div>    
														<div class="form-group">
															<label for="exampleInputEmail1">Deactivated By</label>
															<input type="text" name="DeactivatedBy" class="form-control" id="exampleInputOSName" placeholder="" disabled>
														</div>    
														<div class="form-group">
															<label for="exampleInputEmail1">Deactivated Date</label>
															<input type="text" name="DeactivatedDate" class="form-control" id="exampleInputOSName" placeholder="" disabled>
														</div>                       
														<div class="form-group">
															<label for="exampleInputEmail1">Remark</label>
															<textarea id="Remark" name="Remark" class="form-control" rows="4" cols="50" placeholder="Enter Address"></textarea> 
														</div> 
													</div>
													<!-- /.card-body -->
													</div>

													<div class="card-footer">
														<button type="submit" name="submit" class="btn btn-danger">Submit</button> 
														<input type="button" name="cancel" class="btn btn-outline-dark" value="Cancel" onclick="company()">
													</div>
												</form> 
											</div>
											<!-- /.card -->
										</div>
									</div>
									<!-- /.row -->
								</div><!-- /.container-fluid -->
							</section>
							<!-- /.content -->  
						<?php }}}} ?>
					</div>
					<!-- /.content -->  

					<!-- Control Sidebar -->
					<aside class="control-sidebar control-sidebar-dark">
						<!-- Control sidebar content goes here -->
					</aside>
					<!-- /.control-sidebar --> 
			</body>
			</html>
 