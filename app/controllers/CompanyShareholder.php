<?php  
class CompanyShareholder extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	// public function index(){
	// 	$token = $_SESSION['AccessToken'];
	// 	$UserID = $_SESSION['user_id'];
	// $timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
	// // echo $timezone_name; 
	// if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
	// $timezone_name = 'Asia/Jakarta';
	// $Clock = "WIB";
	// }
	// else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
	// $timezone_name = 'Asia/Makassar'; 
	// $Clock = "WITA";
	// }
	// else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
	// $timezone_name = 'Asia/Jayapura'; 
	// $Clock = "WIT";
	// }
	// date_default_timezone_set($timezone_name);  
	// $date = new DateTime(null); 
	// $Date = strtotime(date("Y-m-d H:i:s"));
	// 	$data['title'] = 'Data CompanyShareholder';
	// 	$data['PHP'] = "CompanyShareholder"; 
	// 	$data['companyShareholder'] = $this->model('companyShareholderModel')->getDataCompanyShareholders($token);
	// 	$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
	// 	$this->view('templates/header', $data);
	// 	$this->view('companyShareholder/index', $data);
	// 	$this->view('templates/footer');  
	// 	$this->view('templates/script'); 
	// }

	public function add($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Company Shareholder'; 
		$data['PHP'] = "CompanyShareholder"; 
		// $data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
        $data['CompanyID'] = $id;
		$this->view('templates/header', $data);
		$this->view('companyShareholder/addCompanyShareholder', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company Shareholder';
		$data['PHP'] = "CompanyShareholder"; 
		$data['companyShareholder'] = $this->model('companyshareholderModel')->getDataCompanyShareholder($token, $id);
		// $data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$this->view('templates/header', $data);
		$this->view('companyShareholder/editCompanyShareholder', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addCompanyShareholder(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Company Shareholder';
		$data['PHP'] = "CompanyShareholder";  
		$row = $this->model('companyshareholderModel')->CreateCompanyShareholder($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);  
			exit;	
		} else if($row['message'] == "Failed to register companyShareholder"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);  
			exit;	
		} 
	} 

	public function updateCompanyShareholder(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company Shareholder';
		$data['PHP'] = "CompanyShareholder";  
		$row = $this->model('companyshareholderModel')->UpdateDataCompanyShareholder($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);  
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} else if($row['message'] == "Failed to update companyShareholder"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} 
	}

	public function deleteCompanyShareholder($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete CompanyShareholder'; 
		$data['companyShareholder'] = $this->model('companyshareholderModel')->getDataCompanyShareholder($token, $id);
        $CompanyID =  $data['companyShareholder']["data"]["company_id"]; 
		$CompanyShareholderName = $data['companyShareholder']["data"]["shareholder_name"]; 
		$CompanyShareholderNewName = $CompanyShareholderName."-DELETE"; 
		$row = $this->model('companyshareholderModel')->DeleteCompanyShareholder($token, $id, $CompanyShareholderNewName, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} else if($row['message'] == "Failed to delete companyShareholder"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} 
	}
}