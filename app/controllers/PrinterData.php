<?php  
class PrinterData extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data Printer';
		$data['PHP'] = "PrinterData"; 
		$data['printer'] = $this->model('printerModel')->getDataPrinters($token);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$this->view('templates/header', $data);
		$this->view('printer/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Printer'; 
		$data['PHP'] = "PrinterData"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['black'] = $this->model('inkModel')->getDataBlackInks($token);
		$data['cyan'] = $this->model('inkModel')->getDataCyanInks($token);
		$data['magenta'] = $this->model('inkModel')->getDataMagentaInks($token);
		$data['yellow'] = $this->model('inkModel')->getDataYellowInks($token);
		$data['color'] = $this->model('inkModel')->getDataColorInks($token);
		$data['tricolor'] = $this->model('inkModel')->getDataTricolorInks($token);
		$data['others'] = $this->model('inkModel')->getDataOthersInks($token);
		$this->view('templates/header', $data);
		$this->view('printer/addPrinter', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Edit Printer';
		$data['PHP'] = "PrinterData"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['printer'] = $this->model('printerModel')->getDataPrinter($token, $id);
		$blackid = $data['printer']["data"]["black_id"]; 
		$data['black'] = $this->model('inkModel')->getExcDataBlackInk($token, $blackid);
		$cyanid = $data['printer']["data"]["cyan_id"]; 
		$data['cyan'] = $this->model('inkModel')->getExcDataCyanInk($token, $cyanid);
		$magentaid = $data['printer']["data"]["magenta_id"]; 
		$data['magenta'] = $this->model('inkModel')->getExcDataMagentaInk($token, $magentaid);
		$yellowid = $data['printer']["data"]["yellow_id"]; 
		$data['yellow'] = $this->model('inkModel')->getExcDataYellowInk($token, $yellowid);
		$colorid = $data['printer']["data"]["color_id"]; 
		$data['color'] = $this->model('inkModel')->getExcDataColorInk($token, $colorid);
		$tricolorid = $data['printer']["data"]["tricolor_id"]; 
		$data['tricolor'] = $this->model('inkModel')->getExcDataTricolorInk($token, $tricolorid);
		$othersid = $data['printer']["data"]["others_id"]; 
		$data['others'] = $this->model('inkModel')->getExcDataOthersInk($token, $othersid);
		$this->view('templates/header', $data);
		$this->view('printer/editPrinter', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addPrinter(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Printer';
		$data['PHP'] = "PrinterData";  
		$row = $this->model('printerModel')->CreatePrinter($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/PrinterData'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} else if($row['message'] == "Failed to register printer"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} 
	} 

	public function updatePrinter(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Printer';
		$data['PHP'] = "PrinterData";  
		$row = $this->model('printerModel')->UpdateDataPrinter($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/PrinterData'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} else if($row['message'] == "Failed to update printer"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} 
	}

	public function deletePrinter($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete Printer'; 
		$data['printer'] = $this->model('printerModel')->getDataPrinter($token, $id); 
		$PrinterName = $data['printer']["data"]["printer_name"]; 
		$PrinterNewName = $PrinterName."-DELETE"; 
		$row = $this->model('printerModel')->DeletePrinter($token, $id, $PrinterNewName, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/PrinterData'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} else if($row['message'] == "Failed to delete printer"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/PrinterData');  
			exit;	
		} 
	}
}