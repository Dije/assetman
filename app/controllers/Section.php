<?php  
class Section extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data Section';
		$data['PHP'] = "Section";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);   
		$this->view('templates/header', $data);
		$this->view('section/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function ajaxGetDataSection(){
		$data['token']  = $_SESSION['AccessToken'];    
		$data['UserID'] = $_SESSION['user_id'];  
		$data['PHP'] = "Section";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($data['token'], $data['UserID']);  
		$this->view('server_side/section_processing', $data);
	}

	public function ajaxGetDepartment(){
		$token = $_SESSION['AccessToken'];   
		$divId = $_POST['DivisionID'];
		$data['department'] = $this->model('departmentModel')->getDataDepartmentDiv($token, $divId);   
		$this->view('section/getDepartment', $data);   
	}

	public function ajaxGetDepartmentEdt(){
		$token = $_SESSION['AccessToken'];   
		$sectionId = $_POST['id'];
		$data['section'] = $this->model('sectionModel')->getDataSection($token, $sectionId); 
		$divId = $data['section']["data"]["division_id"];
		$depId = $data['section']["data"]['department_id'];
		$data['department'] = $this->model('departmentModel')->getExcDataDepartment($token, $divId, $depId); 
		$this->view('section/getDepartmentEdt', $data);   
	}

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Add Section'; 
		$data['PHP'] = "Section";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['department'] = $this->model('departmentModel')->getDataDepartments($token);  
		$data['division'] = $this->model('divisionModel')->getDataDivisions($token);  
		$this->view('templates/header', $data);
		$this->view('section/addSection', $data); 
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Edit Section';
		$data['PHP'] = "Section"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['section'] = $this->model('sectionModel')->getDataSection($token, $id);
		$divId = $data['section']["data"]["division_id"];
		$data['division'] = $this->model('divisionModel')->getExcDataDivision($token, $divId);  
		$this->view('templates/header', $data);
		$this->view('section/editSection', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addSection(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Section';
		$data['PHP'] = "Section";  
		$row = $this->model('sectionModel')->CreateSection($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Section'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} else if($row['message'] == "Failed to register section"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} 
	} 

	public function updateSection(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Section';
		$data['PHP'] = "Section";  
		$row = $this->model('sectionModel')->UpdateDataSection($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Section'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} else if($row['message'] == "Failed to update section"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} 
	}

	public function deleteSection($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete Section'; 
		$data['section'] = $this->model('sectionModel')->getDataSection($token, $id); 
		$SectionName = $data['section']["data"]["section_name"]; 
		$SectionNewName = $SectionName."-DELETE"; 
		$row = $this->model('sectionModel')->DeleteSection($token, $id, $SectionNewName, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Section'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} else if($row['message'] == "Failed to delete section"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Section');  
			exit;	
		} 
	}
} 