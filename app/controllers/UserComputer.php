<?php  
class UserComputer extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data User Computer';
		$data['PHP'] = "UserComputer";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['computer'] = $this->model('computerModel')->getDataComputers($token); 
		$this->view('templates/header', $data);
		$this->view('computer/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	} 

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Add User Computer'; 
		$data['PHP'] = "UserComputer";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['user'] = $this->model('userModel')->getDataUsers($token);
		$data['processor'] = $this->model('processorModel')->getDataProcessors($token);
		$data['motherboard'] = $this->model('motherboardModel')->getDataMotherboards($token);
		$data['ram'] = $this->model('ramModel')->getDataRAMs($token);
		$data['hdd'] = $this->model('hddModel')->getDataHDDs($token);
		$data['casing'] = $this->model('casingModel')->getDataCasings($token);
		$data['keyboard'] = $this->model('keyboardModel')->getDataKeyboards($token);
		$data['mouse'] = $this->model('mouseModel')->getDataMouses($token);
		$data['monitor'] = $this->model('monitorModel')->getDataMonitors($token);
		$data['os'] = $this->model('osModel')->getDataOSs($token);
		$data['office'] = $this->model('officeModel')->getDataOffices($token);
		$this->view('templates/header', $data);
		$this->view('computer/addComputer', $data); 
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Edit User Computer';
		$data['PHP'] = "UserComputer"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['computer'] = $this->model('computerModel')->getDataComputer($token, $id);
		$idUser = $data['computer']["data"]["user_id"];
		$data['user'] = $this->model('userModel')->getExcDataUser($token, $idUser);
		$idProcessor = $data["computer"]["data"]["processor_id"];
		$data['processor'] = $this->model('processorModel')->getExcDataProcessor($token, $idProcessor);
		$idMotherboard = $data["computer"]["data"]["motherboard_id"];
		$data['motherboard'] = $this->model('motherboardModel')->getExcDataMotherboard($token, $idMotherboard);
		$idRAM = $data["computer"]["data"]["ram_id"];
		$data['ram'] = $this->model('ramModel')->getExcDataRAM($token, $idRAM);
		$idHDD = $data["computer"]["data"]["hdd_id"];
		$data['hdd'] = $this->model('hddModel')->getExcDataHDD($token, $idHDD);
		$idCasing = $data["computer"]["data"]["casing_id"];
		$data['casing'] = $this->model('casingModel')->getExcDataCasing($token, $idCasing);
		$idKeyboard = $data["computer"]["data"]["keyboard_id"];
		$data['keyboard'] = $this->model('keyboardModel')->getExcDataKeyboard($token, $idKeyboard);
		$idMouse = $data["computer"]["data"]["mouse_id"];
		$data['mouse'] = $this->model('mouseModel')->getExcDataMouse($token, $idMouse);
		$idMonitor = $data["computer"]["data"]["monitor_id"];
		$data['monitor'] = $this->model('monitorModel')->getExcDataMonitor($token, $idMonitor);
		$idOs = $data["computer"]["data"]["os_id"];
		$data['os'] = $this->model('osModel')->getExcDataOS($token, $idOs);
		$idOffice = $data["computer"]["data"]["office_id"];
		$data['office'] = $this->model('officeModel')->getExcDataOffice($token, $idOffice);
		$this->view('templates/header', $data);
		$this->view('computer/editComputer', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addComputer(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Computer';
		$data['PHP'] = "UserComputer";  
		if($_POST['CD'] == "YES"){
			$CD = 1; 
		}else{
			$CD = 0;
		}
		$row = $this->model('computerModel')->CreateComputer($token, $_POST, $CD, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserComputer'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} else if($row['message'] == "Failed to register computer"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} 
	} 

	public function updateComputer(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Computer';
		$data['PHP'] = "UserComputer";  
		if($_POST['CD'] == "YES"){
			$CD = 1; 
		}else{
			$CD = 0;
		}
		$row = $this->model('computerModel')->UpdateDataComputer($token, $_POST, $CD, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserComputer'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} else if($row['message'] == "Failed to update computer"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} 
	}

	public function deleteComputer($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete Computer';   
		$data['computer'] = $this->model('computerModel')->getDataComputer($token, $id);
		$ComputerCode = $data['computer']["data"]["computer_code"]; 
		$ComputerNewCode = $ComputerCode."-DELETE"; 
		$row = $this->model('computerModel')->DeleteComputer($token, $id, $ComputerNewCode, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserComputer'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} else if($row['message'] == "Failed to delete computer"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserComputer');  
			exit;	
		} 
	}
} 