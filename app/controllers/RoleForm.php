<?php  
class RoleForm extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data User Role';
		$data['PHP'] = "RoleForm";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);   
		$this->view('templates/header', $data);
		$this->view('roleForm/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	} 	

	public function ajaxGetDataRoleForm(){
		$data['token']  = $_SESSION['AccessToken'];    
		$data['UserID'] = $_SESSION['user_id'];  
		$data['PHP'] = "RoleForm";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($data['token'], $data['UserID']);  
		$this->view('server_side/roleForm_processing', $data);
	}

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Add Role Form'; 
		$data['PHP'] = "RoleForm";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['form'] = $this->model('formModel')->getDataForms($token);  
		$data['role'] = $this->model('roleModel')->getDataRoles($token);  
		$this->view('templates/header', $data);
		$this->view('roleForm/addRoleForm', $data); 
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Edit Role Form';
		$data['PHP'] = "RoleForm"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['roleForm'] = $this->model('roleformModel')->getDataRoleForm($token, $id);
		$idRole = $data["roleForm"]["data"]["role_id"];
		$data['role'] = $this->model('roleModel')->getExcDataRole($token, $idRole); 
		$idForm = $data['roleForm']["data"]["form_id"];
		$data['form'] = $this->model('formModel')->getExcDataFormOnly($token, $idForm); 
		$this->view('templates/header', $data);
		$this->view('roleForm/editRoleForm', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addRoleForm(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Role Form';
		$data['PHP'] = "RoleForm";  
		if(isset($_POST['Create'])){
			$Create = $_POST['Create']; 
		}else{
			$Create = 0;
		}
		if(isset($_POST['Read'])){
			$Read = $_POST['Read']; 
		}else{
			$Read = 0;
		} 
		if(isset($_POST['Update'])){
			$Update = $_POST['Update']; 
		}else{
			$Update = 0;
		}  
		if(isset($_POST['Delete'])){
			$Delete = $_POST['Delete']; 
		}else{
			$Delete = 0;
		}    
		$row = $this->model('roleformModel')->CreateRoleForm($token, $_POST, $Create, $Read, $Update, $Delete, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success'); 
			header('location: '. base_url . '/RoleForm'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} else if($row['message'] == "Failed to register roleForm"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} 
	} 

	public function updateRoleForm(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Role Form';
		$data['PHP'] = "RoleForm";  
		if(isset($_POST['Create'])){
			$Create = $_POST['Create']; 
		}else{
			$Create = 0;
		}
		if(isset($_POST['Read'])){
			$Read = $_POST['Read']; 
		}else{
			$Read = 0;
		} 
		if(isset($_POST['Update'])){
			$Update = $_POST['Update']; 
		}else{
			$Update = 0;
		}  
		if(isset($_POST['Delete'])){
			$Delete = $_POST['Delete']; 
		}else{
			$Delete = 0;
		}      
		$row = $this->model('roleformModel')->UpdateDataRoleForm($token, $_POST, $Create, $Read, $Update, $Delete, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/RoleForm'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} else if($row['message'] == "Failed to update roleForm"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} 
	}

	public function deleteRoleForm($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete Role Form';   
		$row = $this->model('roleformModel')->DeleteRoleForm($token, $id, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/RoleForm'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} else if($row['message'] == "Failed to delete roleForm"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/RoleForm');  
			exit;	
		} 
	}
} 