<?php  
class Employee extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data Employee';
		$data['PHP'] = "Employee";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);   
		$this->view('templates/header', $data);
		$this->view('employee/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function ajaxGetDataEmployee(){
		$data['token']  = $_SESSION['AccessToken'];    
		$data['UserID'] = $_SESSION['user_id'];  
		$data['PHP'] = "Employee";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($data['token'], $data['UserID']);  
		$this->view('server_side/employee_processing', $data);
	}

	public function ajaxGetSection(){
		$token = $_SESSION['AccessToken'];   
		$depId = $_POST['DepartmentID'];
		$data['section'] = $this->model('sectionModel')->getDataSectionDiv($token, $depId);   		
		$this->view('employee/getSection', $data);   
	}

	public function ajaxGetSectionEdt(){
		$token = $_SESSION['AccessToken'];   
		$employeeId = $_POST['id'];
		$data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $employeeId); 
		$depId = $data['employee']["data"]['department_id'];
		$secId = $data['employee']["data"]["section_id"];
		$data['section'] = $this->model('sectionModel')->getExcDataSection($token, $depId, $secId); 
		$this->view('employee/getSectionEdt', $data);   
	}

	public function ajaxGetDepartmentEdtEmployee(){
		$token = $_SESSION['AccessToken'];   
		$employeeId = $_POST['id'];
		$data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $employeeId); 
		$divId = $data['employee']["data"]["division_id"];
		$depId = $data['employee']["data"]['department_id'];
		$data['department'] = $this->model('departmentModel')->getExcDataDepartment($token, $divId, $depId); 
		$this->view('employee/getDepartmentEdt', $data);   
	}

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Add Employee'; 
		$data['PHP'] = "Employee";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['department'] = $this->model('departmentModel')->getDataDepartments($token);  
		$data['division'] = $this->model('divisionModel')->getDataDivisions($token);  
		$data['location'] = $this->model('locationModel')->getDataLocations($token);  
		$data['position'] = $this->model('positionModel')->getDataPositions($token);  
		$this->view('templates/header', $data);
		$this->view('employee/addEmployee', $data); 
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Edit Employee';
		$data['PHP'] = "Employee"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $id);
		$divId = $data['employee']["data"]["division_id"];
		$data['division'] = $this->model('divisionModel')->getExcDataDivision($token, $divId);  
		$locId = $data['employee']["data"]["location_id"];
		$data['location'] = $this->model('locationModel')->getExcDataLocation($token, $locId);  
		$posId = $data['employee']["data"]["position_id"];
		$data['position'] = $this->model('positionModel')->getExcDataPosition($token, $posId);  
		$this->view('templates/header', $data);
		$this->view('employee/editEmployee', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addEmployee(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		// ambil data file 
		if($_FILES['Initials']['error']==UPLOAD_ERR_OK){
			$upload_dir='../app/views/initials/' ;
			$temp_name = $_FILES['Initials']['tmp_name'];
			$name = basename($_FILES['Initials']['name']);
			$save_path = $upload_dir.$name;
			move_uploaded_file($temp_name, $save_path);
			$uploadedIn = true;
		}

		if($uploadedIn){
			$fhIn = fopen($save_path,'rb');
			$fbytesIn = fread($fhIn, filesize($save_path));
		}

		// ambil data file 
		if($_FILES['Signature']['error']==UPLOAD_ERR_OK){
			$upload_dir1='../app/views/signature/' ;
			$temp_name1 = $_FILES['Signature']['tmp_name'];
			$name1 = basename($_FILES['Signature']['name']);
			$save_path1 = $upload_dir1.$name1;
			move_uploaded_file($temp_name1, $save_path1);
			$uploadedSin = true;
		}

		if($uploadedSin){
			$fhSin = fopen($save_path1,'rb');
			$fbytesSin = fread($fhSin, filesize($save_path1)); 
		} 
		$data['title'] = 'Add Employee';
		$data['PHP'] = "Employee";  
		$row = $this->model('employeeModel')->CreateEmployee($token, $_POST, base64_encode($fbytesIn), base64_encode($fbytesSin), $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Employee'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} else if($row['message'] == "Failed to register employee"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} 
	} 

	public function updateEmployee(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		// ambil data file 
		if($_FILES['Initials']['error']==UPLOAD_ERR_OK){
			$upload_dir='../app/views/initials/' ;
			$temp_name = $_FILES['Initials']['tmp_name'];
			$name = basename($_FILES['Initials']['name']);
			$save_path = $upload_dir.$name;
			move_uploaded_file($temp_name, $save_path);
			$uploadedIn = true;
		}

		if($uploadedIn){
			$fhIn = fopen($save_path,'rb');
			$fbytesIn = fread($fhIn, filesize($save_path));
		}

		// ambil data file 
		if($_FILES['Signature']['error']==UPLOAD_ERR_OK){
			$upload_dir1='../app/views/signature/' ;
			$temp_name1 = $_FILES['Signature']['tmp_name'];
			$name1 = basename($_FILES['Signature']['name']);
			$save_path1 = $upload_dir1.$name1;
			move_uploaded_file($temp_name1, $save_path1);
			$uploadedSin = true;
		}

		if($uploadedSin){
			$fhSin = fopen($save_path1,'rb');
			$fbytesSin = fread($fhSin, filesize($save_path1)); 
		} 
		$data['title'] = 'Edit Employee';
		$data['PHP'] = "Employee";  
		$row = $this->model('employeeModel')->UpdateDataEmployee($token, $_POST, base64_encode($fbytesIn), base64_encode($fbytesSin), $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Employee'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} else if($row['message'] == "Failed to update employee"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} 
	}

	public function deleteEmployee($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete Employee'; 
		$data['employee'] = $this->model('employeeModel')->getDataEmployee($token, $id); 
		$EmployeeNik = $data['employee']["data"]["nik"]; 
		$EmployeeNewNik = $EmployeeNik."-DELETE"; 
		$row = $this->model('employeeModel')->DeleteEmployee($token, $id, $EmployeeNewNik, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Employee'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} else if($row['message'] == "Failed to delete employee"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Employee');  
			exit;	
		} 
	}
} 