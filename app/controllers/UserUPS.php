<?php  
class UserUPS extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data UPS';
		$data['PHP'] = "UserUPS";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['ups'] = $this->model('upsModel')->getDataUPSs($token); 
		$this->view('templates/header', $data);
		$this->view('ups/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	} 

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Add User Role'; 
		$data['PHP'] = "UserUPS";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['user'] = $this->model('userModel')->getDataUsers($token);  
		$data['printer'] = $this->model('printerModel')->getDataPrinters($token);  
		$this->view('templates/header', $data);
		$this->view('ups/addUPS', $data); 
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Edit UPS';
		$data['PHP'] = "UserUPS"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['ups'] = $this->model('upsModel')->getDataUPS($token, $id);
		$idUser = $data['ups']["data"]["user_id"];
		$data['user'] = $this->model('userModel')->getExcDataUser($token, $idUser); 
		$this->view('templates/header', $data);
		$this->view('ups/editUPS', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addUPS(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add UPS';
		$data['PHP'] = "UserUPS";  
		$row = $this->model('upsModel')->CreateUPS($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserUPS'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} else if($row['message'] == "Failed to register ups"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} 
	} 

	public function updateUPS(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit UPS';
		$data['PHP'] = "UserUPS";  
		$row = $this->model('upsModel')->UpdateDataUPS($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserUPS'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} else if($row['message'] == "Failed to update ups"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} 
	}

	public function deleteUPS($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete UPS';   
		$data['ups'] = $this->model('upsModel')->getDataUPS($token, $id);
		$UPSCode = $data['ups']["data"]["ups_code"]; 
		$UPSNewCode = $UPSCode."-DELETE"; 
		$row = $this->model('upsModel')->DeleteUPS($token, $id, $UPSNewCode, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserUPS'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} else if($row['message'] == "Failed to delete ups"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserUPS');  
			exit;	
		} 
	}
} 