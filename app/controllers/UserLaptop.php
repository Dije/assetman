<?php  
class UserLaptop extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data User Laptop';
		$data['PHP'] = "UserLaptop";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['laptop'] = $this->model('laptopModel')->getDataLaptops($token); 
		$this->view('templates/header', $data);
		$this->view('laptop/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	} 

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Add User Laptop'; 
		$data['PHP'] = "UserLaptop";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['user'] = $this->model('userModel')->getDataUsers($token);
		$data['processor'] = $this->model('processorModel')->getDataProcessors($token);
		$data['motherboard'] = $this->model('motherboardModel')->getDataMotherboards($token);
		$data['ram'] = $this->model('ramModel')->getDataRAMs($token);
		$data['hdd'] = $this->model('hddModel')->getDataHDDs($token); 
		$data['monitor'] = $this->model('monitorModel')->getDataMonitors($token);
		$data['os'] = $this->model('osModel')->getDataOSs($token);
		$data['office'] = $this->model('officeModel')->getDataOffices($token);
		$this->view('templates/header', $data);
		$this->view('laptop/addLaptop', $data); 
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Edit User Laptop';
		$data['PHP'] = "UserLaptop"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['laptop'] = $this->model('laptopModel')->getDataLaptop($token, $id);
		$idUser = $data['laptop']["data"]["user_id"];
		$data['user'] = $this->model('userModel')->getExcDataUser($token, $idUser);
		$idProcessor = $data["laptop"]["data"]["processor_id"];
		$data['processor'] = $this->model('processorModel')->getExcDataProcessor($token, $idProcessor);
		$idMotherboard = $data["laptop"]["data"]["motherboard_id"];
		$data['motherboard'] = $this->model('motherboardModel')->getExcDataMotherboard($token, $idMotherboard);
		$idRAM = $data["laptop"]["data"]["ram_id"];
		$data['ram'] = $this->model('ramModel')->getExcDataRAM($token, $idRAM);
		$idHDD = $data["laptop"]["data"]["hdd_id"];
		$data['hdd'] = $this->model('hddModel')->getExcDataHDD($token, $idHDD); 
		$idMonitor = $data["laptop"]["data"]["monitor_id"];
		$data['monitor'] = $this->model('monitorModel')->getExcDataMonitor($token, $idMonitor);
		$idOs = $data["laptop"]["data"]["os_id"];
		$data['os'] = $this->model('osModel')->getExcDataOS($token, $idOs);
		$idOffice = $data["laptop"]["data"]["office_id"];
		$data['office'] = $this->model('officeModel')->getExcDataOffice($token, $idOffice);
		$this->view('templates/header', $data);
		$this->view('laptop/editLaptop', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addLaptop(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Laptop';
		$data['PHP'] = "UserLaptop";  
		if($_POST['CD'] == "YES"){
			$CD = 1; 
		}else{
			$CD = 0;
		}
		$row = $this->model('laptopModel')->CreateLaptop($token, $_POST, $CD, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserLaptop'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} else if($row['message'] == "Failed to register laptop"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} 
	} 

	public function updateLaptop(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Laptop';
		$data['PHP'] = "UserLaptop";  
		if($_POST['CD'] == "YES"){
			$CD = 1; 
		}else{
			$CD = 0;
		}
		$row = $this->model('laptopModel')->UpdateDataLaptop($token, $_POST, $CD, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserLaptop'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} else if($row['message'] == "Failed to update laptop"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} 
	}

	public function deleteLaptop($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete Laptop';   
		$data['laptop'] = $this->model('laptopModel')->getDataLaptop($token, $id);
		$LaptopCode = $data['laptop']["data"]["laptop_code"]; 
		$LaptopNewCode = $LaptopCode."-DELETE"; 
		$row = $this->model('laptopModel')->DeleteLaptop($token, $id, $LaptopNewCode, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserLaptop'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} else if($row['message'] == "Failed to delete laptop"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserLaptop');  
			exit;	
		} 
	}
} 