<?php  
class User extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data User';
		$data['PHP'] = "User"; 
		$data['user'] = $this->model('userModel')->getDataUsers($token);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$this->view('templates/header', $data);
		$this->view('user/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}
	
	public function ajaxGetDataUser(){
		$data['token']  = $_SESSION['AccessToken'];    
		$data['UserID'] = $_SESSION['user_id'];  
		$data['PHP'] = "User";  
		$data['Access'] = $this->model('formModel')->getDataFormByRole($data['token'], $data['UserID']);  
		$this->view('server_side/user_processing', $data);
	}

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Add User'; 
		$data['PHP'] = "User"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$this->view('templates/header', $data);
		$this->view('user/addUser', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Edit User';
		$data['PHP'] = "User"; 
		$data['user'] = $this->model('userModel')->getDataUser($token, $id);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$this->view('templates/header', $data);
		$this->view('user/editUser', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function resetPassword($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add User';
		$data['PHP'] = "User";  
		$data['user'] = $this->model('userModel')->getDataUser($token, $id); 
		if($data['user']["message"] == "OK"){    
			$to = $data['user']['data']["email"]; 
			$Username = $data['user']['data']["username"];  
			$Email =$data['user']['data']["email"]; 
			$this->model('loginModel')->UpdateDataRequest($Email, strtolower($Username), $Date);  
			// Subject
			$subject = 'Reset Password';

			// Message
			$message = ' 
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Reset Password</title>
			<style>
			/* Add custom classes and styles that you want inlined here */
			</style>
			</head>
			<body class="bg-light">
			<div class="container">
			<div class="card my-10">
			<div class="card-body">
			<h1 class="h3 mb-2">Reset Password</h1> 
			<hr>
			<div class="space-y-3">
			<p class="text-gray-700">Hi Mr/Mrs ' . $Username . '!</p>
			<p class="text-gray-700">Want to reset your password?</p>
			<p class="text-gray-700">Click on the button below to reset your password. You have 24 hours to pick your new password. More than that, you have to do the forgot password process again on the Deli Portal.</p> 
			</div>
			<table cellpadding="0" align="left" cellspacing="0" style="border-radius:2px; background-color:#5794FF; color:#FFFFFF">
			<tbody>
			<tr>
			<td align="center" height="32" style="padding:0px 19px; height:32px; font-size:14px; line-height:12px">  
			<a style="text-decoration:none; color:#FFFFFF" href="http://portal.deli.local/Login/Password&Name='.$Username.'&Email='.$Email.'" target="_blank">Reset Password</a> 
			</td>
			</tr>
			</tbody>
			</table>
			</div>
			</div>
			</div>
			</body>
			</html>
			';
			$data['Mail'] = $this->model('mailModel')->sendMail($to, $subject, $message);    
			Flasher::setMessage($data['user']["message"],$data['user']["status"],'success');
			header('location: '. base_url . '/User'); 
			exit;
		} else if($data['user']["message"] == "Failed to process request"){ 
			Flasher::setMessage($data['user']["message"],$data['user']["errors"][0],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} else if($data['user']["message"] == "Failed to register user"){
			Flasher::setMessage($data['user']["message"],$data['user']["errors"][0],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} 
	} 

	public function addUser(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add User';
		$data['PHP'] = "User";  
		$row = $this->model('userModel')->CreateUser($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){   
			// Multiple recipients
			$to = $_POST['Email']; // note the comma
			$Username = $_POST['Username']; 
			$Email = $_POST['Email']; // note the comma
			// Subject
			$subject = 'Set Password';

			// Message
			$message = ' 
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Set Password</title>
			<style>
			/* Add custom classes and styles that you want inlined here */
			</style>
			</head>
			<body class="bg-light">
			<div class="container">
			<div class="card my-10">
			<div class="card-body">
			<h1 class="h3 mb-2">Set Password</h1> 
			<hr>
			<div class="space-y-3">
			<p class="text-gray-700">Hi Mr/Mrs ' . $Username . '!</p>
			<p class="text-gray-700">Want to access Deli Portal?</p>
			<p class="text-gray-700">Click on the button below to reset your password. You have 24 hours to pick your new password. More than that, you have to do the forgot password process again on the Deli Portal.</p> 
			</div>
			<table cellpadding="0" align="left" cellspacing="0" style="border-radius:2px; background-color:#5794FF; color:#FFFFFF">
			<tbody>
			<tr>
			<td align="center" height="32" style="padding:0px 19px; height:32px; font-size:14px; line-height:12px">  
			<a style="text-decoration:none; color:#FFFFFF" href="http://portal.deli.local/Login/Password&Name='.strtolower($Username).'&Email='.$Email.'" target="_blank">Reset Password</a> 
			</td>
			</tr>
			</tbody>
			</table>
			</div>
			</div>
			</div>
			</body>
			</html> 
			';
			$data['Mail'] = $this->model('mailModel')->sendMail($to, $subject, $message);  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/User'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} else if($row['message'] == "Failed to register user"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} 
	} 

	public function updateUser(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit User';
		$data['PHP'] = "User";  
		$row = $this->model('userModel')->UpdateDataUser($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/User'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} else if($row['message'] == "Failed to update user"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} 
	}

	public function deleteUser($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete User'; 
		$data['user'] = $this->model('userModel')->getDataUser($token, $id); 
		$UserName = $data['user']["data"]["username"]; 
		$UserNewName = $UserName."-DELETE"; 
		$email = $data['user']["data"]["email"]; 
		$row = $this->model('userModel')->DeleteUser($token, $id, $UserNewName, $email, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/User'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} else if($row['message'] == "Failed to delete user"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/User');  
			exit;	
		} 
	}
} 