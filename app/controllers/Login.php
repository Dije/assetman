<?php

class Login extends Controller {
	public function index()
	{
		$data['title'] = 'Halaman Login';
		$this->view('login/login', $data);
	} 

	public function Password()
	{
		$data['title'] = 'Reset Password';  
		$data['get'] = $_GET;
		$row = $this->model('loginModel')->CheckExisiting($_GET);  
		$time = $row["data"]["request_change_at"]; 
		$email = $data['get']['Email'];
		$name =$data['get']['Name']; 
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
			$timezone_name = 'Asia/Jakarta';
			$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
			$timezone_name = 'Asia/Makassar'; 
			$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
			$timezone_name = 'Asia/Jayapura'; 
			$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$exp= $time+86400; 
		if($exp>$Date){
			$this->view('templates/header', $data);
			$this->view('login/password', $data); 
		}else{
			Flasher::setMessage('Link Expired','Forget Password Again','danger');
			header('location: '. base_url . '/Login');  
			exit;
		}
	} 
	
	public function ForgetPassword()
	{
		$data['title'] = 'Forget Password';
		$this->view('login/forget-password', $data);
	}
	
	public function prosesLogin() { 
		$row = $this->model('loginModel')->login($_POST);   
		if($row['status'] == true){
			$_SESSION['user_id'] = $row["data"]["user"]["user_id"];
			$_SESSION['employee_id'] = $row["data"]["user"]["employee_id"]; 
			$_SESSION['user_name'] = $row["data"]["user"]["username"]; 
			$_SESSION['AccessToken'] = $row["data"]["access_token"];
			$_SESSION['RefreshToken'] = $row["data"]["refresh_token"];
			$_SESSION['employee_id'] = $row["data"]["user"]["employee_id"];
			$_SESSION['email'] = $row["data"]["user"]["email"];
			$_SESSION['session_login'] = 'sudah_login';   
			Flasher::setMessage('Welcome',$row['data']['user']['username'],'success');
			header('location: '. base_url . '/Home'); 
			exit;
		} else if($row['status'] == false){ 
			Flasher::setMessage($row['errors'][0], $row['message'], 'danger');
			header('location: '. base_url . '/Login');  
			exit;	
		} 
	}
	
	public function prosesForgetPassword() { 
		$row = $this->model('loginModel')->CheckExisiting($_POST);   
		if($row['status'] == true){
			// Multiple recipients
			$to = $row["data"]["email"]; // note the comma
			$Name = $row["data"]["username"];
			$Email = $row["data"]["email"];
			$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
			// echo $timezone_name; 
			if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
				$timezone_name = 'Asia/Jakarta';
				$Clock = "WIB";
			}
			else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
				$timezone_name = 'Asia/Makassar'; 
				$Clock = "WITA";
			}
			else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
				$timezone_name = 'Asia/Jayapura'; 
				$Clock = "WIT";
			}
			date_default_timezone_set($timezone_name);  
			$date = new DateTime(null); 
			$Date = strtotime(date("Y-m-d H:i:s"));
			$this->model('loginModel')->UpdateDataRequest($Email, strtolower($Name), $Date);  
			// Subject
			$subject = 'Forget Password';
			
			// Message
			$message = '
			<html>
			<head>
			<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
			<title>Forgot Password</title>
			<style>
			/* Add custom classes and styles that you want inlined here */
			</style>
			</head>
			<body class="bg-light">
			<div class="container">
			<div class="card my-10">
			<div class="card-body">
			<h1 class="h3 mb-2">Forgot Password</h1> 
			<hr>
			<div class="space-y-3">
			<p class="text-gray-700">Hi  Mr/Mrs ' . $Name . '!</p>
			<p class="text-gray-700">Forget your password?</p>
			<p class="text-gray-700">Click on the button below to reset your password. You have 24 hours to pick your new password. More than that, you have to do the forgot password process again on the Deli Portal.</p> 
			</div>
			<table cellpadding="0" align="left" cellspacing="0" style="border-radius:2px; background-color:#5794FF; color:#FFFFFF">
			<tbody>
			<tr>
			<td align="center" height="32" style="padding:0px 19px; height:32px; font-size:14px; line-height:12px">
			<a style="text-decoration:none; color:#FFFFFF" href="http://portal.deli.local/Login/Password&Name='.$Name.'&Email='.$Email.'" target="_blank">Reset Password</a> 
			</td>
			</tr>
			</tbody>
			</table>
			</div>
			</div>
			</div>
			</body>
			</html> 
			';
			
			$mail = $this->model('mailModel')->sendMail($to, $subject, $message); 
			if($mail['status'] == true){
				Flasher::setMessage('Email Sent','Kindly check your email','success');
				header('location: '. base_url . '/Login'); 
				exit;
			} else if($mail['status'] == false){ 
				Flasher::setMessage($mail['errors'][0],'Please Capture this Error and Contact IT Team', 'danger');
				header('location: '. base_url . '/Login');  
				exit;	
			} 
		} else if($row['status'] == false){ 
			Flasher::setMessage($row['errors'][0], $row['message'], 'danger');
			header('location: '. base_url . '/Login/ForgetPassword');  
			exit;	
		}  
	} 	

	public function prosesResetPassword() {   
		$Email = $_POST['email'];
		$Name = $_POST['name'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
			$timezone_name = 'Asia/Jakarta';
			$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
			$timezone_name = 'Asia/Makassar'; 
			$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
			$timezone_name = 'Asia/Jayapura'; 
			$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));   
		$row = $this->model('loginModel')->UpdateDataPassword($_POST, $Email, strtolower($Name), $Date);  
		if($row['status'] == true){
			Flasher::setMessage('Password Changed','Please login again','success');
			header('location: '. base_url . '/Login');  
			exit;	
		} else if($row['status'] == false){ 
			Flasher::setMessage($row['errors'][0], $row['message'], 'danger');
			header('location: '. base_url . '/Login/Password&Name='.$Name.'&Email='.$Email);  
			exit;	 
		} 
	}
}