<?php 
class Home extends Controller {
	public function __construct(){	
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
	}  

	public function index(){	 
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = "Dashboard"; 
		$data['PHP'] = "Home"; 
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataExpCompanyLicenses($token);
		$data['company'] = $this->model('companyModel')->getDataCompanyApprove($token);
		$data['userRole'] = $this->model('userroleModel')->getDataUserRoleByUserID($token, $UserID);
		$data['companyLicenseApp'] = $this->model('companylicenseModel')->getDataAppCompanyLicenses($token);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$this->view('templates/header', $data);  
		$this->view('home/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}  
}

 