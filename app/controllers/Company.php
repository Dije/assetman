<?php  
class Company extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data Company';
		$data['PHP'] = "Company"; 
		$data['company'] = $this->model('companyModel')->getDataCompanys($token);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['userRole'] = $this->model('userroleModel')->getDataUserRoleByUserID($token, $UserID);
		$this->view('templates/header', $data);
		$this->view('company/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Add Company'; 
		$data['PHP'] = "Company"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['businessUnit'] = $this->model('businessunitModel')->getDataBusinessUnits($token);
		$this->view('templates/header', $data);
		$this->view('company/addCompany', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company';
		$data['PHP'] = "Company"; 
		$data['company'] = $this->model('companyModel')->getDataCompany($token, $id);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$buid = $data["company"]["data"]["business_unit_id"];
		$data['businessUnit'] = $this->model('businessunitModel')->getExcDataBusinessUnit($token, $buid); 
		$data['companyShareholder'] = $this->model('companyshareholderModel')->getDataCompanyShareholderByCompanyId($token, $id);
		$data['companyManagement'] = $this->model('companymanagementModel')->getDataCompanyManagementByCompanyId($token, $id);
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataCompanyLicenseByCompanyId($token, $id);
		$data['userRole'] = $this->model('userroleModel')->getDataUserRoleByUserID($token, $UserID);
		$this->view('templates/header', $data);
		$this->view('company/editCompany', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function detail($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Company Detail';
		$data['PHP'] = "Company"; 
		$data['company'] = $this->model('companyModel')->getDataCompany($token, $id);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$buid = $data["company"]["data"]["business_unit_id"];
		$data['businessUnit'] = $this->model('businessunitModel')->getExcDataBusinessUnit($token, $buid); 
		$data['companyShareholder'] = $this->model('companyshareholderModel')->getDataCompanyShareholderByCompanyId($token, $id);
		$data['companyManagement'] = $this->model('companymanagementModel')->getDataCompanyManagementByCompanyId($token, $id);
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataCompanyLicenseByCompanyId($token, $id);
		$data['userRole'] = $this->model('userroleModel')->getDataUserRoleByUserID($token, $UserID);
		$this->view('templates/header', $data);
		$this->view('company/showCompany', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addCompany(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Company';
		$data['PHP'] = "Company";  
		$row = $this->model('companyModel')->CreateCompany($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "Failed to register company"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} 
	} 

	public function updateCompany(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company';
		$data['PHP'] = "Company";  
		$row = $this->model('companyModel')->UpdateDataCompany($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "Failed to update company"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} 
	}

	public function updateCompanyApprove($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company';
		$data['PHP'] = "Company";  
		$row = $this->model('companyModel')->UpdateDataCompanyApproved($token, $id, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "Failed to update company"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} 
	}

	public function updateCompanyDeactive($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company';
		$data['PHP'] = "Company";  
		$row = $this->model('companyModel')->UpdateDataCompanyDeactive($token, $id, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "Failed to update company"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} 
	}
	
	public function deleteCompany($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete Company'; 
		$data['company'] = $this->model('companyModel')->getDataCompany($token, $id); 
		$CompanyName = $data['company']["data"]["company_name"]; 
		$CompanyNewName = $CompanyName."-DELETE"; 
		$row = $this->model('companyModel')->DeleteCompany($token, $id, $CompanyNewName, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "Failed to delete company"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company');  
			exit;	
		} 
	}
}
