<?php 
class ChangePassword extends Controller {
	public function __construct(){	
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
	}  

	public function index(){	
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id']; 
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = "Change Password"; 
		$data['PHP'] = "ChangePassword"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$this->view('templates/header', $data);  
		$this->view('home/changePassword', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}  

    public function prosesChangePassword() { 
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id']; 
		$data['user'] = $this->model('userModel')->getDataUser($token, $UserID);   
		$Email = $data['user']['data']['email'];
		$Name = $data['user']['data']['username'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));   
		$row = $this->model('loginModel')->UpdateDataPassword($_POST, $Email, $Name, $Date); 
		if($row['status'] == true){
			Flasher::setMessage('Password Changed',')','success');
			header('location: '. base_url . '/Home');  
			exit;	
		} else if($row['status'] == false){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/ChangePassword');  
			exit;	
		}  
	} 	
}

 