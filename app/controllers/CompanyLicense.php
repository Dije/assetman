<?php  
class CompanyLicense extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data Company License';
		$data['PHP'] = "CompanyLicense"; 
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataCompanyLicenses($token);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$data['userRole'] = $this->model('userroleModel')->getDataUserRoleByUserID($token, $UserID);
		$this->view('templates/header', $data);
		$this->view('companyLicense/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function add($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Company License'; 
		$data['PHP'] = "CompanyLicense"; 
		// $data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['licenseType'] = $this->model('licensetypeModel')->getDataLicenseTypes($token); 
        $data['CompanyID'] = $id;
		$this->view('templates/header', $data);
		$this->view('companyLicense/addCompanyLicense', $data);
		$this->view('templates/footer');  
		$this->view('templates/modal', $data);  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null);  
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company License';
		$data['PHP'] = "CompanyLicense";  
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataCompanyLicense($token, $id);
        $data['CompanyID'] = $data['companyLicense']['data']['company_id'];
		$data['CompanyLicenseID'] = $id;
        $LTID = $data['companyLicense']['data']['license_type_id'];
		$data['licenseType'] = $this->model('licensetypeModel')->getExcDataLicenseType($token, $LTID); 
		$data['userRole'] = $this->model('userroleModel')->getDataUserRoleByUserID($token, $UserID);
		// $data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);   
		$this->view('templates/header', $data);
		$this->view('companyLicense/editCompanyLicense', $data);
		$this->view('templates/footer');  
		$this->view('templates/modal', $data);  
		$this->view('templates/script'); 
	}

	public function addCompanyLicense(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Company License';
		$data['PHP'] = "CompanyLicense";  
		if(isset($_POST['Renewable'])){
			$Renewable = 1; 
		}else{
			$Renewable = 0;
		}  
		$row = $this->model('companylicenseModel')->CreateCompanyLicense($token, $_POST, $Renewable, $UserID, $Date); 
		if($row['message'] == "OK"){  
			$row = $this->model('companylicenserenewaltracingModel')->InsertCompanyLicenseRenewalTracingApprove($token, $row['data']['company_license_id'], $row['data']['renewal_status'], $row['data']['remark'], $UserID, $Date);
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);  
			exit;	
		} else if($row['message'] == "Failed to register companyLicense"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);  
			exit;	
		} 
	} 

	public function updateCompanyLicense(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company License';
		$data['PHP'] = "CompanyLicense";  
		if($_POST['Renewable'] == "true"){
			$Renewable = 1; 
		}else{
			$Renewable = 0;
		}  
		$CLID = $_POST['company_license_id'];
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataCompanyLicense($token, $CLID);
		$RenStat = $data['companyLicense']['data']['renewal_status'];
		$Remark = $data['companyLicense']['data']['remark'];
		$row = $this->model('companylicenseModel')->UpdateDataCompanyLicense($token, $_POST, $Renewable, $UserID, $Date); 
		$RenStatUp = $row['data']['renewal_status'];
		$RemarkUp = $row['data']['remark']; 	
		if($row['message'] == "OK"){  
			if($RenStat != $RenStatUp OR $Remark != $RemarkUp){  
				$row = $this->model('companylicenserenewaltracingModel')->InsertCompanyLicenseRenewalTracingApprove($token, $CLID, $row['data']['renewal_status'], $row['data']['remark'], $UserID, $Date);
			}
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);  
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} else if($row['message'] == "Failed to update companyLicense"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} 
	}

	public function updateCompanyLicenseApprove($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company License';
		$data['PHP'] = "CompanyLicense";  
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataCompanyLicense($token, $id);
		$CompanyID = $data['companyLicense']['data']['company_id'];
		$status = $data['companyLicense']['data']['status'];
		if($status == 2){
			$row = $this->model('companylicenseModel')->UpdateStatusCompanyLicenseApproved($token, $id, $UserID, $Date); 
		}
		$renewalStatus = $data['companyLicense']['data']['renewal_status'];
		if($renewalStatus == 3){
			$row = $this->model('companylicenseModel')->UpdateRenewalStatus3CompanyLicenseApproved($token, $id, $UserID, $Date); 
			$row = $this->model('companylicenserenewaltracingModel')->InsertCompanyLicenseRenewalTracingApprove($token, $id, 5, $data['companyLicense']['data']['remark'], $UserID, $Date);
		}
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);  
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} else if($row['message'] == "Failed to update companyLicense"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} 
	}

	public function updateCompanyLicense4(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company License';
		$data['PHP'] = "CompanyLicense";  
		$id= $_POST["companylicenseID"];
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataCompanyLicense($token, $id);
		$CompanyID = $data['companyLicense']['data']['company_id'];
		$Remark = $data['companyLicense']['data']['remark'];
		$RemarkApp = $_POST['Remark'];
		$ResultRemark = $Remark . "\n" . $RemarkApp;  
		$row = $this->model('companylicenseModel')->UpdateRenewalStatus4CompanyLicenseApproved($token, $id, $UserID, $Date);   
		$row = $this->model('companylicenseModel')->UpdateDataCompanyRemark($token, $id, $ResultRemark, $UserID, $Date); 
		$row = $this->model('companylicenserenewaltracingModel')->InsertCompanyLicenseRenewalTracingApprove($token, $id, 6, $ResultRemark, $UserID, $Date);
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);   
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} else if($row['message'] == "Failed to update companyLicense"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} 
	}

	public function updateCompanyLicenseDeactive($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company License';
		$data['PHP'] = "CompanyLicense";  
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataCompanyLicense($token, $id);
		$CompanyID = $data['companyLicense']['data']['company_id'];
		$row = $this->model('companylicenseModel')->UpdateDataCompanyDeactive($token, $id, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);   
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} else if($row['message'] == "Failed to update companyLicense"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);    
			exit;	
		} 
	}

	public function deleteCompanyLicense($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete CompanyLicense'; 
		$data['companyLicense'] = $this->model('companylicenseModel')->getDataCompanyLicense($token, $id);
        $CompanyID =  $data['companyLicense']["data"]["company_id"]; 
		$CompanyLicenseName = $data['companyLicense']["data"]["license_no"]; 
		$CompanyLicenseNewName = $CompanyLicenseName."-DELETE"; 
		$row = $this->model('companylicenseModel')->DeleteCompanyLicense($token, $id, $CompanyLicenseNewName, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} else if($row['message'] == "Failed to delete companyLicense"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} 
	}
}