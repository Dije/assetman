<?php  
class CompanyLicenseRenewalTracing extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data Company License Renewal Tracing';
		$data['PHP'] = "CompanyLicenseRenewalTracing"; 
		$data['companyLicenseRenewalTracing'] = $this->model('companylicenserenewaltracingModel')->getDataCompanyLicenseRenewalTracings($token);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$this->view('templates/header', $data);
		$this->view('companyLicenseRenewalTracing/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Add Company License Renewal Tracing';
		$data['PHP'] = "CompanyLicenseRenewalTracing"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$this->view('templates/header', $data);
		$this->view('companyLicenseRenewalTracing/addCompanyLicenseRenewalTracing', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company License Renewal Tracing';
		$data['PHP'] = "CompanyLicenseRenewalTracing"; 
		$data['companyLicenseRenewalTracing'] = $this->model('companylicenserenewaltracingModel')->getDataCompanyLicenseRenewalTracing($token, $id);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
		$this->view('templates/header', $data);
		$this->view('companyLicenseRenewalTracing/editCompanyLicenseRenewalTracing', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addCompanyLicenseRenewalTracing(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Company License Renewal Tracing';
		$data['PHP'] = "CompanyLicenseRenewalTracing";  
		$row = $this->model('companylicenserenewaltracingModel')->CreateCompanyLicenseRenewalTracing($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} else if($row['message'] == "Failed to register companyLicenseRenewalTracing"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} 
	} 

	public function updateCompanyLicenseRenewalTracing(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company License Renewal Tracing';
		$data['PHP'] = "CompanyLicenseRenewalTracing";  
		$row = $this->model('companylicenserenewaltracingModel')->UpdateDataCompanyLicenseRenewalTracing($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} else if($row['message'] == "Failed to update companyLicenseRenewalTracing"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} 
	}

	public function deleteCompanyLicenseRenewalTracing($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete Company License Renewal Tracing';  
		$row = $this->model('companylicenserenewaltracingModel')->DeleteCompanyLicenseRenewalTracing($token, $id, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} else if($row['message'] == "Failed to delete companyLicenseRenewalTracing"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/CompanyLicenseRenewalTracing');  
			exit;	
		} 
	}
}