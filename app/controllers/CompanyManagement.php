<?php  
class CompanyManagement extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	// public function index(){
	// 	$token = $_SESSION['AccessToken'];
	// 	$UserID = $_SESSION['user_id'];
	// $timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
	// // echo $timezone_name; 
	// if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
	// $timezone_name = 'Asia/Jakarta';
	// $Clock = "WIB";
	// }
	// else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
	// $timezone_name = 'Asia/Makassar'; 
	// $Clock = "WITA";
	// }
	// else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
	// $timezone_name = 'Asia/Jayapura'; 
	// $Clock = "WIT";
	// }
	// date_default_timezone_set($timezone_name);  
	// $date = new DateTime(null); 
	// $Date = strtotime(date("Y-m-d H:i:s"));
	// 	$data['title'] = 'Data CompanyManagement';
	// 	$data['PHP'] = "CompanyManagement"; 
	// 	$data['companyManagement'] = $this->model('companymanagementModel')->getDataCompanyManagements($token);
	// 	$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
	// 	$this->view('templates/header', $data);
	// 	$this->view('companyManagement/index', $data);
	// 	$this->view('templates/footer');  
	// 	$this->view('templates/script'); 
	// }

	public function add($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Company Management'; 
		$data['PHP'] = "CompanyManagement"; 
		// $data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['companyManagementType'] = $this->model('companymanagementtypeModel')->getDataCompanyManagementTypes($token); 
        $data['CompanyID'] = $id;
		$this->view('templates/header', $data);
		$this->view('companyManagement/addCompanyManagement', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company Management';
		$data['PHP'] = "CompanyManagement"; 
		$data['companyManagement'] = $this->model('companymanagementModel')->getDataCompanyManagement($token, $id);
		// $data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID);  
        $CMTID = $data['companyManagement']['data']['company_management_type_id'];
		$data['companyManagementType'] = $this->model('companymanagementtypeModel')->getExcDataCompanyManagementType($token, $CMTID); 
		$this->view('templates/header', $data);
		$this->view('companyManagement/editCompanyManagement', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addCompanyManagement(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Company Management';
		$data['PHP'] = "CompanyManagement";  
		$row = $this->model('companymanagementModel')->CreateCompanyManagement($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);  
			exit;	
		} else if($row['message'] == "Failed to register companyManagement"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);  
			exit;	
		} 
	} 

	public function updateCompanyManagement(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Company Management';
		$data['PHP'] = "CompanyManagement";  
		$row = $this->model('companymanagementModel')->UpdateDataCompanyManagement($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);  
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} else if($row['message'] == "Failed to update companyManagement"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$_POST['company_id']);   
			exit;	
		} 
	}

	public function deleteCompanyManagement($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete CompanyManagement'; 
		$data['companyManagement'] = $this->model('companymanagementModel')->getDataCompanyManagement($token, $id);
        $CompanyID =  $data['companyManagement']["data"]["company_id"]; 
		$CompanyManagementName = $data['companyManagement']["data"]["management_name"]; 
		$CompanyManagementNewName = $CompanyManagementName."-DELETE"; 
		$row = $this->model('companymanagementModel')->DeleteCompanyManagement($token, $id, $CompanyManagementNewName, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/Company/edit/'.$CompanyID);
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} else if($row['message'] == "Failed to delete companyManagement"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/Company/edit/'.$CompanyID); 
			exit;	
		} 
	}
}