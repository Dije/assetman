<?php  
class EmailQueue extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data Email Queue';
		$data['PHP'] = "EmailQueue"; 
		$data['emailQueue'] = $this->model('emailqueueModel')->getDataEmailQueues($token);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$this->view('templates/header', $data);
		$this->view('emailQueue/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	// public function add(){
	// 	$token = $_SESSION['AccessToken'];
	// 	$UserID = $_SESSION['user_id'];
	// 	$data['title'] = 'Add EmailQueue'; 
	// 	$data['PHP'] = "EmailQueue"; 
	// 	$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
	// 	$data['division'] = $this->model('divisionModel')->getDataDivisions($token);
	// 	$this->view('templates/header', $data);
	// 	$this->view('emailQueue/addEmailQueue', $data);
	// 	$this->view('templates/footer');  
	// 	$this->view('templates/script'); 
	// }

	// public function edit($id){
	// 	$token = $_SESSION['AccessToken'];
	// 	$UserID = $_SESSION['user_id'];
	// 	$data['title'] = 'Edit Email Queue';
	// 	$data['PHP'] = "EmailQueue"; 
	// 	$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
	// 	$data['emailQueue'] = $this->model('emailqueueModel')->getDataEmailQueue($token, $id);
	// 	$divId = $data['emailQueue']["data"]["division_id"];
	// 	$data['division'] = $this->model('divisionModel')->getExcDataDivision($token, $divId);  
	// 	$this->view('templates/header', $data);
	// 	$this->view('emailQueue/editEmailQueue', $data);
	// 	$this->view('templates/footer');  
	// 	$this->view('templates/script'); 
	// }

	// public function addEmailQueue(){
	// 	$token = $_SESSION['AccessToken'];
	// 	$UserID = $_SESSION['user_id'];
	// 	$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
	// 	// echo $timezone_name; 
	// 	if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
	// 	$timezone_name = 'Asia/Jakarta';
	// 	$Clock = "WIB";
	// 	}
	// 	else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
	// 	$timezone_name = 'Asia/Makassar'; 
	// 	$Clock = "WITA";
	// 	}
	// 	else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
	// 	$timezone_name = 'Asia/Jayapura'; 
	// 	$Clock = "WIT";
	// 	}
	// 	date_default_timezone_set($timezone_name);  
	// 	$date = new DateTime(null); 
	// 	$Date = strtotime(date("Y-m-d H:i:s"));
	// 	$data['title'] = 'Add Email Queue';
	// 	$data['PHP'] = "EmailQueue";  
	// 	$row = $this->model('emailqueueModel')->CreateEmailQueue($token, $_POST, $UserID, $Date); 
	// 	if($row['message'] == "OK"){  
	// 		Flasher::setMessage($row['status'],$row['message'],'success');
	// 		header('location: '. base_url . '/EmailQueue'); 
	// 		exit;
	// 	} else if($row['message'] == "Failed to process request"){ 
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} else if($row['message'] == "Failed to register emailQueue"){
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} 
	// } 

	// public function updateEmailQueue(){
	// 	$token = $_SESSION['AccessToken'];
	// 	$UserID = $_SESSION['user_id'];
	// 	$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
	// 	// echo $timezone_name; 
	// 	if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
	// 	$timezone_name = 'Asia/Jakarta';
	// 	$Clock = "WIB";
	// 	}
	// 	else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
	// 	$timezone_name = 'Asia/Makassar'; 
	// 	$Clock = "WITA";
	// 	}
	// 	else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
	// 	$timezone_name = 'Asia/Jayapura'; 
	// 	$Clock = "WIT";
	// 	}
	// 	date_default_timezone_set($timezone_name);  
	// 	$date = new DateTime(null); 
	// 	$Date = strtotime(date("Y-m-d H:i:s"));
	// 	$data['title'] = 'Edit Email Queue';
	// 	$data['PHP'] = "EmailQueue";  
	// 	$row = $this->model('emailqueueModel')->UpdateDataEmailQueue($token, $_POST, $UserID, $Date); 
	// 	if($row['message'] == "OK"){  
	// 		Flasher::setMessage($row['status'],$row['message'],'success');
	// 		header('location: '. base_url . '/EmailQueue'); 
	// 		exit;
	// 	} else if($row['message'] == "Data not found"){ 
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} else if($row['message'] == "Failed to process request"){ 
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} else if($row['message'] == "Failed to update emailQueue"){ 
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} else if($row['message'] == "No param id was found"){
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} 
	// }

	// public function deleteEmailQueue($id){
	// 	$token = $_SESSION['AccessToken'];
	// 	$UserID = $_SESSION['user_id'];
	// 	$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
	// 	// echo $timezone_name; 
	// 	if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
	// 	$timezone_name = 'Asia/Jakarta';
	// 	$Clock = "WIB";
	// 	}
	// 	else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
	// 	$timezone_name = 'Asia/Makassar'; 
	// 	$Clock = "WITA";
	// 	}
	// 	else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
	// 	$timezone_name = 'Asia/Jayapura'; 
	// 	$Clock = "WIT";
	// 	}
	// 	date_default_timezone_set($timezone_name);  
	// 	$date = new DateTime(null); 
	// 	$Date = strtotime(date("Y-m-d H:i:s"));
	// 	$data['title'] = 'Delete Email Queue'; 
	// 	$data['emailQueue'] = $this->model('emailqueueModel')->getDataEmailQueue($token, $id); 
	// 	$EmailQueueName = $data['emailQueue']["data"]["emailQueue_name"]; 
	// 	$EmailQueueNewName = $EmailQueueName."-DELETE"; 
	// 	$row = $this->model('emailqueueModel')->DeleteEmailQueue($token, $id, $EmailQueueNewName, $UserID, $Date); 
	// 	if($row['message'] == "OK"){  
	// 		Flasher::setMessage($row['status'],$row['message'],'success');
	// 		header('location: '. base_url . '/EmailQueue'); 
	// 		exit;
	// 	} else if($row['message'] == "Data not found"){ 
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} else if($row['message'] == "Failed to delete emailQueue"){
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} else if($row['message'] == "Failed to process request"){ 
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} else if($row['message'] == "No param id was found"){
	// 		Flasher::setMessage($row['errors'][0],$row['message'],'danger');
	// 		header('location: '. base_url . '/EmailQueue');  
	// 		exit;	
	// 	} 
	// }
}