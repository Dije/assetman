<?php  
class UserAntivirus extends Controller { 
	public function __construct() {
		if($_SESSION['session_login'] != 'sudah_login') {
			Flasher::setMessage('Login','Tidak ditemukan.','danger');
			header('location: '. base_url . '/Login');
			exit;
		}
    } 

	public function index(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Data Antivirus';
		$data['PHP'] = "UserAntivirus"; 
		$data['antivirus'] = $this->model('antivirusModel')->getDataAntiviruss($token);
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$this->view('templates/header', $data);
		$this->view('antivirus/index', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function add(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Antivirus'; 
		$data['PHP'] = "UserAntivirus"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['division'] = $this->model('divisionModel')->getDataDivisions($token);
		$this->view('templates/header', $data);
		$this->view('antivirus/addAntivirus', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function edit($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$data['title'] = 'Edit Antivirus';
		$data['PHP'] = "UserAntivirus"; 
		$data['Access'] = $this->model('formModel')->getDataFormByRole($token, $UserID); 
		$data['antivirus'] = $this->model('antivirusModel')->getDataAntivirus($token, $id); 
		$this->view('templates/header', $data);
		$this->view('antivirus/editAntivirus', $data);
		$this->view('templates/footer');  
		$this->view('templates/script'); 
	}

	public function addAntivirus(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Add Antivirus';
		$data['PHP'] = "UserAntivirus";  
		$row = $this->model('antivirusModel')->CreateAntivirus($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserAntivirus'); 
			exit;
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} else if($row['message'] == "Failed to register antivirus"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} 
	} 

	public function updateAntivirus(){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Edit Antivirus';
		$data['PHP'] = "UserAntivirus";  
		$row = $this->model('antivirusModel')->UpdateDataAntivirus($token, $_POST, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserAntivirus'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} else if($row['message'] == "Failed to update antivirus"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} 
	}

	public function deleteAntivirus($id){
		$token = $_SESSION['AccessToken'];
		$UserID = $_SESSION['user_id'];
		$timezone_name = timezone_name_from_abbr("", $_COOKIE['offset']*60, 0); 
		// echo $timezone_name; 
		if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 420){
		$timezone_name = 'Asia/Jakarta';
		$Clock = "WIB";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 480){
		$timezone_name = 'Asia/Makassar'; 
		$Clock = "WITA";
		}
		else if(isset($_COOKIE['offset']) && $_COOKIE['offset']== 540){
		$timezone_name = 'Asia/Jayapura'; 
		$Clock = "WIT";
		}
		date_default_timezone_set($timezone_name);  
		$date = new DateTime(null); 
		$Date = strtotime(date("Y-m-d H:i:s"));
		$data['title'] = 'Delete Antivirus'; 
		$data['antivirus'] = $this->model('antivirusModel')->getDataAntivirus($token, $id); 
		$AntivirusName = $data['antivirus']["data"]["device"]; 
		$AntivirusNewName = $AntivirusName."-DELETE"; 
		$row = $this->model('antivirusModel')->DeleteAntivirus($token, $id, $AntivirusNewName, $UserID, $Date); 
		if($row['message'] == "OK"){  
			Flasher::setMessage($row['status'],$row['message'],'success');
			header('location: '. base_url . '/UserAntivirus'); 
			exit;
		} else if($row['message'] == "Data not found"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} else if($row['message'] == "Failed to delete antivirus"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} else if($row['message'] == "Failed to process request"){ 
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} else if($row['message'] == "No param id was found"){
			Flasher::setMessage($row['errors'][0],$row['message'],'danger');
			header('location: '. base_url . '/UserAntivirus');  
			exit;	
		} 
	}
}